SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qk_pay_order
-- ----------------------------
DROP TABLE IF EXISTS `qk_pay_order`;
CREATE TABLE `qk_pay_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '插件名称',
  `order_union_id` int(11) NOT NULL COMMENT '支付表ID',
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户订单号',
  `amount` decimal(9, 2) NOT NULL COMMENT '支付金额',
  `is_pay` int(1) NOT NULL DEFAULT 0 COMMENT '支付状态:0=未支付,1=已支付',
  `pay_code` int(1) NOT NULL DEFAULT 0 COMMENT '支付方式',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付标题',
  `notify_class` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'notify处理类',
  `refund` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '已退款金额',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_union_id`(`order_union_id`) USING BTREE,
  INDEX `idx_plugin`(`plugin`) USING BTREE,
  INDEX `idx_order_no`(`order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_pay_order_union
-- ----------------------------
DROP TABLE IF EXISTS `qk_pay_order_union`;
CREATE TABLE `qk_pay_order_union`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `pay_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统支付单号',
  `amount` decimal(9, 2) NOT NULL COMMENT '支付金额',
  `is_pay` int(1) NOT NULL DEFAULT 0 COMMENT '支付状态:0=未支付,1=已支付',
  `pay_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '支付方式',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付内容',
  `support_pay_types` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支持的支付方式（JSON）',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_order_no`(`pay_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付订单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_pay_refund
-- ----------------------------
DROP TABLE IF EXISTS `qk_pay_refund`;
CREATE TABLE `qk_pay_refund`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款单号',
  `out_trade_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '支付单号',
  `amount` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '退款金额',
  `is_pay` int(1) NOT NULL DEFAULT 0 COMMENT '支付状态:0=未支付,1=已支付',
  `pay_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '支付方式',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付内容',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_order_no`(`order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '退款订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_pay_type
-- ----------------------------
DROP TABLE IF EXISTS `qk_pay_type`;
CREATE TABLE `qk_pay_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付标识',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付名称',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付说明',
  `class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类地址',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态:0=禁用,1=启用',
  `config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支付配置',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pay_code`(`pay_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_pay_type
-- ----------------------------
INSERT INTO `qk_pay_type` VALUES (61, 'wechat', '微信支付', NULL, 'app\\common\\service\\payment\\payType\\WechatType', 1, NULL, '2023-03-14 09:17:10', '2023-03-14 10:10:22');
INSERT INTO `qk_pay_type` VALUES (62, 'alipay', '支付宝支付', NULL, 'app\\common\\service\\payment\\payType\\AlipayType', 1, NULL, '2023-03-14 09:17:34', '2023-03-14 10:18:50');
INSERT INTO `qk_pay_type` VALUES (63, 'balance', '余额支付', NULL, 'app\\common\\service\\payment\\payType\\BalanceType', 1, NULL, '2023-03-14 10:21:07', '2023-03-14 10:21:54');

-- ----------------------------
-- Table structure for qk_pay_type_config
-- ----------------------------
DROP TABLE IF EXISTS `qk_pay_type_config`;
CREATE TABLE `qk_pay_type_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL DEFAULT 0 COMMENT '权限编号',
  `plugin` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件名称',
  `pay_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付标识',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态:0=禁用,1=启用',
  `config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支付配置',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_plugin`(`plugin`) USING BTREE,
  INDEX `idx_pay_code`(`pay_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付参数配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_pay_type_config
-- ----------------------------
INSERT INTO `qk_pay_type_config` VALUES (1, 8, 'admin', 'wechat', 1, '[]', '2023-03-31 17:09:36', '2023-03-31 17:09:36');

-- ----------------------------
-- Table structure for qk_system_admin_info
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_admin_info`;
CREATE TABLE `qk_system_admin_info`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '员工id',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号id',
  `plugin_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模块标识',
  `auth_set` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限',
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工邮箱',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工手机号',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工姓名',
  `nickname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工头像',
  `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '员工性别',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:1启用,0:禁用',
  `is_super_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '超级管理员',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除:0=未删除,1=删除',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统管理员-员工' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_admin_info
-- ----------------------------
INSERT INTO `qk_system_admin_info` VALUES (8, 47, 'admin', NULL, '', '', 'admin', 'admin', '', 'unknow', 1, 1, 0, '2022-12-13 10:20:52', '2022-12-13 10:20:52');

-- ----------------------------
-- Table structure for qk_system_area
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_area`;
CREATE TABLE `qk_system_area`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(10) NULL DEFAULT NULL COMMENT '父id',
  `shortname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '简称',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `mergename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '全称',
  `level` tinyint(4) NULL DEFAULT NULL COMMENT '层级:0=省,1=市,2=区县',
  `pinyin` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拼音',
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '长途区号',
  `zip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮编',
  `first` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '首字母',
  `lng` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '地区表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_attachment
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment`;
CREATE TABLE `qk_system_attachment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL COMMENT '仓储ID',
  `attachment_cate_id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `size` int(11) NOT NULL COMMENT '大小字节',
  `image` varchar(2080) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `thumb_image` varchar(2080) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `type` tinyint(2) NOT NULL COMMENT '类型:1=图片,2=视频',
  `is_recycle` tinyint(1) NOT NULL DEFAULT 0 COMMENT '加入回收站:0=否,1=是',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_cate`(`attachment_cate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_attachment_cate
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment_cate`;
CREATE TABLE `qk_system_attachment_cate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `is_recycle` tinyint(1) NOT NULL DEFAULT 0 COMMENT '加入回收站:0=否,1=是',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_attachment_disk
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment_disk`;
CREATE TABLE `qk_system_attachment_disk`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL COMMENT '用户仓储ID',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sti`(`storage_id`, `type`, `is_deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '物理储存' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_attachment_disk
-- ----------------------------
INSERT INTO `qk_system_attachment_disk` VALUES (1, 1, '本地上传', 'local', '', 1, 0, 0, '2023-03-31 17:02:47', '2023-03-31 17:03:22');
INSERT INTO `qk_system_attachment_disk` VALUES (2, 1, '阿里云', 'alioss', '', 0, 0, 0, '2023-03-31 17:02:59', '2023-03-31 17:03:22');

-- ----------------------------
-- Table structure for qk_system_attachment_storage
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_attachment_storage`;
CREATE TABLE `qk_system_attachment_storage`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `app_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用key',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '仓库说明',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_app_key`(`app_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件仓库表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_attachment_storage
-- ----------------------------
INSERT INTO `qk_system_attachment_storage` VALUES (1, 'admin', '', 0, 0, '2023-03-31 17:02:35', '2023-03-31 17:02:35');

-- ----------------------------
-- Table structure for qk_system_auth
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_auth`;
CREATE TABLE `qk_system_auth`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id',
  `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin_name',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '说明',
  `level` smallint(5) NOT NULL DEFAULT 1 COMMENT '管理级别',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_plugin_id`(`plugin_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_auth
-- ----------------------------
INSERT INTO `qk_system_auth` VALUES (1, 0, 'admin', '管理员', '', 1, 1, 0, '2022-12-13 10:21:12', '2022-12-13 10:22:39');

-- ----------------------------
-- Table structure for qk_system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_auth_node`;
CREATE TABLE `qk_system_auth_node`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '角色',
  `node_id` int(10) NULL DEFAULT 0,
  `node` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_system_auth_auth`(`auth`) USING BTREE,
  INDEX `idx_system_auth_node`(`node`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 615 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-授权' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_auth_node
-- ----------------------------
INSERT INTO `qk_system_auth_node` VALUES (1, 1, 0, 'admin');
INSERT INTO `qk_system_auth_node` VALUES (2, 1, 0, 'admin/index');
INSERT INTO `qk_system_auth_node` VALUES (3, 1, 0, 'admin/index/menu');
INSERT INTO `qk_system_auth_node` VALUES (4, 1, 0, 'admin/index/clearcache');
INSERT INTO `qk_system_auth_node` VALUES (5, 1, 0, 'admin/index/logout');
INSERT INTO `qk_system_auth_node` VALUES (6, 1, 0, 'admin/resource/attachment');
INSERT INTO `qk_system_auth_node` VALUES (7, 1, 0, 'admin/resource/attachment/editcate');
INSERT INTO `qk_system_auth_node` VALUES (8, 1, 0, 'admin/resource/attachment/catelist');
INSERT INTO `qk_system_auth_node` VALUES (9, 1, 0, 'admin/resource/attachment/delcate');
INSERT INTO `qk_system_auth_node` VALUES (10, 1, 0, 'admin/resource/attachment/list');
INSERT INTO `qk_system_auth_node` VALUES (11, 1, 0, 'admin/resource/attachment/delete');
INSERT INTO `qk_system_auth_node` VALUES (12, 1, 0, 'admin/resource/attachment/update');
INSERT INTO `qk_system_auth_node` VALUES (13, 1, 0, 'admin/resource/attachment/recycle');
INSERT INTO `qk_system_auth_node` VALUES (14, 1, 0, 'admin/resource/attachment/movecate');
INSERT INTO `qk_system_auth_node` VALUES (15, 1, 0, 'admin/resource/attachment/upload');
INSERT INTO `qk_system_auth_node` VALUES (16, 1, 0, 'admin/resource/attachment/index');
INSERT INTO `qk_system_auth_node` VALUES (17, 1, 0, 'admin/resource/attachment/attachment_cate_list_action');
INSERT INTO `qk_system_auth_node` VALUES (18, 1, 0, 'admin/resource/admin');
INSERT INTO `qk_system_auth_node` VALUES (19, 1, 0, 'admin/resource/admin/searchuser');
INSERT INTO `qk_system_auth_node` VALUES (20, 1, 0, 'admin/resource/admin/index');
INSERT INTO `qk_system_auth_node` VALUES (21, 1, 0, 'admin/resource/admin/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (22, 1, 0, 'admin/resource/admin/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (23, 1, 0, 'admin/resource/admin/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (24, 1, 0, 'admin/resource/admin/edit_user_password_action');
INSERT INTO `qk_system_auth_node` VALUES (25, 1, 0, 'admin/resource/admin/add_action');
INSERT INTO `qk_system_auth_node` VALUES (26, 1, 0, 'admin/resource/auth');
INSERT INTO `qk_system_auth_node` VALUES (27, 1, 0, 'admin/resource/auth/index');
INSERT INTO `qk_system_auth_node` VALUES (28, 1, 0, 'admin/resource/auth/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (29, 1, 0, 'admin/resource/auth/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (30, 1, 0, 'admin/resource/auth/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (31, 1, 0, 'admin/resource/auth/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (32, 1, 0, 'admin/resource/auth/add_action');
INSERT INTO `qk_system_auth_node` VALUES (33, 1, 0, 'admin/resource/auth/refresh_node_action');
INSERT INTO `qk_system_auth_node` VALUES (34, 1, 0, 'admin/resource/group');
INSERT INTO `qk_system_auth_node` VALUES (35, 1, 0, 'admin/resource/group/index');
INSERT INTO `qk_system_auth_node` VALUES (36, 1, 0, 'admin/resource/group/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (37, 1, 0, 'admin/resource/group/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (38, 1, 0, 'admin/resource/group/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (39, 1, 0, 'admin/resource/group/group_data_action');
INSERT INTO `qk_system_auth_node` VALUES (40, 1, 0, 'admin/resource/group/add_action');
INSERT INTO `qk_system_auth_node` VALUES (41, 1, 0, 'admin/resource/group_data');
INSERT INTO `qk_system_auth_node` VALUES (42, 1, 0, 'admin/resource/group_data/index');
INSERT INTO `qk_system_auth_node` VALUES (43, 1, 0, 'admin/resource/group_data/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (44, 1, 0, 'admin/resource/group_data/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (45, 1, 0, 'admin/resource/group_data/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (46, 1, 0, 'admin/resource/group_data/add_action');
INSERT INTO `qk_system_auth_node` VALUES (47, 1, 0, 'admin/resource/menus');
INSERT INTO `qk_system_auth_node` VALUES (48, 1, 0, 'admin/resource/menus/index');
INSERT INTO `qk_system_auth_node` VALUES (49, 1, 0, 'admin/resource/menus/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (50, 1, 0, 'admin/resource/menus/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (51, 1, 0, 'admin/resource/menus/add_action');
INSERT INTO `qk_system_auth_node` VALUES (52, 1, 0, 'admin/resource/menus/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (53, 1, 0, 'admin/resource/menus/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (54, 1, 0, 'admin/resource/node');
INSERT INTO `qk_system_auth_node` VALUES (55, 1, 0, 'admin/resource/node/index');
INSERT INTO `qk_system_auth_node` VALUES (56, 1, 0, 'admin/resource/node/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (57, 1, 0, 'admin/resource/oplog');
INSERT INTO `qk_system_auth_node` VALUES (58, 1, 0, 'admin/resource/oplog/index');
INSERT INTO `qk_system_auth_node` VALUES (59, 1, 0, 'admin/resource/oplog/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (60, 1, 0, 'admin/resource/oplog/view_action');
INSERT INTO `qk_system_auth_node` VALUES (61, 1, 0, 'admin/resource/admin_setting');
INSERT INTO `qk_system_auth_node` VALUES (62, 1, 0, 'admin/resource/admin_setting/admin_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (63, 1, 0, 'admin/resource/admin_setting/admin_password_action');
INSERT INTO `qk_system_auth_node` VALUES (64, 1, 0, 'admin/resource/config');
INSERT INTO `qk_system_auth_node` VALUES (65, 1, 0, 'admin/resource/config/index');
INSERT INTO `qk_system_auth_node` VALUES (66, 1, 0, 'admin/resource/config/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (67, 1, 0, 'admin/resource/config/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (68, 1, 0, 'admin/resource/config/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (69, 1, 0, 'admin/resource/config/add_action');
INSERT INTO `qk_system_auth_node` VALUES (70, 1, 0, 'admin/resource/config/config_group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (71, 1, 0, 'admin/resource/config/config_group_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (72, 1, 0, 'admin/resource/config/config_group_del_action');
INSERT INTO `qk_system_auth_node` VALUES (73, 1, 0, 'admin/resource/config/config_group_list_action');
INSERT INTO `qk_system_auth_node` VALUES (74, 1, 0, 'admin/resource/dashboard');
INSERT INTO `qk_system_auth_node` VALUES (75, 1, 0, 'admin/resource/dashboard/index');
INSERT INTO `qk_system_auth_node` VALUES (76, 1, 0, 'admin/resource/group_new');
INSERT INTO `qk_system_auth_node` VALUES (77, 1, 0, 'admin/resource/group_new/index');
INSERT INTO `qk_system_auth_node` VALUES (78, 1, 0, 'admin/resource/group_new/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (79, 1, 0, 'admin/resource/group_new/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (80, 1, 0, 'admin/resource/group_new/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (81, 1, 0, 'admin/resource/group_new/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (82, 1, 0, 'admin/resource/group_new/add_action');
INSERT INTO `qk_system_auth_node` VALUES (83, 1, 0, 'admin/resource/group_new/group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (84, 1, 0, 'admin/resource/plugin');
INSERT INTO `qk_system_auth_node` VALUES (85, 1, 0, 'admin/resource/plugin/index');
INSERT INTO `qk_system_auth_node` VALUES (86, 1, 0, 'admin/resource/plugin/plugin_status_action');
INSERT INTO `qk_system_auth_node` VALUES (87, 1, 0, 'admin/resource/plugin/plugin_update_action');
INSERT INTO `qk_system_auth_node` VALUES (88, 1, 0, 'admin/resource/plugin/plugin_install_action');
INSERT INTO `qk_system_auth_node` VALUES (89, 1, 0, 'admin/resource/plugin/plugin_uninstall_action');
INSERT INTO `qk_system_auth_node` VALUES (90, 1, 0, 'admin/resource/plugin/plugin_user_info_action');
INSERT INTO `qk_system_auth_node` VALUES (91, 1, 0, 'admin/resource/system_config');
INSERT INTO `qk_system_auth_node` VALUES (92, 1, 0, 'admin/resource/system_config/list');
INSERT INTO `qk_system_auth_node` VALUES (93, 1, 0, 'admin/resource/system_config/index');
INSERT INTO `qk_system_auth_node` VALUES (94, 1, 0, 'admin/resource/system_queue');
INSERT INTO `qk_system_auth_node` VALUES (95, 1, 0, 'admin/resource/system_queue/index');
INSERT INTO `qk_system_auth_node` VALUES (96, 1, 0, 'admin/resource/system_queue/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (97, 1, 0, 'admin/resource/system_queue/re_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (98, 1, 0, 'admin/resource/system_queue/queue_log_action');
INSERT INTO `qk_system_auth_node` VALUES (99, 1, 0, 'admin/resource/system_user');
INSERT INTO `qk_system_auth_node` VALUES (100, 1, 0, 'admin/resource/system_user/index');
INSERT INTO `qk_system_auth_node` VALUES (101, 1, 0, 'admin/resource/system_user/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (102, 1, 0, 'admin/resource/system_user/add_action');
INSERT INTO `qk_system_auth_node` VALUES (103, 1, 0, 'admin/resource/system_user_balance_log');
INSERT INTO `qk_system_auth_node` VALUES (104, 1, 0, 'admin/resource/system_user_balance_log/index');
INSERT INTO `qk_system_auth_node` VALUES (105, 1, 0, 'admin/resource/system_user_info');
INSERT INTO `qk_system_auth_node` VALUES (106, 1, 0, 'admin/resource/system_user_info/index');
INSERT INTO `qk_system_auth_node` VALUES (107, 1, 0, 'admin/resource/system_user_info/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (108, 1, 0, 'admin/resource/system_user_info/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (109, 1, 0, 'admin/resource/system_user_info/user_info_integral_action');
INSERT INTO `qk_system_auth_node` VALUES (110, 1, 0, 'admin/resource/system_user_info/user_info_balance_action');
INSERT INTO `qk_system_auth_node` VALUES (111, 1, 0, 'admin/resource/system_user_info/add_action');
INSERT INTO `qk_system_auth_node` VALUES (112, 1, 0, 'admin/resource/system_user_integral_log');
INSERT INTO `qk_system_auth_node` VALUES (113, 1, 0, 'admin/resource/system_user_integral_log/index');
INSERT INTO `qk_system_auth_node` VALUES (114, 1, 0, 'admin/resource/upgrade');
INSERT INTO `qk_system_auth_node` VALUES (115, 1, 0, 'admin/resource/upgrade/index');
INSERT INTO `qk_system_auth_node` VALUES (116, 1, 0, 'admin/resource/upgrade/upgrade_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (117, 2, 0, 'admin');
INSERT INTO `qk_system_auth_node` VALUES (118, 2, 0, 'admin/index');
INSERT INTO `qk_system_auth_node` VALUES (119, 2, 0, 'admin/index/menu');
INSERT INTO `qk_system_auth_node` VALUES (120, 2, 0, 'admin/index/clearcache');
INSERT INTO `qk_system_auth_node` VALUES (121, 2, 0, 'admin/index/logout');
INSERT INTO `qk_system_auth_node` VALUES (122, 2, 0, 'admin/resource/attachment');
INSERT INTO `qk_system_auth_node` VALUES (123, 2, 0, 'admin/resource/attachment/editcate');
INSERT INTO `qk_system_auth_node` VALUES (124, 2, 0, 'admin/resource/attachment/catelist');
INSERT INTO `qk_system_auth_node` VALUES (125, 2, 0, 'admin/resource/attachment/delcate');
INSERT INTO `qk_system_auth_node` VALUES (126, 2, 0, 'admin/resource/attachment/list');
INSERT INTO `qk_system_auth_node` VALUES (127, 2, 0, 'admin/resource/attachment/delete');
INSERT INTO `qk_system_auth_node` VALUES (128, 2, 0, 'admin/resource/attachment/update');
INSERT INTO `qk_system_auth_node` VALUES (129, 2, 0, 'admin/resource/attachment/recycle');
INSERT INTO `qk_system_auth_node` VALUES (130, 2, 0, 'admin/resource/attachment/movecate');
INSERT INTO `qk_system_auth_node` VALUES (131, 2, 0, 'admin/resource/attachment/upload');
INSERT INTO `qk_system_auth_node` VALUES (132, 2, 0, 'admin/resource/attachment/index');
INSERT INTO `qk_system_auth_node` VALUES (133, 2, 0, 'admin/resource/attachment/attachment_cate_list_action');
INSERT INTO `qk_system_auth_node` VALUES (134, 2, 0, 'admin/resource/admin');
INSERT INTO `qk_system_auth_node` VALUES (135, 2, 0, 'admin/resource/admin/searchuser');
INSERT INTO `qk_system_auth_node` VALUES (136, 2, 0, 'admin/resource/admin/index');
INSERT INTO `qk_system_auth_node` VALUES (137, 2, 0, 'admin/resource/admin/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (138, 2, 0, 'admin/resource/admin/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (139, 2, 0, 'admin/resource/admin/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (140, 2, 0, 'admin/resource/admin/edit_user_password_action');
INSERT INTO `qk_system_auth_node` VALUES (141, 2, 0, 'admin/resource/admin/add_action');
INSERT INTO `qk_system_auth_node` VALUES (142, 2, 0, 'admin/resource/auth');
INSERT INTO `qk_system_auth_node` VALUES (143, 2, 0, 'admin/resource/auth/index');
INSERT INTO `qk_system_auth_node` VALUES (144, 2, 0, 'admin/resource/auth/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (145, 2, 0, 'admin/resource/auth/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (146, 2, 0, 'admin/resource/auth/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (147, 2, 0, 'admin/resource/auth/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (148, 2, 0, 'admin/resource/auth/add_action');
INSERT INTO `qk_system_auth_node` VALUES (149, 2, 0, 'admin/resource/auth/refresh_node_action');
INSERT INTO `qk_system_auth_node` VALUES (150, 2, 0, 'admin/resource/group');
INSERT INTO `qk_system_auth_node` VALUES (151, 2, 0, 'admin/resource/group/index');
INSERT INTO `qk_system_auth_node` VALUES (152, 2, 0, 'admin/resource/group/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (153, 2, 0, 'admin/resource/group/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (154, 2, 0, 'admin/resource/group/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (155, 2, 0, 'admin/resource/group/group_data_action');
INSERT INTO `qk_system_auth_node` VALUES (156, 2, 0, 'admin/resource/group/add_action');
INSERT INTO `qk_system_auth_node` VALUES (157, 2, 0, 'admin/resource/group_data');
INSERT INTO `qk_system_auth_node` VALUES (158, 2, 0, 'admin/resource/group_data/index');
INSERT INTO `qk_system_auth_node` VALUES (159, 2, 0, 'admin/resource/group_data/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (160, 2, 0, 'admin/resource/group_data/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (161, 2, 0, 'admin/resource/group_data/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (162, 2, 0, 'admin/resource/group_data/add_action');
INSERT INTO `qk_system_auth_node` VALUES (163, 2, 0, 'admin/resource/menus');
INSERT INTO `qk_system_auth_node` VALUES (164, 2, 0, 'admin/resource/menus/index');
INSERT INTO `qk_system_auth_node` VALUES (165, 2, 0, 'admin/resource/menus/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (166, 2, 0, 'admin/resource/menus/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (167, 2, 0, 'admin/resource/menus/add_action');
INSERT INTO `qk_system_auth_node` VALUES (168, 2, 0, 'admin/resource/menus/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (169, 2, 0, 'admin/resource/menus/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (170, 2, 0, 'admin/resource/node');
INSERT INTO `qk_system_auth_node` VALUES (171, 2, 0, 'admin/resource/node/index');
INSERT INTO `qk_system_auth_node` VALUES (172, 2, 0, 'admin/resource/node/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (173, 2, 0, 'admin/resource/oplog');
INSERT INTO `qk_system_auth_node` VALUES (174, 2, 0, 'admin/resource/oplog/index');
INSERT INTO `qk_system_auth_node` VALUES (175, 2, 0, 'admin/resource/oplog/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (176, 2, 0, 'admin/resource/oplog/view_action');
INSERT INTO `qk_system_auth_node` VALUES (177, 2, 0, 'admin/resource/admin_setting');
INSERT INTO `qk_system_auth_node` VALUES (178, 2, 0, 'admin/resource/admin_setting/admin_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (179, 2, 0, 'admin/resource/admin_setting/admin_password_action');
INSERT INTO `qk_system_auth_node` VALUES (180, 2, 0, 'admin/resource/config');
INSERT INTO `qk_system_auth_node` VALUES (181, 2, 0, 'admin/resource/config/index');
INSERT INTO `qk_system_auth_node` VALUES (182, 2, 0, 'admin/resource/config/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (183, 2, 0, 'admin/resource/config/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (184, 2, 0, 'admin/resource/config/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (185, 2, 0, 'admin/resource/config/add_action');
INSERT INTO `qk_system_auth_node` VALUES (186, 2, 0, 'admin/resource/config/config_group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (187, 2, 0, 'admin/resource/config/config_group_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (188, 2, 0, 'admin/resource/config/config_group_del_action');
INSERT INTO `qk_system_auth_node` VALUES (189, 2, 0, 'admin/resource/config/config_group_list_action');
INSERT INTO `qk_system_auth_node` VALUES (190, 2, 0, 'admin/resource/dashboard');
INSERT INTO `qk_system_auth_node` VALUES (191, 2, 0, 'admin/resource/dashboard/index');
INSERT INTO `qk_system_auth_node` VALUES (192, 2, 0, 'admin/resource/group_new');
INSERT INTO `qk_system_auth_node` VALUES (193, 2, 0, 'admin/resource/group_new/index');
INSERT INTO `qk_system_auth_node` VALUES (194, 2, 0, 'admin/resource/group_new/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (195, 2, 0, 'admin/resource/group_new/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (196, 2, 0, 'admin/resource/group_new/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (197, 2, 0, 'admin/resource/group_new/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (198, 2, 0, 'admin/resource/group_new/add_action');
INSERT INTO `qk_system_auth_node` VALUES (199, 2, 0, 'admin/resource/group_new/group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (200, 2, 0, 'admin/resource/plugin');
INSERT INTO `qk_system_auth_node` VALUES (201, 2, 0, 'admin/resource/plugin/index');
INSERT INTO `qk_system_auth_node` VALUES (202, 2, 0, 'admin/resource/plugin/plugin_status_action');
INSERT INTO `qk_system_auth_node` VALUES (203, 2, 0, 'admin/resource/plugin/plugin_update_action');
INSERT INTO `qk_system_auth_node` VALUES (204, 2, 0, 'admin/resource/plugin/plugin_install_action');
INSERT INTO `qk_system_auth_node` VALUES (205, 2, 0, 'admin/resource/plugin/plugin_uninstall_action');
INSERT INTO `qk_system_auth_node` VALUES (206, 2, 0, 'admin/resource/plugin/plugin_user_info_action');
INSERT INTO `qk_system_auth_node` VALUES (207, 2, 0, 'admin/resource/system_config');
INSERT INTO `qk_system_auth_node` VALUES (208, 2, 0, 'admin/resource/system_config/list');
INSERT INTO `qk_system_auth_node` VALUES (209, 2, 0, 'admin/resource/system_config/index');
INSERT INTO `qk_system_auth_node` VALUES (210, 2, 0, 'admin/resource/system_queue');
INSERT INTO `qk_system_auth_node` VALUES (211, 2, 0, 'admin/resource/system_queue/index');
INSERT INTO `qk_system_auth_node` VALUES (212, 2, 0, 'admin/resource/system_queue/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (213, 2, 0, 'admin/resource/system_queue/re_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (214, 2, 0, 'admin/resource/system_queue/queue_log_action');
INSERT INTO `qk_system_auth_node` VALUES (215, 2, 0, 'admin/resource/system_user');
INSERT INTO `qk_system_auth_node` VALUES (216, 2, 0, 'admin/resource/system_user/index');
INSERT INTO `qk_system_auth_node` VALUES (217, 2, 0, 'admin/resource/system_user/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (218, 2, 0, 'admin/resource/system_user/add_action');
INSERT INTO `qk_system_auth_node` VALUES (219, 2, 0, 'admin/resource/system_user_balance_log');
INSERT INTO `qk_system_auth_node` VALUES (220, 2, 0, 'admin/resource/system_user_balance_log/index');
INSERT INTO `qk_system_auth_node` VALUES (221, 2, 0, 'admin/resource/system_user_info');
INSERT INTO `qk_system_auth_node` VALUES (222, 2, 0, 'admin/resource/system_user_info/index');
INSERT INTO `qk_system_auth_node` VALUES (223, 2, 0, 'admin/resource/system_user_info/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (224, 2, 0, 'admin/resource/system_user_info/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (225, 2, 0, 'admin/resource/system_user_info/user_info_integral_action');
INSERT INTO `qk_system_auth_node` VALUES (226, 2, 0, 'admin/resource/system_user_info/user_info_balance_action');
INSERT INTO `qk_system_auth_node` VALUES (227, 2, 0, 'admin/resource/system_user_info/add_action');
INSERT INTO `qk_system_auth_node` VALUES (228, 2, 0, 'admin/resource/system_user_integral_log');
INSERT INTO `qk_system_auth_node` VALUES (229, 2, 0, 'admin/resource/system_user_integral_log/index');
INSERT INTO `qk_system_auth_node` VALUES (230, 2, 0, 'admin/resource/upgrade');
INSERT INTO `qk_system_auth_node` VALUES (231, 2, 0, 'admin/resource/upgrade/index');
INSERT INTO `qk_system_auth_node` VALUES (232, 2, 0, 'admin/resource/upgrade/upgrade_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (233, 1, 0, 'admin');
INSERT INTO `qk_system_auth_node` VALUES (234, 1, 0, 'admin/index');
INSERT INTO `qk_system_auth_node` VALUES (235, 1, 0, 'admin/index/menu');
INSERT INTO `qk_system_auth_node` VALUES (236, 1, 0, 'admin/index/clearcache');
INSERT INTO `qk_system_auth_node` VALUES (237, 1, 0, 'admin/index/logout');
INSERT INTO `qk_system_auth_node` VALUES (238, 1, 0, 'admin/resource/attachment');
INSERT INTO `qk_system_auth_node` VALUES (239, 1, 0, 'admin/resource/attachment/editcate');
INSERT INTO `qk_system_auth_node` VALUES (240, 1, 0, 'admin/resource/attachment/catelist');
INSERT INTO `qk_system_auth_node` VALUES (241, 1, 0, 'admin/resource/attachment/delcate');
INSERT INTO `qk_system_auth_node` VALUES (242, 1, 0, 'admin/resource/attachment/list');
INSERT INTO `qk_system_auth_node` VALUES (243, 1, 0, 'admin/resource/attachment/delete');
INSERT INTO `qk_system_auth_node` VALUES (244, 1, 0, 'admin/resource/attachment/update');
INSERT INTO `qk_system_auth_node` VALUES (245, 1, 0, 'admin/resource/attachment/recycle');
INSERT INTO `qk_system_auth_node` VALUES (246, 1, 0, 'admin/resource/attachment/movecate');
INSERT INTO `qk_system_auth_node` VALUES (247, 1, 0, 'admin/resource/attachment/upload');
INSERT INTO `qk_system_auth_node` VALUES (248, 1, 0, 'admin/resource/attachment/index');
INSERT INTO `qk_system_auth_node` VALUES (249, 1, 0, 'admin/resource/attachment/attachment_cate_list_action');
INSERT INTO `qk_system_auth_node` VALUES (250, 1, 0, 'admin/resource/admin');
INSERT INTO `qk_system_auth_node` VALUES (251, 1, 0, 'admin/resource/admin/searchuser');
INSERT INTO `qk_system_auth_node` VALUES (252, 1, 0, 'admin/resource/admin/index');
INSERT INTO `qk_system_auth_node` VALUES (253, 1, 0, 'admin/resource/admin/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (254, 1, 0, 'admin/resource/admin/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (255, 1, 0, 'admin/resource/admin/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (256, 1, 0, 'admin/resource/admin/edit_user_password_action');
INSERT INTO `qk_system_auth_node` VALUES (257, 1, 0, 'admin/resource/admin/add_action');
INSERT INTO `qk_system_auth_node` VALUES (258, 1, 0, 'admin/resource/auth');
INSERT INTO `qk_system_auth_node` VALUES (259, 1, 0, 'admin/resource/auth/index');
INSERT INTO `qk_system_auth_node` VALUES (260, 1, 0, 'admin/resource/auth/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (261, 1, 0, 'admin/resource/auth/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (262, 1, 0, 'admin/resource/auth/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (263, 1, 0, 'admin/resource/auth/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (264, 1, 0, 'admin/resource/auth/add_action');
INSERT INTO `qk_system_auth_node` VALUES (265, 1, 0, 'admin/resource/auth/refresh_node_action');
INSERT INTO `qk_system_auth_node` VALUES (266, 1, 0, 'admin/resource/group');
INSERT INTO `qk_system_auth_node` VALUES (267, 1, 0, 'admin/resource/group/index');
INSERT INTO `qk_system_auth_node` VALUES (268, 1, 0, 'admin/resource/group/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (269, 1, 0, 'admin/resource/group/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (270, 1, 0, 'admin/resource/group/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (271, 1, 0, 'admin/resource/group/group_data_action');
INSERT INTO `qk_system_auth_node` VALUES (272, 1, 0, 'admin/resource/group/add_action');
INSERT INTO `qk_system_auth_node` VALUES (273, 1, 0, 'admin/resource/group_data');
INSERT INTO `qk_system_auth_node` VALUES (274, 1, 0, 'admin/resource/group_data/index');
INSERT INTO `qk_system_auth_node` VALUES (275, 1, 0, 'admin/resource/group_data/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (276, 1, 0, 'admin/resource/group_data/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (277, 1, 0, 'admin/resource/group_data/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (278, 1, 0, 'admin/resource/group_data/add_action');
INSERT INTO `qk_system_auth_node` VALUES (279, 1, 0, 'admin/resource/menus');
INSERT INTO `qk_system_auth_node` VALUES (280, 1, 0, 'admin/resource/menus/index');
INSERT INTO `qk_system_auth_node` VALUES (281, 1, 0, 'admin/resource/menus/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (282, 1, 0, 'admin/resource/menus/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (283, 1, 0, 'admin/resource/menus/add_action');
INSERT INTO `qk_system_auth_node` VALUES (284, 1, 0, 'admin/resource/menus/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (285, 1, 0, 'admin/resource/menus/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (286, 1, 0, 'admin/resource/node');
INSERT INTO `qk_system_auth_node` VALUES (287, 1, 0, 'admin/resource/node/index');
INSERT INTO `qk_system_auth_node` VALUES (288, 1, 0, 'admin/resource/node/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (289, 1, 0, 'admin/resource/oplog');
INSERT INTO `qk_system_auth_node` VALUES (290, 1, 0, 'admin/resource/oplog/index');
INSERT INTO `qk_system_auth_node` VALUES (291, 1, 0, 'admin/resource/oplog/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (292, 1, 0, 'admin/resource/oplog/view_action');
INSERT INTO `qk_system_auth_node` VALUES (293, 1, 0, 'admin/resource/admin_setting');
INSERT INTO `qk_system_auth_node` VALUES (294, 1, 0, 'admin/resource/admin_setting/admin_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (295, 1, 0, 'admin/resource/admin_setting/admin_password_action');
INSERT INTO `qk_system_auth_node` VALUES (296, 1, 0, 'admin/resource/config');
INSERT INTO `qk_system_auth_node` VALUES (297, 1, 0, 'admin/resource/config/index');
INSERT INTO `qk_system_auth_node` VALUES (298, 1, 0, 'admin/resource/config/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (299, 1, 0, 'admin/resource/config/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (300, 1, 0, 'admin/resource/config/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (301, 1, 0, 'admin/resource/config/add_action');
INSERT INTO `qk_system_auth_node` VALUES (302, 1, 0, 'admin/resource/config/config_group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (303, 1, 0, 'admin/resource/config/config_group_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (304, 1, 0, 'admin/resource/config/config_group_del_action');
INSERT INTO `qk_system_auth_node` VALUES (305, 1, 0, 'admin/resource/config/config_group_list_action');
INSERT INTO `qk_system_auth_node` VALUES (306, 1, 0, 'admin/resource/dashboard');
INSERT INTO `qk_system_auth_node` VALUES (307, 1, 0, 'admin/resource/dashboard/index');
INSERT INTO `qk_system_auth_node` VALUES (308, 1, 0, 'admin/resource/group_new');
INSERT INTO `qk_system_auth_node` VALUES (309, 1, 0, 'admin/resource/group_new/index');
INSERT INTO `qk_system_auth_node` VALUES (310, 1, 0, 'admin/resource/group_new/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (311, 1, 0, 'admin/resource/group_new/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (312, 1, 0, 'admin/resource/group_new/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (313, 1, 0, 'admin/resource/group_new/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (314, 1, 0, 'admin/resource/group_new/add_action');
INSERT INTO `qk_system_auth_node` VALUES (315, 1, 0, 'admin/resource/group_new/group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (316, 1, 0, 'admin/resource/plugin');
INSERT INTO `qk_system_auth_node` VALUES (317, 1, 0, 'admin/resource/plugin/index');
INSERT INTO `qk_system_auth_node` VALUES (318, 1, 0, 'admin/resource/plugin/plugin_status_action');
INSERT INTO `qk_system_auth_node` VALUES (319, 1, 0, 'admin/resource/plugin/plugin_update_action');
INSERT INTO `qk_system_auth_node` VALUES (320, 1, 0, 'admin/resource/plugin/plugin_install_action');
INSERT INTO `qk_system_auth_node` VALUES (321, 1, 0, 'admin/resource/plugin/plugin_uninstall_action');
INSERT INTO `qk_system_auth_node` VALUES (322, 1, 0, 'admin/resource/plugin/plugin_user_info_action');
INSERT INTO `qk_system_auth_node` VALUES (323, 1, 0, 'admin/resource/system_config');
INSERT INTO `qk_system_auth_node` VALUES (324, 1, 0, 'admin/resource/system_config/list');
INSERT INTO `qk_system_auth_node` VALUES (325, 1, 0, 'admin/resource/system_config/index');
INSERT INTO `qk_system_auth_node` VALUES (326, 1, 0, 'admin/resource/system_queue');
INSERT INTO `qk_system_auth_node` VALUES (327, 1, 0, 'admin/resource/system_queue/index');
INSERT INTO `qk_system_auth_node` VALUES (328, 1, 0, 'admin/resource/system_queue/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (329, 1, 0, 'admin/resource/system_queue/re_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (330, 1, 0, 'admin/resource/system_queue/queue_log_action');
INSERT INTO `qk_system_auth_node` VALUES (331, 1, 0, 'admin/resource/system_user');
INSERT INTO `qk_system_auth_node` VALUES (332, 1, 0, 'admin/resource/system_user/index');
INSERT INTO `qk_system_auth_node` VALUES (333, 1, 0, 'admin/resource/system_user/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (334, 1, 0, 'admin/resource/system_user/add_action');
INSERT INTO `qk_system_auth_node` VALUES (335, 1, 0, 'admin/resource/system_user_balance_log');
INSERT INTO `qk_system_auth_node` VALUES (336, 1, 0, 'admin/resource/system_user_balance_log/index');
INSERT INTO `qk_system_auth_node` VALUES (337, 1, 0, 'admin/resource/system_user_info');
INSERT INTO `qk_system_auth_node` VALUES (338, 1, 0, 'admin/resource/system_user_info/index');
INSERT INTO `qk_system_auth_node` VALUES (339, 1, 0, 'admin/resource/system_user_info/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (340, 1, 0, 'admin/resource/system_user_info/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (341, 1, 0, 'admin/resource/system_user_info/user_info_integral_action');
INSERT INTO `qk_system_auth_node` VALUES (342, 1, 0, 'admin/resource/system_user_info/user_info_balance_action');
INSERT INTO `qk_system_auth_node` VALUES (343, 1, 0, 'admin/resource/system_user_info/add_action');
INSERT INTO `qk_system_auth_node` VALUES (344, 1, 0, 'admin/resource/system_user_integral_log');
INSERT INTO `qk_system_auth_node` VALUES (345, 1, 0, 'admin/resource/system_user_integral_log/index');
INSERT INTO `qk_system_auth_node` VALUES (346, 1, 0, 'admin/resource/upgrade');
INSERT INTO `qk_system_auth_node` VALUES (347, 1, 0, 'admin/resource/upgrade/index');
INSERT INTO `qk_system_auth_node` VALUES (348, 1, 0, 'admin/resource/upgrade/upgrade_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (366, 1, 0, 'admin');
INSERT INTO `qk_system_auth_node` VALUES (367, 1, 0, 'admin/index');
INSERT INTO `qk_system_auth_node` VALUES (368, 1, 0, 'admin/index/menu');
INSERT INTO `qk_system_auth_node` VALUES (369, 1, 0, 'admin/index/clearcache');
INSERT INTO `qk_system_auth_node` VALUES (370, 1, 0, 'admin/index/logout');
INSERT INTO `qk_system_auth_node` VALUES (371, 1, 0, 'admin/resource/attachment');
INSERT INTO `qk_system_auth_node` VALUES (372, 1, 0, 'admin/resource/attachment/editcate');
INSERT INTO `qk_system_auth_node` VALUES (373, 1, 0, 'admin/resource/attachment/catelist');
INSERT INTO `qk_system_auth_node` VALUES (374, 1, 0, 'admin/resource/attachment/delcate');
INSERT INTO `qk_system_auth_node` VALUES (375, 1, 0, 'admin/resource/attachment/list');
INSERT INTO `qk_system_auth_node` VALUES (376, 1, 0, 'admin/resource/attachment/delete');
INSERT INTO `qk_system_auth_node` VALUES (377, 1, 0, 'admin/resource/attachment/update');
INSERT INTO `qk_system_auth_node` VALUES (378, 1, 0, 'admin/resource/attachment/recycle');
INSERT INTO `qk_system_auth_node` VALUES (379, 1, 0, 'admin/resource/attachment/movecate');
INSERT INTO `qk_system_auth_node` VALUES (380, 1, 0, 'admin/resource/attachment/upload');
INSERT INTO `qk_system_auth_node` VALUES (381, 1, 0, 'admin/resource/attachment/index');
INSERT INTO `qk_system_auth_node` VALUES (382, 1, 0, 'admin/resource/attachment/attachment_cate_list_action');
INSERT INTO `qk_system_auth_node` VALUES (383, 1, 0, 'admin/resource/admin');
INSERT INTO `qk_system_auth_node` VALUES (384, 1, 0, 'admin/resource/admin/searchuser');
INSERT INTO `qk_system_auth_node` VALUES (385, 1, 0, 'admin/resource/admin/index');
INSERT INTO `qk_system_auth_node` VALUES (386, 1, 0, 'admin/resource/admin/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (387, 1, 0, 'admin/resource/admin/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (388, 1, 0, 'admin/resource/admin/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (389, 1, 0, 'admin/resource/admin/edit_user_password_action');
INSERT INTO `qk_system_auth_node` VALUES (390, 1, 0, 'admin/resource/admin/add_action');
INSERT INTO `qk_system_auth_node` VALUES (391, 1, 0, 'admin/resource/auth');
INSERT INTO `qk_system_auth_node` VALUES (392, 1, 0, 'admin/resource/auth/index');
INSERT INTO `qk_system_auth_node` VALUES (393, 1, 0, 'admin/resource/auth/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (394, 1, 0, 'admin/resource/auth/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (395, 1, 0, 'admin/resource/auth/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (396, 1, 0, 'admin/resource/auth/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (397, 1, 0, 'admin/resource/auth/add_action');
INSERT INTO `qk_system_auth_node` VALUES (398, 1, 0, 'admin/resource/auth/refresh_node_action');
INSERT INTO `qk_system_auth_node` VALUES (399, 1, 0, 'admin/resource/group');
INSERT INTO `qk_system_auth_node` VALUES (400, 1, 0, 'admin/resource/group/index');
INSERT INTO `qk_system_auth_node` VALUES (401, 1, 0, 'admin/resource/group/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (402, 1, 0, 'admin/resource/group/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (403, 1, 0, 'admin/resource/group/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (404, 1, 0, 'admin/resource/group/group_data_action');
INSERT INTO `qk_system_auth_node` VALUES (405, 1, 0, 'admin/resource/group/add_action');
INSERT INTO `qk_system_auth_node` VALUES (406, 1, 0, 'admin/resource/group_data');
INSERT INTO `qk_system_auth_node` VALUES (407, 1, 0, 'admin/resource/group_data/index');
INSERT INTO `qk_system_auth_node` VALUES (408, 1, 0, 'admin/resource/group_data/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (409, 1, 0, 'admin/resource/group_data/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (410, 1, 0, 'admin/resource/group_data/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (411, 1, 0, 'admin/resource/group_data/add_action');
INSERT INTO `qk_system_auth_node` VALUES (412, 1, 0, 'admin/resource/menus');
INSERT INTO `qk_system_auth_node` VALUES (413, 1, 0, 'admin/resource/menus/index');
INSERT INTO `qk_system_auth_node` VALUES (414, 1, 0, 'admin/resource/menus/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (415, 1, 0, 'admin/resource/menus/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (416, 1, 0, 'admin/resource/menus/add_action');
INSERT INTO `qk_system_auth_node` VALUES (417, 1, 0, 'admin/resource/menus/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (418, 1, 0, 'admin/resource/menus/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (419, 1, 0, 'admin/resource/node');
INSERT INTO `qk_system_auth_node` VALUES (420, 1, 0, 'admin/resource/node/index');
INSERT INTO `qk_system_auth_node` VALUES (421, 1, 0, 'admin/resource/node/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (422, 1, 0, 'admin/resource/oplog');
INSERT INTO `qk_system_auth_node` VALUES (423, 1, 0, 'admin/resource/oplog/index');
INSERT INTO `qk_system_auth_node` VALUES (424, 1, 0, 'admin/resource/oplog/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (425, 1, 0, 'admin/resource/oplog/view_action');
INSERT INTO `qk_system_auth_node` VALUES (426, 1, 0, 'admin/resource/admin_setting');
INSERT INTO `qk_system_auth_node` VALUES (427, 1, 0, 'admin/resource/admin_setting/admin_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (428, 1, 0, 'admin/resource/admin_setting/admin_password_action');
INSERT INTO `qk_system_auth_node` VALUES (429, 1, 0, 'admin/resource/config');
INSERT INTO `qk_system_auth_node` VALUES (430, 1, 0, 'admin/resource/config/index');
INSERT INTO `qk_system_auth_node` VALUES (431, 1, 0, 'admin/resource/config/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (432, 1, 0, 'admin/resource/config/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (433, 1, 0, 'admin/resource/config/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (434, 1, 0, 'admin/resource/config/add_action');
INSERT INTO `qk_system_auth_node` VALUES (435, 1, 0, 'admin/resource/config/config_group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (436, 1, 0, 'admin/resource/config/config_group_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (437, 1, 0, 'admin/resource/config/config_group_del_action');
INSERT INTO `qk_system_auth_node` VALUES (438, 1, 0, 'admin/resource/config/config_group_list_action');
INSERT INTO `qk_system_auth_node` VALUES (439, 1, 0, 'admin/resource/dashboard');
INSERT INTO `qk_system_auth_node` VALUES (440, 1, 0, 'admin/resource/dashboard/index');
INSERT INTO `qk_system_auth_node` VALUES (441, 1, 0, 'admin/resource/group_new');
INSERT INTO `qk_system_auth_node` VALUES (442, 1, 0, 'admin/resource/group_new/index');
INSERT INTO `qk_system_auth_node` VALUES (443, 1, 0, 'admin/resource/group_new/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (444, 1, 0, 'admin/resource/group_new/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (445, 1, 0, 'admin/resource/group_new/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (446, 1, 0, 'admin/resource/group_new/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (447, 1, 0, 'admin/resource/group_new/add_action');
INSERT INTO `qk_system_auth_node` VALUES (448, 1, 0, 'admin/resource/group_new/group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (449, 1, 0, 'admin/resource/plugin');
INSERT INTO `qk_system_auth_node` VALUES (450, 1, 0, 'admin/resource/plugin/index');
INSERT INTO `qk_system_auth_node` VALUES (451, 1, 0, 'admin/resource/plugin/plugin_status_action');
INSERT INTO `qk_system_auth_node` VALUES (452, 1, 0, 'admin/resource/plugin/plugin_update_action');
INSERT INTO `qk_system_auth_node` VALUES (453, 1, 0, 'admin/resource/plugin/plugin_install_action');
INSERT INTO `qk_system_auth_node` VALUES (454, 1, 0, 'admin/resource/plugin/plugin_uninstall_action');
INSERT INTO `qk_system_auth_node` VALUES (455, 1, 0, 'admin/resource/plugin/plugin_user_info_action');
INSERT INTO `qk_system_auth_node` VALUES (456, 1, 0, 'admin/resource/system_config');
INSERT INTO `qk_system_auth_node` VALUES (457, 1, 0, 'admin/resource/system_config/list');
INSERT INTO `qk_system_auth_node` VALUES (458, 1, 0, 'admin/resource/system_config/index');
INSERT INTO `qk_system_auth_node` VALUES (459, 1, 0, 'admin/resource/system_queue');
INSERT INTO `qk_system_auth_node` VALUES (460, 1, 0, 'admin/resource/system_queue/index');
INSERT INTO `qk_system_auth_node` VALUES (461, 1, 0, 'admin/resource/system_queue/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (462, 1, 0, 'admin/resource/system_queue/re_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (463, 1, 0, 'admin/resource/system_queue/queue_log_action');
INSERT INTO `qk_system_auth_node` VALUES (464, 1, 0, 'admin/resource/system_user');
INSERT INTO `qk_system_auth_node` VALUES (465, 1, 0, 'admin/resource/system_user/index');
INSERT INTO `qk_system_auth_node` VALUES (466, 1, 0, 'admin/resource/system_user/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (467, 1, 0, 'admin/resource/system_user/add_action');
INSERT INTO `qk_system_auth_node` VALUES (468, 1, 0, 'admin/resource/system_user_balance_log');
INSERT INTO `qk_system_auth_node` VALUES (469, 1, 0, 'admin/resource/system_user_balance_log/index');
INSERT INTO `qk_system_auth_node` VALUES (470, 1, 0, 'admin/resource/system_user_info');
INSERT INTO `qk_system_auth_node` VALUES (471, 1, 0, 'admin/resource/system_user_info/index');
INSERT INTO `qk_system_auth_node` VALUES (472, 1, 0, 'admin/resource/system_user_info/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (473, 1, 0, 'admin/resource/system_user_info/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (474, 1, 0, 'admin/resource/system_user_info/user_info_integral_action');
INSERT INTO `qk_system_auth_node` VALUES (475, 1, 0, 'admin/resource/system_user_info/user_info_balance_action');
INSERT INTO `qk_system_auth_node` VALUES (476, 1, 0, 'admin/resource/system_user_info/add_action');
INSERT INTO `qk_system_auth_node` VALUES (477, 1, 0, 'admin/resource/system_user_integral_log');
INSERT INTO `qk_system_auth_node` VALUES (478, 1, 0, 'admin/resource/system_user_integral_log/index');
INSERT INTO `qk_system_auth_node` VALUES (479, 1, 0, 'admin/resource/upgrade');
INSERT INTO `qk_system_auth_node` VALUES (480, 1, 0, 'admin/resource/upgrade/index');
INSERT INTO `qk_system_auth_node` VALUES (481, 1, 0, 'admin/resource/upgrade/upgrade_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (482, 1, 0, 'admin');
INSERT INTO `qk_system_auth_node` VALUES (483, 1, 0, 'admin/index');
INSERT INTO `qk_system_auth_node` VALUES (484, 1, 0, 'admin/index/menu');
INSERT INTO `qk_system_auth_node` VALUES (485, 1, 0, 'admin/index/clearcache');
INSERT INTO `qk_system_auth_node` VALUES (486, 1, 0, 'admin/index/logout');
INSERT INTO `qk_system_auth_node` VALUES (487, 1, 0, 'admin/resource/attachment');
INSERT INTO `qk_system_auth_node` VALUES (488, 1, 0, 'admin/resource/attachment/editcate');
INSERT INTO `qk_system_auth_node` VALUES (489, 1, 0, 'admin/resource/attachment/catelist');
INSERT INTO `qk_system_auth_node` VALUES (490, 1, 0, 'admin/resource/attachment/delcate');
INSERT INTO `qk_system_auth_node` VALUES (491, 1, 0, 'admin/resource/attachment/list');
INSERT INTO `qk_system_auth_node` VALUES (492, 1, 0, 'admin/resource/attachment/delete');
INSERT INTO `qk_system_auth_node` VALUES (493, 1, 0, 'admin/resource/attachment/update');
INSERT INTO `qk_system_auth_node` VALUES (494, 1, 0, 'admin/resource/attachment/recycle');
INSERT INTO `qk_system_auth_node` VALUES (495, 1, 0, 'admin/resource/attachment/movecate');
INSERT INTO `qk_system_auth_node` VALUES (496, 1, 0, 'admin/resource/attachment/upload');
INSERT INTO `qk_system_auth_node` VALUES (497, 1, 0, 'admin/resource/attachment/index');
INSERT INTO `qk_system_auth_node` VALUES (498, 1, 0, 'admin/resource/attachment/attachment_cate_list_action');
INSERT INTO `qk_system_auth_node` VALUES (499, 1, 0, 'admin/resource/admin');
INSERT INTO `qk_system_auth_node` VALUES (500, 1, 0, 'admin/resource/admin/searchuser');
INSERT INTO `qk_system_auth_node` VALUES (501, 1, 0, 'admin/resource/admin/index');
INSERT INTO `qk_system_auth_node` VALUES (502, 1, 0, 'admin/resource/admin/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (503, 1, 0, 'admin/resource/admin/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (504, 1, 0, 'admin/resource/admin/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (505, 1, 0, 'admin/resource/admin/edit_user_password_action');
INSERT INTO `qk_system_auth_node` VALUES (506, 1, 0, 'admin/resource/admin/add_action');
INSERT INTO `qk_system_auth_node` VALUES (507, 1, 0, 'admin/resource/auth');
INSERT INTO `qk_system_auth_node` VALUES (508, 1, 0, 'admin/resource/auth/index');
INSERT INTO `qk_system_auth_node` VALUES (509, 1, 0, 'admin/resource/auth/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (510, 1, 0, 'admin/resource/auth/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (511, 1, 0, 'admin/resource/auth/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (512, 1, 0, 'admin/resource/auth/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (513, 1, 0, 'admin/resource/auth/add_action');
INSERT INTO `qk_system_auth_node` VALUES (514, 1, 0, 'admin/resource/auth/refresh_node_action');
INSERT INTO `qk_system_auth_node` VALUES (515, 1, 0, 'admin/resource/group');
INSERT INTO `qk_system_auth_node` VALUES (516, 1, 0, 'admin/resource/group/index');
INSERT INTO `qk_system_auth_node` VALUES (517, 1, 0, 'admin/resource/group/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (518, 1, 0, 'admin/resource/group/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (519, 1, 0, 'admin/resource/group/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (520, 1, 0, 'admin/resource/group/group_data_action');
INSERT INTO `qk_system_auth_node` VALUES (521, 1, 0, 'admin/resource/group/add_action');
INSERT INTO `qk_system_auth_node` VALUES (522, 1, 0, 'admin/resource/group_data');
INSERT INTO `qk_system_auth_node` VALUES (523, 1, 0, 'admin/resource/group_data/index');
INSERT INTO `qk_system_auth_node` VALUES (524, 1, 0, 'admin/resource/group_data/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (525, 1, 0, 'admin/resource/group_data/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (526, 1, 0, 'admin/resource/group_data/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (527, 1, 0, 'admin/resource/group_data/add_action');
INSERT INTO `qk_system_auth_node` VALUES (528, 1, 0, 'admin/resource/menus');
INSERT INTO `qk_system_auth_node` VALUES (529, 1, 0, 'admin/resource/menus/index');
INSERT INTO `qk_system_auth_node` VALUES (530, 1, 0, 'admin/resource/menus/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (531, 1, 0, 'admin/resource/menus/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (532, 1, 0, 'admin/resource/menus/add_action');
INSERT INTO `qk_system_auth_node` VALUES (533, 1, 0, 'admin/resource/menus/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (534, 1, 0, 'admin/resource/menus/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (535, 1, 0, 'admin/resource/node');
INSERT INTO `qk_system_auth_node` VALUES (536, 1, 0, 'admin/resource/node/index');
INSERT INTO `qk_system_auth_node` VALUES (537, 1, 0, 'admin/resource/node/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (538, 1, 0, 'admin/resource/oplog');
INSERT INTO `qk_system_auth_node` VALUES (539, 1, 0, 'admin/resource/oplog/index');
INSERT INTO `qk_system_auth_node` VALUES (540, 1, 0, 'admin/resource/oplog/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (541, 1, 0, 'admin/resource/oplog/view_action');
INSERT INTO `qk_system_auth_node` VALUES (542, 1, 0, 'admin/resource/admin_setting');
INSERT INTO `qk_system_auth_node` VALUES (543, 1, 0, 'admin/resource/admin_setting/admin_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (544, 1, 0, 'admin/resource/admin_setting/admin_password_action');
INSERT INTO `qk_system_auth_node` VALUES (545, 1, 0, 'admin/resource/config');
INSERT INTO `qk_system_auth_node` VALUES (546, 1, 0, 'admin/resource/config/index');
INSERT INTO `qk_system_auth_node` VALUES (547, 1, 0, 'admin/resource/config/inline_input_column_sort');
INSERT INTO `qk_system_auth_node` VALUES (548, 1, 0, 'admin/resource/config/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (549, 1, 0, 'admin/resource/config/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (550, 1, 0, 'admin/resource/config/add_action');
INSERT INTO `qk_system_auth_node` VALUES (551, 1, 0, 'admin/resource/config/config_group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (552, 1, 0, 'admin/resource/config/config_group_edit_action');
INSERT INTO `qk_system_auth_node` VALUES (553, 1, 0, 'admin/resource/config/config_group_del_action');
INSERT INTO `qk_system_auth_node` VALUES (554, 1, 0, 'admin/resource/config/config_group_list_action');
INSERT INTO `qk_system_auth_node` VALUES (555, 1, 0, 'admin/resource/dashboard');
INSERT INTO `qk_system_auth_node` VALUES (556, 1, 0, 'admin/resource/dashboard/index');
INSERT INTO `qk_system_auth_node` VALUES (557, 1, 0, 'admin/resource/group_new');
INSERT INTO `qk_system_auth_node` VALUES (558, 1, 0, 'admin/resource/group_new/index');
INSERT INTO `qk_system_auth_node` VALUES (559, 1, 0, 'admin/resource/group_new/batch_delete_action');
INSERT INTO `qk_system_auth_node` VALUES (560, 1, 0, 'admin/resource/group_new/switch_column_status');
INSERT INTO `qk_system_auth_node` VALUES (561, 1, 0, 'admin/resource/group_new/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (562, 1, 0, 'admin/resource/group_new/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (563, 1, 0, 'admin/resource/group_new/add_action');
INSERT INTO `qk_system_auth_node` VALUES (564, 1, 0, 'admin/resource/group_new/group_add_action');
INSERT INTO `qk_system_auth_node` VALUES (565, 1, 0, 'admin/resource/plugin');
INSERT INTO `qk_system_auth_node` VALUES (566, 1, 0, 'admin/resource/plugin/index');
INSERT INTO `qk_system_auth_node` VALUES (567, 1, 0, 'admin/resource/plugin/plugin_status_action');
INSERT INTO `qk_system_auth_node` VALUES (568, 1, 0, 'admin/resource/plugin/plugin_update_action');
INSERT INTO `qk_system_auth_node` VALUES (569, 1, 0, 'admin/resource/plugin/plugin_install_action');
INSERT INTO `qk_system_auth_node` VALUES (570, 1, 0, 'admin/resource/plugin/plugin_uninstall_action');
INSERT INTO `qk_system_auth_node` VALUES (571, 1, 0, 'admin/resource/plugin/plugin_user_info_action');
INSERT INTO `qk_system_auth_node` VALUES (572, 1, 0, 'admin/resource/system_config');
INSERT INTO `qk_system_auth_node` VALUES (573, 1, 0, 'admin/resource/system_config/list');
INSERT INTO `qk_system_auth_node` VALUES (574, 1, 0, 'admin/resource/system_config/index');
INSERT INTO `qk_system_auth_node` VALUES (575, 1, 0, 'admin/resource/system_queue');
INSERT INTO `qk_system_auth_node` VALUES (576, 1, 0, 'admin/resource/system_queue/index');
INSERT INTO `qk_system_auth_node` VALUES (577, 1, 0, 'admin/resource/system_queue/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (578, 1, 0, 'admin/resource/system_queue/re_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (579, 1, 0, 'admin/resource/system_queue/queue_log_action');
INSERT INTO `qk_system_auth_node` VALUES (580, 1, 0, 'admin/resource/system_user');
INSERT INTO `qk_system_auth_node` VALUES (581, 1, 0, 'admin/resource/system_user/index');
INSERT INTO `qk_system_auth_node` VALUES (582, 1, 0, 'admin/resource/system_user/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (583, 1, 0, 'admin/resource/system_user/add_action');
INSERT INTO `qk_system_auth_node` VALUES (584, 1, 0, 'admin/resource/system_user_balance_log');
INSERT INTO `qk_system_auth_node` VALUES (585, 1, 0, 'admin/resource/system_user_balance_log/index');
INSERT INTO `qk_system_auth_node` VALUES (586, 1, 0, 'admin/resource/system_user_info');
INSERT INTO `qk_system_auth_node` VALUES (587, 1, 0, 'admin/resource/system_user_info/index');
INSERT INTO `qk_system_auth_node` VALUES (588, 1, 0, 'admin/resource/system_user_info/edit_action');
INSERT INTO `qk_system_auth_node` VALUES (589, 1, 0, 'admin/resource/system_user_info/delete_action');
INSERT INTO `qk_system_auth_node` VALUES (590, 1, 0, 'admin/resource/system_user_info/user_info_integral_action');
INSERT INTO `qk_system_auth_node` VALUES (591, 1, 0, 'admin/resource/system_user_info/user_info_balance_action');
INSERT INTO `qk_system_auth_node` VALUES (592, 1, 0, 'admin/resource/system_user_info/add_action');
INSERT INTO `qk_system_auth_node` VALUES (593, 1, 0, 'admin/resource/system_user_integral_log');
INSERT INTO `qk_system_auth_node` VALUES (594, 1, 0, 'admin/resource/system_user_integral_log/index');
INSERT INTO `qk_system_auth_node` VALUES (595, 1, 0, 'admin/resource/upgrade');
INSERT INTO `qk_system_auth_node` VALUES (596, 1, 0, 'admin/resource/upgrade/index');
INSERT INTO `qk_system_auth_node` VALUES (597, 1, 0, 'admin/resource/upgrade/upgrade_queue_action');
INSERT INTO `qk_system_auth_node` VALUES (598, 1, 0, 'crud');
INSERT INTO `qk_system_auth_node` VALUES (599, 1, 0, 'crud/api');
INSERT INTO `qk_system_auth_node` VALUES (600, 1, 0, 'crud/api/classlist');
INSERT INTO `qk_system_auth_node` VALUES (601, 1, 0, 'crud/api/update');
INSERT INTO `qk_system_auth_node` VALUES (602, 1, 0, 'crud/index');
INSERT INTO `qk_system_auth_node` VALUES (603, 1, 0, 'crud/index/tablefields');
INSERT INTO `qk_system_auth_node` VALUES (604, 1, 0, 'crud/index/tablelist');
INSERT INTO `qk_system_auth_node` VALUES (605, 1, 0, 'crud/index/previewform');
INSERT INTO `qk_system_auth_node` VALUES (606, 1, 0, 'crud/index/previewmodel');
INSERT INTO `qk_system_auth_node` VALUES (607, 1, 0, 'crud/index/previewtable');
INSERT INTO `qk_system_auth_node` VALUES (608, 1, 0, 'crud/index/previewresource');
INSERT INTO `qk_system_auth_node` VALUES (609, 1, 0, 'crud/index/createcrud');
INSERT INTO `qk_system_auth_node` VALUES (610, 1, 0, 'crud/index/previewaction');
INSERT INTO `qk_system_auth_node` VALUES (611, 1, 0, 'crud/index/createaction');
INSERT INTO `qk_system_auth_node` VALUES (612, 1, 0, 'crud/index/createapi');
INSERT INTO `qk_system_auth_node` VALUES (613, 1, 0, 'crud/index/menulist');
INSERT INTO `qk_system_auth_node` VALUES (614, 1, 0, 'crud/resource/index');

-- ----------------------------
-- Table structure for qk_system_config
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_config`;
CREATE TABLE `qk_system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `group` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '分组',
  `plugin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'admin' COMMENT '插件标识',
  `config_type` enum('code','admin') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'admin' COMMENT '配置类型:code=代码配置,admin=后台配置',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '类型:string,text,int,bool,json,datetime,date,file',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置标题',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置简介',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '配置名',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置值',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '变量字典数据',
  `rule` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '验证规则',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:1=显示,0=隐藏',
  `sort` int(10) NOT NULL DEFAULT 1 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_config
-- ----------------------------
INSERT INTO `qk_system_config` VALUES (11, 'storage', 'admin', 'admin', 'radio', '储存类型', '', 'storage_type', 'local', 'local=本地\nalioss=阿里云\r\ntxcos=腾讯云', '', 1, 100);
INSERT INTO `qk_system_config` VALUES (19, 'wxapp', 'admin', 'admin', 'text', '小程序AppId', '', 'appid', 'wxa', '', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (20, 'wxapp', 'admin', 'admin', 'text', '小程序AppSecret', '', 'app_secret', '8287f79885', '', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (21, 'base', 'mall', 'code', 'string', '基础设置', '配置', 'sted', '2', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (22, 'base', 'mall', 'code', 'string', '基础设置', '配置', 'tesst', '3', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (24, 'show', 'mall', 'code', 'string', '显示配置', '显示配置', 'sted', '56', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (25, 'show', 'mall', 'code', 'string', '显示配置', '显示配置', 'tesst', '77', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (26, 'base3', 'mall', 'code', 'string', '支付方式', '支付方式', 'test', '343434', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (27, 'order', 'mall', 'code', 'string', '订单配置', '订单配置', 'sted', '4', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (28, 'order', 'mall', 'code', 'string', '订单配置', '订单配置', 'tesst', '6', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (29, 'system_mail', 'admin', 'admin', 'text', 'SMTP 服务器', '', 'mail_server', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (30, 'system_mail', 'admin', 'admin', 'text', 'SMTP端口', '', 'mail_port', '465', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (31, 'system_mail', 'admin', 'admin', 'text', 'SMTP 用户名', '', 'mail_user', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (32, 'system_mail', 'admin', 'admin', 'radio', 'SMTP 验证方式', '', 'mail_verification', 'ssl', 'ssl=SSL\ntls=TLS\n', '', 1, 1);
INSERT INTO `qk_system_config` VALUES (33, 'system_mail', 'admin', 'admin', 'text', 'SMTP 发件人邮箱', '', 'mail_sender_mail', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (34, 'system_mail', 'admin', 'admin', 'text', 'SMTP 密码', '', 'mail_password', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (42, 'admin', 'admin', 'code', 'string', '数据库版本', '数据库版本,用于标记系统升级版本号', 'quick_sql_version', '1.0.3', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (43, 'base', 'admin', 'code', 'string', '', NULL, 'app_logo', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (44, 'base', 'admin', 'code', 'string', '', NULL, 'loginBanner', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (45, 'base', 'admin', 'code', 'string', '', NULL, 'loginBgImage', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (46, 'base', 'admin', 'code', 'string', '', NULL, 'app_name', 'QuickAdmin', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (47, 'base', 'admin', 'code', 'string', '', NULL, 'app_version', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (48, 'base', 'admin', 'code', 'string', '', NULL, 'copyrightDates', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (49, 'base', 'admin', 'code', 'string', '', NULL, 'copyright', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (50, 'base', 'admin', 'code', 'string', '', NULL, 'website', '', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (51, 'storage', 'admin', 'code', 'string', '', NULL, 'storage_dir_prefix', 'admin', NULL, '', 1, 1);
INSERT INTO `qk_system_config` VALUES (52, 'storage', 'admin', 'code', 'string', '', NULL, 'allow_exts', 'text,zip,png,jpg', NULL, '', 1, 1);

-- ----------------------------
-- Table structure for qk_system_config_group
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_config_group`;
CREATE TABLE `qk_system_config_group`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级ID',
  `group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组变量名称',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分组别名',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态:1=启用,0=禁用',
  `show` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '显示:1=显示,0=隐藏',
  `type` int(2) NULL DEFAULT 0 COMMENT '分组类型',
  `sort` int(11) NOT NULL DEFAULT 100 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统-配置-分组' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_config_group
-- ----------------------------
INSERT INTO `qk_system_config_group` VALUES (3, 0, 'wxapp', '小程序配置', 1, 0, 0, 100);
INSERT INTO `qk_system_config_group` VALUES (31, 0, 'system_mail', '邮箱配置', 1, 0, 0, 100);

-- ----------------------------
-- Table structure for qk_system_group
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_group`;
CREATE TABLE `qk_system_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '组合数据ID',
  `plugin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块插件',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据组名称',
  `info` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据提示',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据字段名称',
  `fields` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据组字段（json数据）',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组合数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_group_data
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_group_data`;
CREATE TABLE `qk_system_group_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `plugin` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块插件',
  `group` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '数据组',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '数据组数据（json数据）',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '数据排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态1：开启；0：关闭',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_plugin_name`(`plugin`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '组合数据详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_jobs
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_jobs`;
CREATE TABLE `qk_system_jobs`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `queue` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '队列名称',
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '有效负载',
  `attempts` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '重试次数',
  `reserved` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订阅次数',
  `reserve_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '订阅时间',
  `available_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效时间',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-消息列的' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_menu
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_menu`;
CREATE TABLE `qk_system_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin_name ',
  `pid` bigint(20) UNSIGNED NULL DEFAULT 0 COMMENT '上级ID',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '菜单图标',
  `badge` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT 'badge',
  `node` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点代码',
  `path` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '链接节点',
  `params` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '_self' COMMENT '打开方式 _blank _self',
  `sort` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '排序权重',
  `is_admin` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '平台菜单 1',
  `level` smallint(5) UNSIGNED NULL DEFAULT 1 COMMENT '管理级别',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_node`(`node`) USING BTREE,
  INDEX `idx_plugin_name`(`plugin_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 174 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_menu
-- ----------------------------
INSERT INTO `qk_system_menu` VALUES (3, 'admin', 64, '菜单管理', 'redenvelope-fill', '', '', 'admin/resource/menus/index', '', '_self', 0, 0, 1, 1, '2021-06-22 22:52:17', '2021-06-22 22:52:17');
INSERT INTO `qk_system_menu` VALUES (10, 'admin', 75, '权限管理', 'el-icon-Coin', '', '', '#', '', '_self', 20, 0, 1, 1, '2022-01-13 11:42:34', '2022-01-13 11:42:34');
INSERT INTO `qk_system_menu` VALUES (14, 'admin', 10, '权限管理', 'radius-bottomright', '', '', 'admin/resource/auth/index', '', '_self', 0, 0, 1, 1, '2021-10-02 10:45:14', '2021-10-02 10:45:14');
INSERT INTO `qk_system_menu` VALUES (16, 'admin', 10, '节点管理', 'interation-fill', '', '', 'admin/resource/node/index', '', '_self', 0, 0, 1, 0, '2022-01-21 14:57:35', '2022-03-15 16:51:22');
INSERT INTO `qk_system_menu` VALUES (23, 'admin', 75, '首页', 'el-icon-house', '', '', 'dashboard', '', '_self', 25, 0, 1, 1, '2022-01-21 14:59:52', '2022-01-21 14:59:52');
INSERT INTO `qk_system_menu` VALUES (27, 'admin', 64, '系统日志', 'deleteteam', '', '', 'admin/resource/oplog/index', '', '_self', 7, 0, 1, 1, '2021-06-22 22:51:58', '2021-06-22 22:51:58');
INSERT INTO `qk_system_menu` VALUES (28, 'admin', 10, '管理员工', 'project-fill', '', '', 'admin/resource/admin/index', '', '_self', 1, 0, 1, 1, '2021-10-01 12:20:30', '2021-10-01 12:20:30');
INSERT INTO `qk_system_menu` VALUES (64, 'admin', 75, '系统配置', 'el-icon-Setting', '', '', '#', '', '_self', 22, 0, 1, 1, '2022-01-13 11:42:04', '2022-01-13 11:42:04');
INSERT INTO `qk_system_menu` VALUES (65, 'admin', 64, '系统配置', 'border-inner', '', '', 'admin/resource/system_config/index', '', '_self', 1, 0, 1, 1, '2021-10-07 20:55:34', '2021-10-07 20:55:34');
INSERT INTO `qk_system_menu` VALUES (67, 'admin', 84, '系统任务', '', '', '', 'admin/resource/system_queue/index', '', '_self', 1, 0, 1, 1, '2021-10-03 16:06:37', '2022-04-17 21:58:49');
INSERT INTO `qk_system_menu` VALUES (69, 'admin', 84, '组合配置', '', '', '', 'admin/resource/group_new/index', '', '_self', 1, 0, 1, 1, '2022-01-20 14:31:58', '2022-01-20 14:31:58');
INSERT INTO `qk_system_menu` VALUES (70, 'admin', 64, '组合数据', 'border-verticle', '', '', 'admin/resource/group/index', '', '_self', 1, 0, 1, 0, '2021-12-27 16:29:20', '2021-12-27 16:29:20');
INSERT INTO `qk_system_menu` VALUES (75, 'admin', 0, '系统管理', 'el-icon-Setting', '', '', 'admin', '', '_self', 400, 0, 1, 1, '2022-01-23 21:59:51', '2022-01-23 21:59:51');
INSERT INTO `qk_system_menu` VALUES (84, 'admin', 75, '系统维护', 'el-icon-Setting', '', '', '#', '', '_self', 1, 0, 1, 1, '2022-01-04 20:28:26', '2022-01-04 20:28:26');
INSERT INTO `qk_system_menu` VALUES (85, 'admin', 84, '代码crud', '', '', '', 'crud/resource/index/index', '', '_self', 4, 0, 1, 1, '2022-01-17 16:58:27', '2022-01-17 16:58:27');
INSERT INTO `qk_system_menu` VALUES (87, 'admin', 84, '系统配置', '', '', '', 'admin/resource/config/index', '', '_self', 1, 0, 1, 1, '2022-01-19 09:12:28', '2022-12-06 16:35:00');
INSERT INTO `qk_system_menu` VALUES (88, 'admin', 75, '会员管理', 'el-icon-User', '', '', 'admin/resource/system_user_info/index', '', '_self', 1, 0, 1, 1, '2022-01-28 17:29:00', '2023-03-07 10:55:44');
INSERT INTO `qk_system_menu` VALUES (167, 'admin', 84, '系统更新', '', '', '', 'admin/resource/upgrade/index', '', '_self', 1, 0, 1, 1, '2022-04-17 21:58:26', '2022-04-17 21:58:26');
INSERT INTO `qk_system_menu` VALUES (168, 'admin', 88, '会员列表', '', '', '', 'admin/resource/system_user_info/index', '', '_self', 1, 0, 1, 1, '2023-03-07 10:56:14', '2023-03-07 10:56:14');
INSERT INTO `qk_system_menu` VALUES (169, 'admin', 88, ' 积分记录', '', '', '', 'admin/resource/system_user_integral_log/index 1', '', '_self', 1, 0, 1, 1, '2023-03-07 10:56:27', '2023-03-07 10:56:27');
INSERT INTO `qk_system_menu` VALUES (170, 'admin', 88, ' 余额收支', '', '', '', 'admin/resource/system_user_balance_log/index', '', '_self', 1, 0, 1, 1, '2023-03-07 10:56:43', '2023-03-07 10:56:43');
INSERT INTO `qk_system_menu` VALUES (171, 'admin', 75, ' 插件管理', 'el-icon-Sell', 'hot', '', 'admin/resource/plugin/index', '', '_self', 1, 0, 1, 1, '2023-03-07 11:09:36', '2023-03-08 09:59:14');
INSERT INTO `qk_system_menu` VALUES (172, 'admin', 84, '支付方式', '', '', '', 'admin/resource/pay_support/index', '', '_self', 1, 0, 1, 1, '2023-03-31 17:01:42', '2023-03-31 17:01:42');
INSERT INTO `qk_system_menu` VALUES (173, 'admin', 84, '上传配置', '', '', '', 'admin/resource/disk/index', '', '_self', 1, 0, 1, 1, '2023-03-31 17:02:29', '2023-03-31 17:02:29');

-- ----------------------------
-- Table structure for qk_system_node
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_node`;
CREATE TABLE `qk_system_node`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin',
  `node` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '节点规则',
  `pnode` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '父节点',
  `mode` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点类型:controller、resource',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求类型json',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `level` tinyint(1) NOT NULL DEFAULT 1 COMMENT '节点层级',
  `is_menu` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否可设置为菜单',
  `is_auth` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动RBAC权限控制',
  `is_login` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '是否启动登录控制',
  `is_log` tinyint(1) NULL DEFAULT 0 COMMENT '记录日志',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `sort` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '排序',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `node`(`node`) USING BTREE,
  INDEX `idx_node`(`node`) USING BTREE,
  INDEX `idx_plugin_id`(`plugin_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 161 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '节点表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_node
-- ----------------------------
INSERT INTO `qk_system_node` VALUES (1, 'admin', 'admin', '', 'resource', '系统管理', '[]', '', '', 1, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (2, 'admin', 'admin/index', 'admin', 'controller', '系统管理', '[]', '', '', 2, 1, 1, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (3, 'admin', 'admin/index/index', 'admin/index', 'controller', 'Index', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (4, 'admin', 'admin/index/menu', 'admin/index', 'controller', '菜单', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (5, 'admin', 'admin/index/clearcache', 'admin/index', 'controller', '清除缓存', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (6, 'admin', 'admin/index/logout', 'admin/index', 'controller', '退出登录', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (7, 'admin', 'admin/passport', 'admin', 'controller', '登录系统', '[]', '', '', 2, 1, 0, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (8, 'admin', 'admin/passport/login', 'admin/passport', 'controller', '登录', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2023-03-06 17:25:39');
INSERT INTO `qk_system_node` VALUES (9, 'admin', 'admin/passport/userinfo', 'admin/passport', 'controller', '员工数据', '[]', '', '', 3, 0, 0, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (10, 'admin', 'admin/update', 'admin', 'controller', '升级管理', '[]', '', '', 2, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (11, 'admin', 'admin/update/get', 'admin/update', 'controller', '升级管理', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (12, 'admin', 'admin/update/node', 'admin/update', 'controller', '升级管理', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (13, 'admin', 'admin/update/version', 'admin/update', 'controller', '升级管理1', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (14, 'crud', 'crud', '', 'resource', 'CRUD工具', '[]', '', '', 1, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (15, 'crud', 'crud/api', 'crud', 'controller', 'CRUD工具', '[]', '', '', 2, 1, 1, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (16, 'crud', 'crud/api/classlist', 'crud/api', 'controller', 'classList', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (17, 'crud', 'crud/api/update', 'crud/api', 'controller', 'update', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (18, 'crud', 'crud/index', 'crud', 'controller', 'CRUD工具', '[]', '', '', 2, 1, 1, 0, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (19, 'crud', 'crud/index/tablefields', 'crud/index', 'controller', '获取表字段', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (20, 'crud', 'crud/index/tablelist', 'crud/index', 'controller', '获取表列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (21, 'crud', 'crud/index/previewform', 'crud/index', 'controller', '预览表单', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (22, 'crud', 'crud/index/previewmodel', 'crud/index', 'controller', '预览Model', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (23, 'crud', 'crud/index/previewtable', 'crud/index', 'controller', '预览Table', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (24, 'crud', 'crud/index/previewresource', 'crud/index', 'controller', '预览Resource', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (25, 'crud', 'crud/index/createcrud', 'crud/index', 'controller', '创建CRUD', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (26, 'crud', 'crud/index/previewaction', 'crud/index', 'controller', '预览Servcie', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (27, 'crud', 'crud/index/createaction', 'crud/index', 'controller', '创建Servcie', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (28, 'crud', 'crud/index/createapi', 'crud/index', 'controller', '创建Api', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (29, 'crud', 'crud/index/menulist', 'crud/index', 'controller', '菜单', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-18 23:06:34');
INSERT INTO `qk_system_node` VALUES (30, 'admin', 'admin/resource/attachment', 'admin', 'resource', '素材管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (31, 'admin', 'admin/resource/attachment/editcate', 'admin/resource/attachment', 'resource', '编辑分类', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (32, 'admin', 'admin/resource/attachment/catelist', 'admin/resource/attachment', 'resource', '分类列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (33, 'admin', 'admin/resource/attachment/delcate', 'admin/resource/attachment', 'resource', '删除分类', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (34, 'admin', 'admin/resource/attachment/list', 'admin/resource/attachment', 'resource', '素材列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (35, 'admin', 'admin/resource/attachment/delete', 'admin/resource/attachment', 'resource', '删除图片', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (36, 'admin', 'admin/resource/attachment/update', 'admin/resource/attachment', 'resource', '修改图片名称', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (37, 'admin', 'admin/resource/attachment/recycle', 'admin/resource/attachment', 'resource', '放入回收站', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (38, 'admin', 'admin/resource/attachment/movecate', 'admin/resource/attachment', 'resource', '迁移', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (39, 'admin', 'admin/resource/attachment/upload', 'admin/resource/attachment', 'resource', '上传', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (40, 'admin', 'admin/resource/attachment/index', 'admin/resource/attachment', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (41, 'admin', 'admin/resource/attachment/attachment_cate_list_action', 'admin/resource/attachment', 'resource', '设置密码', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (42, 'admin', 'admin/resource/admin', 'admin', 'resource', '系统管理员', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (43, 'admin', 'admin/resource/admin/searchuser', 'admin/resource/admin', 'resource', '搜索会员', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (44, 'admin', 'admin/resource/admin/index', 'admin/resource/admin', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (45, 'admin', 'admin/resource/admin/switch_column_status', 'admin/resource/admin', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (46, 'admin', 'admin/resource/admin/edit_action', 'admin/resource/admin', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (47, 'admin', 'admin/resource/admin/delete_action', 'admin/resource/admin', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (48, 'admin', 'admin/resource/admin/edit_user_password_action', 'admin/resource/admin', 'resource', '设置密码', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (49, 'admin', 'admin/resource/admin/add_action', 'admin/resource/admin', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (50, 'admin', 'admin/resource/auth', 'admin', 'resource', '权限管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (51, 'admin', 'admin/resource/auth/index', 'admin/resource/auth', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (52, 'admin', 'admin/resource/auth/batch_delete_action', 'admin/resource/auth', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (53, 'admin', 'admin/resource/auth/switch_column_status', 'admin/resource/auth', 'resource', '状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (54, 'admin', 'admin/resource/auth/edit_action', 'admin/resource/auth', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (55, 'admin', 'admin/resource/auth/delete_action', 'admin/resource/auth', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (56, 'admin', 'admin/resource/auth/add_action', 'admin/resource/auth', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (57, 'admin', 'admin/resource/auth/refresh_node_action', 'admin/resource/auth', 'resource', '刷新权限', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (58, 'admin', 'admin/resource/group', 'admin', 'resource', '配置组合', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (59, 'admin', 'admin/resource/group/index', 'admin/resource/group', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (60, 'admin', 'admin/resource/group/batch_delete_action', 'admin/resource/group', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (61, 'admin', 'admin/resource/group/edit_action', 'admin/resource/group', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (62, 'admin', 'admin/resource/group/delete_action', 'admin/resource/group', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (63, 'admin', 'admin/resource/group/group_data_action', 'admin/resource/group', 'resource', '组合数据', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (64, 'admin', 'admin/resource/group/add_action', 'admin/resource/group', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (65, 'admin', 'admin/resource/group_data', 'admin', 'resource', '组合数据', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (66, 'admin', 'admin/resource/group_data/index', 'admin/resource/group_data', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (67, 'admin', 'admin/resource/group_data/batch_delete_action', 'admin/resource/group_data', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (68, 'admin', 'admin/resource/group_data/edit_action', 'admin/resource/group_data', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (69, 'admin', 'admin/resource/group_data/delete_action', 'admin/resource/group_data', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (70, 'admin', 'admin/resource/group_data/add_action', 'admin/resource/group_data', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (71, 'admin', 'admin/resource/menus', 'admin', 'resource', '菜单管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (72, 'admin', 'admin/resource/menus/index', 'admin/resource/menus', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (73, 'admin', 'admin/resource/menus/inline_input_column_sort', 'admin/resource/menus', 'resource', '排序', '[]', '', '', 3, 0, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (74, 'admin', 'admin/resource/menus/switch_column_status', 'admin/resource/menus', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (75, 'admin', 'admin/resource/menus/add_action', 'admin/resource/menus', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (76, 'admin', 'admin/resource/menus/edit_action', 'admin/resource/menus', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (77, 'admin', 'admin/resource/menus/delete_action', 'admin/resource/menus', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (78, 'admin', 'admin/resource/node', 'admin', 'resource', '节点管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (79, 'admin', 'admin/resource/node/index', 'admin/resource/node', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (80, 'admin', 'admin/resource/node/batch_delete_action', 'admin/resource/node', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (81, 'admin', 'admin/resource/oplog', 'admin', 'resource', '系统日志', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (82, 'admin', 'admin/resource/oplog/index', 'admin/resource/oplog', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (83, 'admin', 'admin/resource/oplog/batch_delete_action', 'admin/resource/oplog', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (84, 'admin', 'admin/resource/oplog/view_action', 'admin/resource/oplog', 'resource', 'View', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:29:56', '2022-12-12 16:29:56');
INSERT INTO `qk_system_node` VALUES (85, 'admin', 'admin/resource/admin_setting', 'admin', 'resource', '个人中心', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (86, 'admin', 'admin/resource/admin_setting/index', 'admin/resource/admin_setting', 'resource', '列表', '[]', '', '', 3, 1, 0, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (87, 'admin', 'admin/resource/admin_setting/list', 'admin/resource/admin_setting', 'resource', '保存参数', '[]', '', '', 3, 1, 0, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (88, 'admin', 'admin/resource/admin_setting/admin_edit_action', 'admin/resource/admin_setting', 'resource', '基本信息', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (89, 'admin', 'admin/resource/admin_setting/admin_password_action', 'admin/resource/admin_setting', 'resource', '设置密码', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (90, 'admin', 'admin/resource/config', 'admin', 'resource', '系统配置', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (91, 'admin', 'admin/resource/config/index', 'admin/resource/config', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (92, 'admin', 'admin/resource/config/inline_input_column_sort', 'admin/resource/config', 'resource', '排序', '[]', '', '', 3, 0, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (93, 'admin', 'admin/resource/config/edit_action', 'admin/resource/config', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (94, 'admin', 'admin/resource/config/delete_action', 'admin/resource/config', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (95, 'admin', 'admin/resource/config/add_action', 'admin/resource/config', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (96, 'admin', 'admin/resource/config/config_group_add_action', 'admin/resource/config', 'resource', '添加配置分组', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (97, 'admin', 'admin/resource/config/config_group_edit_action', 'admin/resource/config', 'resource', '编辑配置分组', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (98, 'admin', 'admin/resource/config/config_group_del_action', 'admin/resource/config', 'resource', '删除配置分组', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (99, 'admin', 'admin/resource/config/config_group_list_action', 'admin/resource/config', 'resource', '配置分组列表', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (100, 'admin', 'admin/resource/dashboard', 'admin', 'resource', '系统首页', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (101, 'admin', 'admin/resource/dashboard/index', 'admin/resource/dashboard', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (102, 'admin', 'admin/resource/group_new', 'admin', 'resource', '配置组合', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (103, 'admin', 'admin/resource/group_new/index', 'admin/resource/group_new', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (104, 'admin', 'admin/resource/group_new/batch_delete_action', 'admin/resource/group_new', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (105, 'admin', 'admin/resource/group_new/switch_column_status', 'admin/resource/group_new', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (106, 'admin', 'admin/resource/group_new/edit_action', 'admin/resource/group_new', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (107, 'admin', 'admin/resource/group_new/delete_action', 'admin/resource/group_new', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (108, 'admin', 'admin/resource/group_new/add_action', 'admin/resource/group_new', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (109, 'admin', 'admin/resource/group_new/group_add_action', 'admin/resource/group_new', 'resource', '添加数据组', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (110, 'admin', 'admin/resource/plugin', 'admin', 'resource', '插件管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (111, 'admin', 'admin/resource/plugin/index', 'admin/resource/plugin', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (112, 'admin', 'admin/resource/plugin/plugin_status_action', 'admin/resource/plugin', 'resource', '插件状态', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (113, 'admin', 'admin/resource/plugin/plugin_update_action', 'admin/resource/plugin', 'resource', '升级插件', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (114, 'admin', 'admin/resource/plugin/plugin_install_action', 'admin/resource/plugin', 'resource', '安装插件', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (115, 'admin', 'admin/resource/plugin/plugin_uninstall_action', 'admin/resource/plugin', 'resource', '卸载插件', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (116, 'admin', 'admin/resource/plugin/plugin_user_info_action', 'admin/resource/plugin', 'resource', '插件会员信息', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (117, 'admin', 'admin/resource/system_config', 'admin', 'resource', '系统参数', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (118, 'admin', 'admin/resource/system_config/list', 'admin/resource/system_config', 'resource', '系统设置', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (119, 'admin', 'admin/resource/system_config/index', 'admin/resource/system_config', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (120, 'admin', 'admin/resource/system_queue', 'admin', 'resource', '系统任务', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (121, 'admin', 'admin/resource/system_queue/index', 'admin/resource/system_queue', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (122, 'admin', 'admin/resource/system_queue/delete_action', 'admin/resource/system_queue', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (123, 'admin', 'admin/resource/system_queue/re_queue_action', 'admin/resource/system_queue', 'resource', '重置任务', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (124, 'admin', 'admin/resource/system_queue/queue_log_action', 'admin/resource/system_queue', 'resource', '任务状态', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (125, 'admin', 'admin/resource/system_user', 'admin', 'resource', '账户管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (126, 'admin', 'admin/resource/system_user/index', 'admin/resource/system_user', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (127, 'admin', 'admin/resource/system_user/edit_action', 'admin/resource/system_user', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (128, 'admin', 'admin/resource/system_user/add_action', 'admin/resource/system_user', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (129, 'admin', 'admin/resource/system_user_balance_log', 'admin', 'resource', '余额记录', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (130, 'admin', 'admin/resource/system_user_balance_log/index', 'admin/resource/system_user_balance_log', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (131, 'admin', 'admin/resource/system_user_info', 'admin', 'resource', '会员管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (132, 'admin', 'admin/resource/system_user_info/index', 'admin/resource/system_user_info', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (133, 'admin', 'admin/resource/system_user_info/edit_action', 'admin/resource/system_user_info', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (134, 'admin', 'admin/resource/system_user_info/delete_action', 'admin/resource/system_user_info', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (135, 'admin', 'admin/resource/system_user_info/user_info_integral_action', 'admin/resource/system_user_info', 'resource', '会员积分设置', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (136, 'admin', 'admin/resource/system_user_info/user_info_balance_action', 'admin/resource/system_user_info', 'resource', '会员余额设置', '[]', '', '', 3, 0, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (138, 'admin', 'admin/resource/system_user_integral_log', 'admin', 'resource', '积分记录', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (139, 'admin', 'admin/resource/system_user_integral_log/index', 'admin/resource/system_user_integral_log', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (140, 'admin', 'admin/resource/upgrade', 'admin', 'resource', '升级管理', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (141, 'admin', 'admin/resource/upgrade/index', 'admin/resource/upgrade', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (142, 'admin', 'admin/resource/upgrade/upgrade_queue_action', 'admin/resource/upgrade', 'resource', '更新系统', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (143, 'crud', 'crud/resource/index', 'crud', 'resource', '首页', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (144, 'crud', 'crud/resource/index/index', 'crud/resource/index', 'resource', '数据列表', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2022-12-12 16:32:30', '2022-12-12 16:32:30');
INSERT INTO `qk_system_node` VALUES (145, 'admin', 'admin/index/test', 'admin/index', 'controller', '菜单', '[]', '', '', 3, 0, 0, 0, 0, 1, 0, '2023-03-31 16:23:11', '2023-03-31 16:23:11');
INSERT INTO `qk_system_node` VALUES (146, 'admin', 'admin/resource/disk', 'admin', 'resource', '上传配置', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (147, 'admin', 'admin/resource/disk/index', 'admin/resource/disk', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (148, 'admin', 'admin/resource/disk/switch_column_status', 'admin/resource/disk', 'resource', '状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (149, 'admin', 'admin/resource/disk/edit_action', 'admin/resource/disk', 'resource', '编辑', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (150, 'admin', 'admin/resource/disk/delete_action', 'admin/resource/disk', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (151, 'admin', 'admin/resource/disk/storage_config_action', 'admin/resource/disk', 'resource', '配置上传', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (152, 'admin', 'admin/resource/disk/add_action', 'admin/resource/disk', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (153, 'admin', 'admin/resource/pay_support', 'admin', 'resource', '支付方式', '[]', '', '', 2, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (154, 'admin', 'admin/resource/pay_support/index', 'admin/resource/pay_support', 'resource', '列表', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (155, 'admin', 'admin/resource/pay_support/batch_delete_action', 'admin/resource/pay_support', 'resource', '批量删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (156, 'admin', 'admin/resource/pay_support/switch_column_status', 'admin/resource/pay_support', 'resource', '启用状态', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (157, 'admin', 'admin/resource/pay_support/delete_action', 'admin/resource/pay_support', 'resource', '删除', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (158, 'admin', 'admin/resource/pay_support/setting_pay_action', 'admin/resource/pay_support', 'resource', '配置支付', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (159, 'admin', 'admin/resource/pay_support/add_action', 'admin/resource/pay_support', 'resource', '添加', '[]', '', '', 3, 1, 1, 1, 1, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');
INSERT INTO `qk_system_node` VALUES (160, 'admin', 'admin/resource/plugin/plugin_config_action', 'admin/resource/plugin', 'resource', '插件设置', '[]', '', '', 3, 1, 1, 1, 0, 1, 0, '2023-03-31 17:13:45', '2023-03-31 17:13:45');

-- ----------------------------
-- Table structure for qk_system_oplog
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_oplog`;
CREATE TABLE `qk_system_oplog`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT 0 COMMENT '日志类型:0=后台管理,1=用户接口',
  `plugin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块',
  `node` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `geoip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `action` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作行为名称',
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '账户id',
  `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作内容描述',
  `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数',
  `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求方法',
  `json_result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '放回数据',
  `error_msg` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误信息',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_oplog
-- ----------------------------
INSERT INTO `qk_system_oplog` VALUES (1, 0, '', 'admin/passport/login', '127.0.0.1', '登录系统', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36 Edg/110.0.1587.63', '/index.php/admin/passport/login', 'admin', '[]', '', '[]', '', '2023-03-09 10:29:22');
INSERT INTO `qk_system_oplog` VALUES (2, 0, '', 'admin/passport/login', '127.0.0.1', '登录系统', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/passport/login', 'admin', '[]', '', '[]', '', '2023-03-31 16:22:37');
INSERT INTO `qk_system_oplog` VALUES (3, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=5', 'admin', '{\"_keyValues_\":\"5\",\"isTrusted\":true,\"_vts\":1680252471355,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:47:52');
INSERT INTO `qk_system_oplog` VALUES (4, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=6', 'admin', '{\"_keyValues_\":\"6\",\"isTrusted\":true,\"_vts\":1680252474872,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:47:56');
INSERT INTO `qk_system_oplog` VALUES (5, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=7', 'admin', '{\"_keyValues_\":\"7\",\"isTrusted\":true,\"_vts\":1680252477782,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:47:59');
INSERT INTO `qk_system_oplog` VALUES (6, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=8', 'admin', '{\"_keyValues_\":\"8\",\"isTrusted\":true,\"_vts\":1680252480370,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:01');
INSERT INTO `qk_system_oplog` VALUES (7, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=10', 'admin', '{\"_keyValues_\":\"10\",\"isTrusted\":true,\"_vts\":1680252482634,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:03');
INSERT INTO `qk_system_oplog` VALUES (8, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=9', 'admin', '{\"_keyValues_\":\"9\",\"isTrusted\":true,\"_vts\":1680252485125,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:06');
INSERT INTO `qk_system_oplog` VALUES (9, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=36', 'admin', '{\"_keyValues_\":\"36\",\"isTrusted\":true,\"_vts\":1680252494199,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:15');
INSERT INTO `qk_system_oplog` VALUES (10, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=37', 'admin', '{\"_keyValues_\":\"37\",\"isTrusted\":true,\"_vts\":1680252497297,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:18');
INSERT INTO `qk_system_oplog` VALUES (11, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=38', 'admin', '{\"_keyValues_\":\"38\",\"isTrusted\":true,\"_vts\":1680252500237,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:21');
INSERT INTO `qk_system_oplog` VALUES (12, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=39', 'admin', '{\"_keyValues_\":\"39\",\"isTrusted\":true,\"_vts\":1680252502556,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:23');
INSERT INTO `qk_system_oplog` VALUES (13, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=40', 'admin', '{\"_keyValues_\":\"40\",\"isTrusted\":true,\"_vts\":1680252505112,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:26');
INSERT INTO `qk_system_oplog` VALUES (14, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=41', 'admin', '{\"_keyValues_\":\"41\",\"isTrusted\":true,\"_vts\":1680252507332,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 16:48:28');
INSERT INTO `qk_system_oplog` VALUES (15, 0, 'admin', 'admin/resource/menus/add_action', '127.0.0.1', '菜单管理-添加', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/menus/add_action/store', 'admin', '{\"pid\":84,\"title\":\"\\u652f\\u4ed8\\u65b9\\u5f0f\",\"path\":\"admin\\/resource\\/pay_support\\/index\",\"params\":\"\",\"node\":\"\",\"icon\":[],\"badge\":\"\",\"sort\":1,\"resource\":\"menus\",\"action\":\"add_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"menus\",\"action\":\"add_action\",\"func\":\"store\"}]', '2023-03-31 17:01:41');
INSERT INTO `qk_system_oplog` VALUES (16, 0, 'admin', 'admin/resource/menus/add_action', '127.0.0.1', '菜单管理-添加', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/menus/add_action/store', 'admin', '{\"pid\":84,\"title\":\"\\u4e0a\\u4f20\\u914d\\u7f6e\",\"path\":\"admin\\/resource\\/disk\\/index\",\"params\":\"\",\"node\":\"\",\"icon\":[],\"badge\":\"\",\"sort\":1,\"resource\":\"menus\",\"action\":\"add_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"menus\",\"action\":\"add_action\",\"func\":\"store\"}]', '2023-03-31 17:02:29');
INSERT INTO `qk_system_oplog` VALUES (17, 0, 'admin', 'admin/resource/disk/add_action', '127.0.0.1', 'admin/resource/disk/add_action', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/add_action/store', 'admin', '{\"type\":\"local\",\"desc\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"resource\":\"disk\",\"action\":\"add_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"disk\",\"action\":\"add_action\",\"func\":\"store\"}]', '2023-03-31 17:02:47');
INSERT INTO `qk_system_oplog` VALUES (18, 0, 'admin', 'admin/resource/disk/index', '127.0.0.1', 'admin/resource/disk/index', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/index', 'admin', '{\"spm\":\"m-75-84-173\",\"per_page\":0,\"page\":1,\"resource\":\"disk\",\"action\":\"index\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":{\"per_page\":20,\"total\":1,\"current_page\":1,\"data\":[{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":1,\"originalValue\":1,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"local\",\"originalValue\":\"local\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"originalValue\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":0,\"originalValue\":0,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":1,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":0,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":0,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:47\",\"originalValue\":\"2023-03-31 17:02:47\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=1\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":1,\"storage_id\":1,\"desc\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"type\":\"local\",\"config\":\"\",\"status\":0,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:47\",\"updated_at\":\"2023-03-31 17:02:47\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":1,\"originalValue\":1,\"display\":false}}]}}', '[{\"resource\":\"disk\",\"action\":\"index\"}]', '2023-03-31 17:02:47');
INSERT INTO `qk_system_oplog` VALUES (19, 0, 'admin', 'admin/resource/disk/add_action', '127.0.0.1', 'admin/resource/disk/add_action', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/add_action/store', 'admin', '{\"type\":\"alioss\",\"desc\":\"\\u963f\\u91cc\\u4e91\",\"resource\":\"disk\",\"action\":\"add_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"disk\",\"action\":\"add_action\",\"func\":\"store\"}]', '2023-03-31 17:02:59');
INSERT INTO `qk_system_oplog` VALUES (20, 0, 'admin', 'admin/resource/disk/index', '127.0.0.1', 'admin/resource/disk/index', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/index', 'admin', '{\"spm\":\"m-75-84-173\",\"per_page\":0,\"page\":1,\"resource\":\"disk\",\"action\":\"index\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":{\"per_page\":20,\"total\":2,\"current_page\":1,\"data\":[{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":2,\"originalValue\":2,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"alioss\",\"originalValue\":\"alioss\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u963f\\u91cc\\u4e91\",\"originalValue\":\"\\u963f\\u91cc\\u4e91\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":0,\"originalValue\":0,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":2,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":0,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":0,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:59\",\"originalValue\":\"2023-03-31 17:02:59\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=2\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=2\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=2\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":2,\"storage_id\":1,\"desc\":\"\\u963f\\u91cc\\u4e91\",\"type\":\"alioss\",\"config\":\"\",\"status\":0,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:59\",\"updated_at\":\"2023-03-31 17:02:59\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":2,\"originalValue\":2,\"display\":false}},{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":1,\"originalValue\":1,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"local\",\"originalValue\":\"local\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"originalValue\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":0,\"originalValue\":0,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":1,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":0,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":0,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:47\",\"originalValue\":\"2023-03-31 17:02:47\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=1\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":1,\"storage_id\":1,\"desc\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"type\":\"local\",\"config\":\"\",\"status\":0,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:47\",\"updated_at\":\"2023-03-31 17:02:47\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":1,\"originalValue\":1,\"display\":false}}]}}', '[{\"resource\":\"disk\",\"action\":\"index\"}]', '2023-03-31 17:03:00');
INSERT INTO `qk_system_oplog` VALUES (21, 0, 'admin', 'admin/resource/disk/switch_column_status', '127.0.0.1', 'admin/resource/disk/switch_column_status', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/switch_column_status/store?_action=handle', 'admin', '{\"_action\":\"handle\",\"_keyValue_\":2,\"_colValue_\":1,\"resource\":\"disk\",\"action\":\"switch_column_status\",\"func\":\"store\"}', 'POST', '{\"msg\":\"\\u4fee\\u6539\\u6210\\u529f\",\"code\":0,\"data\":[]}', '[{\"resource\":\"disk\",\"action\":\"switch_column_status\",\"func\":\"store\"}]', '2023-03-31 17:03:21');
INSERT INTO `qk_system_oplog` VALUES (22, 0, 'admin', 'admin/resource/disk/index', '127.0.0.1', 'admin/resource/disk/index', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/index', 'admin', '{\"spm\":\"m-75-84-173\",\"per_page\":0,\"page\":1,\"resource\":\"disk\",\"action\":\"index\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":{\"per_page\":20,\"total\":2,\"current_page\":1,\"data\":[{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":2,\"originalValue\":2,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"alioss\",\"originalValue\":\"alioss\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u963f\\u91cc\\u4e91\",\"originalValue\":\"\\u963f\\u91cc\\u4e91\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":1,\"originalValue\":1,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":2,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":1,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":1,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:59\",\"originalValue\":\"2023-03-31 17:02:59\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=2\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=2\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=2\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":2,\"storage_id\":1,\"desc\":\"\\u963f\\u91cc\\u4e91\",\"type\":\"alioss\",\"config\":\"\",\"status\":1,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:59\",\"updated_at\":\"2023-03-31 17:03:21\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":2,\"originalValue\":2,\"display\":false}},{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":1,\"originalValue\":1,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"local\",\"originalValue\":\"local\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"originalValue\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":0,\"originalValue\":0,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":1,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":0,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":0,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:47\",\"originalValue\":\"2023-03-31 17:02:47\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=1\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":1,\"storage_id\":1,\"desc\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"type\":\"local\",\"config\":\"\",\"status\":0,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:47\",\"updated_at\":\"2023-03-31 17:02:47\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":1,\"originalValue\":1,\"display\":false}}]}}', '[{\"resource\":\"disk\",\"action\":\"index\"}]', '2023-03-31 17:03:21');
INSERT INTO `qk_system_oplog` VALUES (23, 0, 'admin', 'admin/resource/disk/switch_column_status', '127.0.0.1', 'admin/resource/disk/switch_column_status', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/switch_column_status/store?_action=handle', 'admin', '{\"_action\":\"handle\",\"_keyValue_\":1,\"_colValue_\":1,\"resource\":\"disk\",\"action\":\"switch_column_status\",\"func\":\"store\"}', 'POST', '{\"msg\":\"\\u4fee\\u6539\\u6210\\u529f\",\"code\":0,\"data\":[]}', '[{\"resource\":\"disk\",\"action\":\"switch_column_status\",\"func\":\"store\"}]', '2023-03-31 17:03:22');
INSERT INTO `qk_system_oplog` VALUES (24, 0, 'admin', 'admin/resource/disk/index', '127.0.0.1', 'admin/resource/disk/index', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/disk/index', 'admin', '{\"spm\":\"m-75-84-173\",\"per_page\":0,\"page\":1,\"resource\":\"disk\",\"action\":\"index\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":{\"per_page\":20,\"total\":2,\"current_page\":1,\"data\":[{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":2,\"originalValue\":2,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"alioss\",\"originalValue\":\"alioss\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u963f\\u91cc\\u4e91\",\"originalValue\":\"\\u963f\\u91cc\\u4e91\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":0,\"originalValue\":0,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":2,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":0,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":0,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:59\",\"originalValue\":\"2023-03-31 17:02:59\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=2\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=2\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=2\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":2,\"storage_id\":1,\"desc\":\"\\u963f\\u91cc\\u4e91\",\"type\":\"alioss\",\"config\":\"\",\"status\":0,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:59\",\"updated_at\":\"2023-03-31 17:03:22\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":2,\"originalValue\":2,\"display\":false}},{\"id\":{\"title\":\"Id\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":1,\"originalValue\":1,\"display\":false},\"type\":{\"title\":\"\\u7c7b\\u578b\",\"name\":\"type\",\"uriKey\":\"column_type\",\"value\":\"local\",\"originalValue\":\"local\",\"display\":false},\"desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"desc\",\"uriKey\":\"column_desc\",\"value\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"originalValue\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"display\":false},\"status\":{\"title\":\"\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":1,\"originalValue\":1,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":1,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/disk\\/switch_column_status\\/load\",\"value\":1,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":1,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"created_at\":{\"title\":\"\\u521b\\u5efa\\u65e5\\u671f\",\"name\":\"created_at\",\"uriKey\":\"column_created_at\",\"value\":\"2023-03-31 17:02:47\",\"originalValue\":\"2023-03-31 17:02:47\",\"display\":false},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/edit_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u7f16\\u8f91\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"750px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"\\u7f16\\u8f91\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u7f16\\u8f91\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"edit_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/disk\\/storage_config_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"storage_config_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/disk\\/delete_action\\/store?_keyValues_=1\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":1,\"storage_id\":1,\"desc\":\"\\u672c\\u5730\\u4e0a\\u4f20\",\"type\":\"local\",\"config\":\"\",\"status\":1,\"is_deleted\":0,\"deleted_at\":0,\"created_at\":\"2023-03-31 17:02:47\",\"updated_at\":\"2023-03-31 17:03:22\"},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":1,\"originalValue\":1,\"display\":false}}]}}', '[{\"resource\":\"disk\",\"action\":\"index\"}]', '2023-03-31 17:03:22');
INSERT INTO `qk_system_oplog` VALUES (28, 0, 'admin', 'admin/resource/pay_support/add_action', '127.0.0.1', 'admin/resource/pay_support/add_action', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/pay_support/add_action/store', 'admin', '{\"pay_code\":\"wechat\",\"status\":\"1\",\"resource\":\"pay_support\",\"action\":\"add_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"pay_support\",\"action\":\"add_action\",\"func\":\"store\"}]', '2023-03-31 17:09:36');
INSERT INTO `qk_system_oplog` VALUES (29, 0, 'admin', 'admin/resource/pay_support/index', '127.0.0.1', 'admin/resource/pay_support/index', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/pay_support/index', 'admin', '{\"spm\":\"m-75-84-172\",\"per_page\":0,\"page\":1,\"resource\":\"pay_support\",\"action\":\"index\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":{\"per_page\":20,\"total\":1,\"current_page\":1,\"data\":[{\"id\":{\"title\":\"ID\",\"name\":\"id\",\"uriKey\":\"column_id\",\"value\":1,\"originalValue\":1,\"display\":false},\"pay_code\":{\"title\":\"\\u652f\\u4ed8\\u7f16\\u53f7\",\"name\":\"pay_code\",\"uriKey\":\"column_pay_code\",\"value\":\"wechat\",\"originalValue\":\"wechat\",\"display\":false},\"pay_type_title\":{\"title\":\"\\u540d\\u79f0\",\"name\":\"pay_type_title\",\"uriKey\":\"column_pay_type_title\",\"value\":\"\\u5fae\\u4fe1\\u652f\\u4ed8\",\"originalValue\":\"\\u5fae\\u4fe1\\u652f\\u4ed8\",\"display\":false},\"pay_type_desc\":{\"title\":\"\\u8bf4\\u660e\",\"name\":\"pay_type_desc\",\"uriKey\":\"column_pay_type_desc\",\"value\":null,\"originalValue\":null,\"display\":false},\"status\":{\"title\":\"\\u542f\\u7528\\u72b6\\u6001\",\"name\":\"status\",\"uriKey\":\"column_status\",\"value\":1,\"originalValue\":1,\"display\":{\"component\":\"index-switch-field\",\"children\":[],\"props\":{\"switch\":{\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"keyValue\":1,\"keyName\":\"_keyValue_\",\"colName\":\"_colValue_\",\"submitUrl\":\"admin\\/resource\\/pay_support\\/switch_column_status\\/store\",\"fieldsUrl\":\"admin\\/resource\\/pay_support\\/switch_column_status\\/load\",\"value\":1,\"active-value\":1,\"inactive-value\":0,\"inactive-text\":\"\\u7981\\u7528\",\"active-text\":\"\\u542f\\u7528\",\"width\":55},\"originalValue\":1,\"authorizedToEdit\":true,\"uriKey\":\"switch_column_status\"}},\"_actions\":{\"title\":\"\\u64cd\\u4f5c\",\"name\":\"_actions\",\"uriKey\":\"action_column\",\"value\":null,\"originalValue\":null,\"display\":{\"component\":\"index-action-field\",\"children\":[],\"props\":{\"actionList\":[{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"dialog\",\"delay\":0,\"params\":{\"content\":\"\\/admin\\/resource\\/pay_support\\/setting_pay_action\\/load?_keyValues_=1\",\"config\":{\"component\":\"quick-dialog\",\"children\":[],\"props\":{\"title\":\"\\u8bbe\\u7f6e\",\"type\":\"dialog\",\"max-height\":\"65vh\",\"width\":\"900px\",\"lock-scroll\":false,\"top\":\"10vh\"},\"title\":\"title\"}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u8bbe\\u7f6e\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\"}}}},\"uriKey\":\"setting_pay_action\"},{\"component\":\"quick-action\",\"children\":[],\"props\":{\"action\":{\"action\":\"confirm\",\"delay\":0,\"params\":{\"confirm\":{\"action\":\"request\",\"delay\":0,\"params\":{\"url\":\"\\/admin\\/resource\\/pay_support\\/delete_action\\/store?_keyValues_=1\",\"method\":\"POST\"}},\"msg\":\"\\u4f60\\u786e\\u5b9a\\u8981\\u5220\\u9664\\u5417\\uff1f\",\"title\":\"\\u63d0\\u793a\",\"attributes\":{\"lockScroll\":false}}},\"display\":{\"component\":\"el-button\",\"children\":\"\\u5220\\u9664\",\"props\":{\"text\":true,\"type\":\"primary\",\"size\":\"small\",\"style\":{\"padding\":\"2px\",\"color\":\"red\"}}}},\"uriKey\":\"delete_action\"}],\"value\":null},\"originalValue\":null,\"value\":{\"id\":1,\"code\":8,\"plugin\":\"admin\",\"pay_code\":\"wechat\",\"status\":1,\"config\":\"[]\",\"created_at\":\"2023-03-31 17:09:36\",\"updated_at\":\"2023-03-31 17:09:36\",\"payType\":{\"id\":61,\"pay_code\":\"wechat\",\"title\":\"\\u5fae\\u4fe1\\u652f\\u4ed8\",\"desc\":null,\"class\":\"app\\\\common\\\\service\\\\payment\\\\payType\\\\WechatType\",\"status\":1,\"config\":null,\"created_at\":\"2023-03-14 09:17:10\",\"updated_at\":\"2023-03-14 10:10:22\"}},\"authorizedToEdit\":true,\"uriKey\":\"tools_column__actions\"}},\"_key\":{\"title\":\"_Key\",\"name\":\"_key\",\"uriKey\":\"column__key\",\"value\":1,\"originalValue\":1,\"display\":false}}]}}', '[{\"resource\":\"pay_support\",\"action\":\"index\"}]', '2023-03-31 17:09:36');
INSERT INTO `qk_system_oplog` VALUES (30, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=4', 'admin', '{\"_keyValues_\":\"4\",\"isTrusted\":true,\"_vts\":1680253834451,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 17:10:37');
INSERT INTO `qk_system_oplog` VALUES (31, 0, 'admin', 'admin/resource/config/delete_action', '127.0.0.1', '系统配置-删除', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/config/delete_action/store?_keyValues_=12', 'admin', '{\"_keyValues_\":\"12\",\"isTrusted\":true,\"_vts\":1680253838516,\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}', 'POST', '{\"msg\":\"success\",\"code\":0,\"data\":[]}', '[{\"resource\":\"config\",\"action\":\"delete_action\",\"func\":\"store\"}]', '2023-03-31 17:10:40');
INSERT INTO `qk_system_oplog` VALUES (32, 0, 'admin', 'admin/resource/system_config/list', '127.0.0.1', '系统参数-系统设置', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/system_config/list?_group=base', 'admin', '{\"_group\":\"base\",\"app_logo\":\"\",\"loginBanner\":\"\",\"loginBgImage\":\"\",\"app_name\":\"QuickAdmin\",\"app_version\":\"\",\"copyrightDates\":\"\",\"copyright\":\"\",\"website\":\"\",\"resource\":\"system_config\",\"action\":\"list\"}', 'POST', '{\"msg\":\"\\u8bbe\\u7f6e\\u6210\\u529f\",\"code\":0,\"data\":[]}', '[{\"resource\":\"system_config\",\"action\":\"list\"}]', '2023-03-31 17:11:58');
INSERT INTO `qk_system_oplog` VALUES (33, 0, 'admin', 'admin/resource/system_config/list', '127.0.0.1', '系统参数-系统设置', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/resource/system_config/list?_group=storage', 'admin', '{\"_group\":\"storage\",\"storage_dir_prefix\":\"admin\",\"allow_exts\":\"text,zip,png,jpg\",\"resource\":\"system_config\",\"action\":\"list\"}', 'POST', '{\"msg\":\"\\u8bbe\\u7f6e\\u6210\\u529f\",\"code\":0,\"data\":[]}', '[{\"resource\":\"system_config\",\"action\":\"list\"}]', '2023-03-31 17:12:34');
INSERT INTO `qk_system_oplog` VALUES (34, 0, '', 'admin/passport/login', '127.0.0.1', '登录系统', 47, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36 Edg/111.0.1661.54', '/index.php/admin/passport/login', 'admin', '[]', '', '[]', '', '2023-03-31 17:13:25');

-- ----------------------------
-- Table structure for qk_system_plugin
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_plugin`;
CREATE TABLE `qk_system_plugin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '插件key',
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '显示名称',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '描述',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '版本号',
  `sql_version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据版本',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态 1:启用, 0:禁用',
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除: 1已删除 0未删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `create_by` int(11) NOT NULL DEFAULT 0 COMMENT '创建人admin_id',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  `update_by` int(11) NOT NULL DEFAULT 0 COMMENT '修改人admin_id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统插件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_plugin
-- ----------------------------
INSERT INTO `qk_system_plugin` VALUES (3, 'crud', 'crud', '快捷生成代码', '', '', '', 1, NULL, 0, 2147483647, 0, '2022-02-25 11:30:06', '2023-03-07 11:28:03', 0);

-- ----------------------------
-- Table structure for qk_system_queue
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_queue`;
CREATE TABLE `qk_system_queue`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务编号',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `queue` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '队列名称',
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '有效负载',
  `command` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '执行指令',
  `run_pid` bigint(20) NULL DEFAULT 0 COMMENT '执行进程',
  `available_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '有效时间',
  `reserve_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '执行时间',
  `enter_time` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '开始时间',
  `outer_time` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '结束时间',
  `desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '执行描述',
  `attempts_max` int(10) NOT NULL DEFAULT 3 COMMENT '重发最大次数',
  `loops_time` bigint(20) NULL DEFAULT 0 COMMENT '循环时间',
  `attempts` bigint(20) NULL DEFAULT 0 COMMENT '执行次数',
  `rscript` tinyint(1) NULL DEFAULT 1 COMMENT '任务类型(0单例,1多例)',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '任务状态(1新任务,2处理中,3成功,4失败)',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_system_queue_code`(`code`) USING BTREE,
  INDEX `idx_system_queue_title`(`title`) USING BTREE,
  INDEX `idx_system_queue_create_at`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统-任务' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_queue
-- ----------------------------
INSERT INTO `qk_system_queue` VALUES (1, 'Q202303315957042', '系统更新', 'quick', '{\"job\":\"app\\\\admin\\\\jobs\\\\UpgradeJob\",\"maxTries\":null,\"timeout\":null,\"data\":{\"do\":\"handle\",\"attempts_max\":0,\"title\":\"\\u7cfb\\u7edf\\u66f4\\u65b0\",\"release\":0,\"rscript\":0,\"code\":\"Q202303315957042\",\"data\":[]}}', 'app\\admin\\jobs\\UpgradeJob', 0, 1680252237, 1680252238, 1680252238.3305, 1680252272.4170, NULL, 0, 0, 1, 0, 3, 1680252237);

-- ----------------------------
-- Table structure for qk_system_user
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user`;
CREATE TABLE `qk_system_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '账号id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账户名称',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 1:启用, 0:禁用',
  `last_login_ip_at` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后一次登录ip',
  `create_ip_at` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建ip',
  `login_num` int(11) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `login_fail_num` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '失败次数',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除: 1已删除 0未删除',
  `login_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_email`(`email`) USING BTREE,
  INDEX `idx_phone`(`phone`) USING BTREE,
  INDEX `idx_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统统一账户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qk_system_user
-- ----------------------------
INSERT INTO `qk_system_user` VALUES (47, 'admin', 'admin', '', '', '', '4be6ee75d7a32f2a68423a5fab2ad8a2', 'dzsms', 1, '', '', 0, 0, 0, '2023-03-31 17:13:25', 0, '2022-12-13 10:20:52', '2023-03-31 17:13:25');

-- ----------------------------
-- Table structure for qk_system_user_balance_log
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_balance_log`;
CREATE TABLE `qk_system_user_balance_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `num` decimal(10, 2) NOT NULL COMMENT '变动数量',
  `current_num` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '当前余额-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '余额记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_info
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_info`;
CREATE TABLE `qk_system_user_info`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号id',
  `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '性别:male=男,female=女,unknow=未知',
  `integral` int(11) NOT NULL DEFAULT 0 COMMENT '积分',
  `total_integral` int(11) NOT NULL DEFAULT 0 COMMENT '最高积分',
  `balance` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '余额',
  `total_balance` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '总余额',
  `contact_way` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '联系方式',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `motto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '个性签名',
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '上级id',
  `temp_parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '临时上级',
  `platform_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '平台id 如微信 openid',
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE,
  INDEX `idx_temp_parent_id`(`temp_parent_id`) USING BTREE,
  INDEX `idx_platform_id`(`platform_openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_integral_log
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_integral_log`;
CREATE TABLE `qk_system_user_integral_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `integral` int(11) NOT NULL COMMENT '变动积分',
  `current_integral` int(11) NOT NULL DEFAULT 0 COMMENT '当前积分-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '积分记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_platform
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_platform`;
CREATE TABLE `qk_system_user_platform`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '账号id',
  `nickname` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '性别',
  `platform_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '平台id 如微信 openid',
  `platform` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'h5密码',
  `unionid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信unionid',
  `subscribe` tinyint(1) NOT NULL DEFAULT 0 COMMENT '微信是否关注',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_platform`(`platform`) USING BTREE,
  INDEX `idx_platform_id`(`platform_openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '第三方用户信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qk_system_user_token
-- ----------------------------
DROP TABLE IF EXISTS `qk_system_user_token`;
CREATE TABLE `qk_system_user_token`  (
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Token',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '会员ID',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '创建时间',
  `expire_time` int(11) NULL DEFAULT NULL COMMENT '过期时间',
  INDEX `idx_token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '会员Token表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
