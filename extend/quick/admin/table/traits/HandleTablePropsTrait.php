<?php
declare (strict_types=1);

namespace quick\admin\table\traits;


use quick\admin\components\Component;

trait HandleTablePropsTrait
{

    /**
     * 禁用table右边工具条
     *
     * @return $this
     */
    public function disableRightTools()
    {
        $this->withMeta(['disableRightTools' => true]);
        return $this;
    }


    /**
     * 显示table刷新按钮
     *
     * @return $this
     */
    public function showRefresh()
    {
        $this->props(['showRefresh' => true]);
        return $this;
    }


    /**
     * show 配置表格尺寸按钮
     *
     * @return $this
     */
    public function showSize()
    {
        $this->props(['showSize' => true]);
        return $this;
    }


    /**
     * 禁用table栏目显示配置工具
     *
     * @return $this
     */
    public function showColumnSelector()
    {
        $this->props(['showColumnSelector' => true]);
        return $this;
    }


    /**
     * 创建带斑马纹的表格
     *
     * @return $this
     */
    public function stripe()
    {
        $this->withAttributes([__FUNCTION__ => true]);
        return $this;
    }

    /**
     * height属性为 Table 指定高度
     *
     * @param int|string $height
     * @return $this
     */
    public function height($height)
    {
        $this->withAttributes([__FUNCTION__ => $height]);
        return $this;
    }

    /**
     * max-height属性为 Table 指定最大高度
     *
     * @param $height
     * @return $this
     */
    public function maxHeight($height)
    {
        $this->withAttributes(["max-height" => $height]);
        return $this;
    }


    /**
     * 设置默认尺寸
     *
     * @param string $size medium|small|mini
     * @return mixed
     */
    public function defaultSize(string $size)
    {
        return $this->props(['tableSize' => $size]);
    }

    /**
     * @param string $type checkbox|radio
     * @return \quick\admin\table\Table
     */
    public function showSelection(string $type = 'checkbox')
    {
        return $this->props([
            'showSelection' => true,
            'selectionType' => $type,
        ]);
    }

    /**
     * 显示多选
     *
     * @return mixed
     */
    public function checkbox(\Closure  $func = null)
    {
        if(!$func){
            $func = function($value, $row, $originalValue){
                return $row;
            };
        }
        $this->column('_select_data')->display($func)->onlyDataColumn();
        $this->showConfirmBtn();
        return $this;
    }

    /**
     * 显示多选
     *
     * @return mixed
     */
    public function radio(\Closure  $func = null)
    {
        if(!$func){
            $func = function($value, $row, $originalValue){
                return $row;
            };
        }
        $this->column('_select_data')->display($func)->onlyDataColumn();
        $this->showConfirmBtn();
        return $this;
    }

    public function showConfirmBtn(string $type = 'checkbox')
    {
        $this->showSelection($type);
        return $this->props([
            'showConfirmBtn' => true,
        ]);
    }

    /**
     * 隐藏过滤器
     *
     * @return mixed
     */
    public function hideFilter()
    {
        return $this->withMeta(['hideFilter' => true]);
    }


}
