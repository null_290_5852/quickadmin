<?php
declare (strict_types=1);

namespace quick\admin\form\fields;


use app\common\model\SystemArea;
use Closure;
use quick\admin\Element;
use quick\admin\metable\Metable;
use think\Exception;

class SelectArea extends Field
{


    public $component = 'form-select-area-field';

    /**
     * @var string
     */
    protected $valueType = 'array';

    /**
     * @var
     */
    protected $max;

    /**
     * @var
     */
    protected $min;

    /**
     * @var
     */
    public $default;


    /**
     * @var
     */
    protected $options;

    /**
     * @var string
     */
    protected $keyName;



    /**
     * @var array
     */
    protected $props = [];



//    public function transform($value)
//    {
//        if($this->cascaderType = 'region'){
//            $value = SystemArea::getValuesByIds($value);
//            return $value;
//        }
//    }





    /**
     * 最小个数
     *
     * @param int $num
     * @return $this
     */
    public function min(int $num): self
    {
        $this->min = $num;
        $this->rules('min:' . $num);
        return $this;
    }


    /**
     * 最大个数
     *
     * @param int $num
     * @return $this
     */
    public function max(int $num): self
    {
        $this->max = $num;
        $this->rules('max:' . $num);
        return $this;
    }


    /**
     * 固定值配置
     * @param array $value 选中固定值
     * @param array $default 取消固定值时赋值默认值
     * @param string $text
     * @return $this
     */
    public function fixedValue(array $value = [],array $default = [],string $text)
    {
        $this->props([
            'width' => 'auto',
            'fixedValue' => $value,
            'fixedText' => $text,
            'fixedDefault' => $default,
        ]);
        return $this;
    }


    /**
     * 禁止选择
     * @param array $disableIds
     * @return SelectArea
     */
    public function disable(array $disableIds)
    {
        return $this->props('disable',$disableIds);
    }


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function jsonSerialize(): array
    {
        $areaOptions = SystemArea::cascaderOptions();
        $this->attribute('props', $this->props);
        $this->props([
            'min' => $this->min ?: 0,
            'max' => $this->max ?: 0,
            'options' => $areaOptions,
        ]);
        return array_merge(parent::jsonSerialize(), []);
    }
}
