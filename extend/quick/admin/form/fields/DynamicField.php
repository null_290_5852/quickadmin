<?php
declare (strict_types=1);

namespace quick\admin\form\fields;


use quick\admin\components\Component;
use quick\admin\Element;
use quick\admin\form\Form;
use quick\admin\form\layout\Row;

class DynamicField extends Field
{


    public $component = 'form-dynamic-field';

    public $field;

    public $valueType = 'array';


    /**
     * @var
     */
    protected $max;

    /**
     * @var
     */
    protected $min;


    /**
     * @var Element
     */
    public $displayComponent;


    public function transform($value)
    {
        $value = json_decode($value, true);
        return $value;
    }

    public function displayComponent()
    {

        $this->displayComponent = Component::row()->style([
            "margin-bottom" => '20px',
            "display" => 'flex',
        ])->children('');
        return $this;
    }

    public function form(\Closure $closure)
    {
        $form = Form::make();
        $form = call_user_func($closure, $form);
        if ($form instanceof Form) {
            $this->displayComponent = $form;
        }

        return $this;
    }

    public function getDisplayComponent()
    {
        if (!$this->displayComponent) {
            $this->displayComponent();
        }
        return $this->displayComponent;
    }




    protected function jsonOut($data)
    {
        return json_decode(json_encode($data));
    }


    /**
     * 模板弹出选择
     * @param string $load 加载地址
     * @param string $title 按钮名称
     * @param bool $isPush 追加模式
     * @return $this
     */
    public function load(string $load, string $title = 'select', bool $isPush = false): self
    {
        $this->props([
            'load' => $load,
            'loadTitle' => $title,
            'loadMode' => $isPush ? 'push' : 'submit',
        ]);
        return $this;
    }

    /**
     * 最小个数
     *
     * @param int $num
     * @return $this
     */
    public function min(int $num): self
    {
        $this->min = $num;
        $this->rules('min:' . $num);
        return $this;
    }


    /**
     * 按钮顶部显示
     * @return DynamicField
     */
    public function btnPositionTop()
    {
        return $this->props([
            'position' => 'top',
        ]);
    }

    /**
     * 按钮底部显示
     * @return DynamicField
     */
    public function btnPositionBottom()
    {
        return $this->props([
            'position' => 'bottom',
        ]);
    }

    /**
     * 按钮底部显示
     * @return DynamicField
     */
    public function btnBottomCenter()
    {
        return $this->props([
            'position' => 'bottom',
            'btnFlexClass' => 'main-center',
        ]);
    }

    /**
     * 按钮底部显示
     * @return DynamicField
     */
    public function btnBottomRight()
    {
        return $this->props([
            'position' => 'bottom',
            'btnFlexClass' => 'main-right',
        ]);
    }

    /**
     * 最大个数
     *
     * @param int $num
     * @return $this
     */
    public function max(int $num): self
    {
        $this->max = $num;
        $this->rules('max:' . $num);
        return $this;
    }


    /**
     * @return array
     */
    public function jsonSerialize(): array
    {


        $form = $this->getDisplayComponent();
        $buildFormFunc = function () use ($form){
            return Component::row()
                ->style([
                    "margin-top" => '10px',
                    "flex-wrap" => 'wrap',
                ])
                ->attribute([
                    'type' => 'flex',
                    'gutter' => 30,
                ])
                ->children($form->getFields());
        };
        $fieldJson = $this->jsonOut($buildFormFunc()->jsonSerialize());
        $fieldData = [];
        if ($this->value) {
            $value = is_string($this->value) ? json_decode($this->value, true):$this->value;
            foreach ($value as $item) {
                $form_item = clone $form;
                $form_item->resolve($item);
                $fieldData[] = $this->jsonOut($buildFormFunc());
            }
        }

        $this->props([
            'fieldJson' => $fieldJson,
            'fieldData' => $fieldData,
            'min' => $this->min ?: 0,
            'max' => $this->max ?: 0,
        ]);
        return array_merge(parent::jsonSerialize(), []);
    }

}
