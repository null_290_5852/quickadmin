<?php

namespace quick\admin;

use quick\admin\form\Form;

interface  PluginConfigInterface
{

    /**
     * 配置表单
     * @return Form
     */
    public function form():Form;

    /**
     * 配置修改前执行
     * @return mixed
     */
    public function beforeSave($data, $request);

    /**
     *
     * 配置修改后执行
     */
    public function afterSave($data, $request);
}