<?php


// +----------------------------------------------------------------------
// | 当前模块使用的是 Library for ThinkAdmin 版权归其所有
// +----------------------------------------------------------------------
// | 版权所有 2014~2022 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace quick\admin\library\storage;


use quick\admin\form\Form;
use quick\admin\library\tools\HttpTools;

/**
 * 腾讯云COS存储支持
 * Class TxcosStorage
 * @package think\admin\storage
 */
class TxcosStorage extends Storage
{
    /**
     * 数据中心
     * @var string
     */
    private $point;

    /**
     * 存储空间名称
     * @var string
     */
    private $bucket;

    /**
     * $secretId
     * @var string
     */
    private $secretId;

    /**
     * secretKey
     * @var string
     */
    private $secretKey;

    /**
     *
     */
    protected function initialize()
    {
        if(empty($this->config)) return true;
        // 读取配置文件
        $this->point = $this->config['point'] ?? '';
        $this->bucket = $this->config['bucket'] ?? '';
        $this->secretId =  $this->config['access_key'] ?? '';
        $this->secretKey = $this->config['secret_key'] ?? '';
        // 计算链接前缀
         $type = strtolower($this->config['protocol'] ?? '') ;
        $domain = strtolower($this->config['domain'] ?? '') ;
        if ($type === 'auto') {
            $this->domian = "//{$domain}";
        } elseif (in_array($type, ['http', 'https'])) {
            $this->domian = "{$type}://{$domain}";
        } else throw new \Exception('未配置腾讯云COS访问域名哦');

    }

    public function configForm():Form
    {
        $form = Form::make();
        $form->radio('name_type','命名方式')->options([
            'hash' => '文件哈希值',
            'date' => '日期加随机',
        ])->default('hash')->required();

        $form->radio('link_type','链接类型')
            ->options([
                'none'=>'简洁链接',
                'full'=>'完整链接',
                'none+compress'=>'简洁并压缩图片',
                'full+compress'=>'完整并压缩图片'
            ])->help('类型为“简洁链接”时链接将只返回 hash 地址，而“完整链接”将携带参数保留文件名，图片压缩功能云平台会单独收费。');

        $form->radio('protocol','访问协议')->options([
            'http' => 'HTTP',
            'https' => 'HTTPS',
            'auto' => 'AUTO',
        ])->required()->help('腾讯云COS存储访问协议，其中 HTTPS 需要配置证书才能使用（ AUTO 为相对协议 ）');

        $form->text('domain','访问域名')->required()
            ->help('填写腾讯云COS存储外部访问域名，如：static.quickadmin.top');

        $form->select('point','存储区域')->options($this->region())->required()
            ->help('腾讯云COS存储访问协议，其中 HTTPS 需要配置证书才能使用（ AUTO 为相对协议 ）');
        $form->text('bucket','空间名称')->required()
            ->help('填写腾讯云COS存储空间名称，如：quickadmin-1251143395');
        $form->text('access_key','access_key')
            ->help('可以在 [ 腾讯云 > 个人中心 ] 设置并获取到访问密钥')->required();
        $form->text('secret_key','secret_key')
            ->help('可以在 [ 腾讯云 > 个人中心 ] 设置并获取到访问密钥')->required();
        return $form;
    }


    /**
     * 上传文件内容
     * @param string $name 文件名称
     * @param string $file 文件内容
     * @param boolean $safe 安全模式
     * @param null|string $attname 下载名称
     * @return array
     */
    public function save(string $name, string $file, bool $safe = false, ?string $attname = null): array
    {
        $data = $this->buildUploadToken($name) + ['key' => $name];
        if (is_string($attname) && strlen($attname) > 0) {
            $data['Content-Disposition'] = urlencode($attname);
        }
        $data['success_action_status'] = '200';
        $file = ['field' => 'file', 'name' => $name, 'content' => $file];
        if (is_numeric(stripos(HttpTools::submit($this->upload(), $data, $file), '200 OK'))) {
            return ['file' => $this->savePath($name, $safe), 'url' => $this->url($name, $safe, $attname), 'key' => $name];
        } else {
            return [];
        }

    }

    /**
     * 根据文件名读取文件内容
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return string
     */
    public function get(string $name, bool $safe = false): string
    {
        return static::curlGet($this->url($name, $safe));
    }

    /**
     * 删除存储的文件
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return boolean
     */
    public function del(string $name, bool $safe = false): bool
    {
        [$file] = explode('?', $name);
        $result = HttpTools::request('DELETE', "http://{$this->bucket}.{$this->point}/{$file}", [
            'returnHeader' => true, 'headers' => $this->headerSign('DELETE', $file),
        ]);
        return is_numeric(stripos($result, '204 No Content'));
    }

    /**
     * 判断文件是否存在
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return boolean
     */
    public function has(string $name, bool $safe = false): bool
    {
        $file = $this->delSuffix($name);
        $result = HttpTools::request('HEAD', "http://{$this->bucket}.{$this->point}/{$file}", [
            'returnHeader' => true, 'headers' => $this->headerSign('HEAD', $name),
        ]);
        return is_numeric(stripos($result, 'HTTP/1.1 200 OK'));
    }

    /**
     * 获取文件当前URL地址
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @param null|string $attname 下载名称
     * @return string
     */
    public function url(string $name, bool $safe = false, ?string $attname = null): string
    {
        return "{$this->domian}/{$this->delSuffix($name)}{$this->getSuffix($attname,$name)}";
    }

    /**
     * 获取文件存储路径
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return string
     */
    public function savePath(string $name, bool $safe = false): string
    {
        return $this->url($name, $safe);
    }

    /**
     * 获取文件存储信息
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @param null|string $attname 下载名称
     * @return array
     */
    public function info(string $name, bool $safe = false, ?string $attname = null): array
    {
        return $this->has($name, $safe) ? [
            'url' => $this->url($name, $safe, $attname),
            'key' => $name, 'file' => $this->savePath($name, $safe),
        ] : [];
    }

    /**
     * 获取文件上传地址
     * @return string
     */
    public function upload(): string
    {
        $protocol = $this->app->request->isSsl() ? 'https' : 'http';
        return "{$protocol}://{$this->bucket}.{$this->point}";
    }

    /**
     * 获取文件上传令牌
     * @param string $name 文件名称
     * @param integer $expires 有效时间
     * @param null|string $attname 下载名称
     * @return array
     */
    public function buildUploadToken(string $name, int $expires = 3600, ?string $attname = null): array
    {
        $startTimestamp = time();
        $endTimestamp = $startTimestamp + $expires;
        $keyTime = "{$startTimestamp};{$endTimestamp}";
        $siteurl = $this->url($name, false, $attname);
        $policy = json_encode([
            'expiration' => date('Y-m-d\TH:i:s.000\Z', $endTimestamp),
            'conditions' => [['q-ak' => $this->secretId], ['q-sign-time' => $keyTime], ['q-sign-algorithm' => 'sha1']],
        ]);
        return [
            'policy'      => base64_encode($policy), 'q-ak' => $this->secretId,
            'siteurl'     => $siteurl, 'q-key-time' => $keyTime, 'q-sign-algorithm' => 'sha1',
            'q-signature' => hash_hmac('sha1', sha1($policy), hash_hmac('sha1', $keyTime, $this->secretKey)),
        ];
    }

    /**
     * 操作请求头信息签名
     * @param string $method 请求方式
     * @param string $soruce 资源名称
     * @return array
     */
    private function headerSign(string $method, string $soruce): array
    {
        $header = [];
        // 1.生成 KeyTime
        $startTimestamp = time();
        $endTimestamp = $startTimestamp + 3600;
        $keyTime = "{$startTimestamp};{$endTimestamp}";
        // 2.生成 SignKey
        $signKey = hash_hmac('sha1', $keyTime, $this->secretKey);
        // 3.生成 UrlParamList, HttpParameters
        [$parse_url, $urlParamList, $httpParameters] = [parse_url($soruce), '', ''];
        if (!empty($parse_url['query'])) {
            parse_str($parse_url['query'], $params);
            uksort($params, 'strnatcasecmp');
            $urlParamList = join(';', array_keys($params));
            $httpParameters = http_build_query($params);
        }
        // 4.生成 HeaderList, HttpHeaders
        [$headerList, $httpHeaders] = ['', ''];
        if (!empty($header)) {
            uksort($header, 'strnatcasecmp');
            $headerList = join(';', array_keys($header));
            $httpHeaders = http_build_query($header);
        }
        // 5.生成 HttpString
        $httpString = strtolower($method) . "\n/{$parse_url['path']}\n{$httpParameters}\n{$httpHeaders}\n";
        // 6.生成 StringToSign
        $httpStringSha1 = sha1($httpString);
        $stringToSign = "sha1\n{$keyTime}\n{$httpStringSha1}\n";
        // 7.生成 Signature
        $signature = hash_hmac('sha1', $stringToSign, $signKey);
        // 8.生成签名
        $signArray = [
            'q-sign-algorithm' => 'sha1',
            'q-ak'             => $this->secretId,
            'q-sign-time'      => $keyTime,
            'q-key-time'       => $keyTime,
            'q-header-list'    => $headerList,
            'q-url-param-list' => $urlParamList,
            'q-signature'      => $signature,
        ];
        $header['Authorization'] = urldecode(http_build_query($signArray));
        foreach ($header as $key => $value) $header[$key] = ucfirst($key) . ": {$value}";
        return array_values($header);
    }

    /**
     * 腾讯云COS存储区域
     * @return array
     */
    public static function region(): array
    {
        return [
            'cos.ap-beijing-1.myqcloud.com'     => '中国大陆 公有云地域 北京一区',
            'cos.ap-beijing.myqcloud.com'       => '中国大陆 公有云地域 北京',
            'cos.ap-nanjing.myqcloud.com'       => '中国大陆 公有云地域 南京',
            'cos.ap-shanghai.myqcloud.com'      => '中国大陆 公有云地域 上海',
            'cos.ap-guangzhou.myqcloud.com'     => '中国大陆 公有云地域 广州',
            'cos.ap-chengdu.myqcloud.com'       => '中国大陆 公有云地域 成都',
            'cos.ap-chongqing.myqcloud.com'     => '中国大陆 公有云地域 重庆',
            'cos.ap-shenzhen-fsi.myqcloud.com'  => '中国大陆 金融云地域 深圳金融',
            'cos.ap-shanghai-fsi.myqcloud.com'  => '中国大陆 金融云地域 上海金融',
            'cos.ap-beijing-fsi.myqcloud.com'   => '中国大陆 金融云地域 北京金融',
            'cos.ap-hongkong.myqcloud.com'      => '亚太地区 公有云地域 中国香港',
            'cos.ap-singapore.myqcloud.com'     => '亚太地区 公有云地域 新加坡',
            'cos.ap-mumbai.myqcloud.com'        => '亚太地区 公有云地域 孟买',
            'cos.ap-seoul.myqcloud.com'         => '亚太地区 公有云地域 首尔',
            'cos.ap-bangkok.myqcloud.com'       => '亚太地区 公有云地域 曼谷',
            'cos.ap-tokyo.myqcloud.com'         => '亚太地区 公有云地域 东京',
            'cos.na-siliconvalley.myqcloud.com' => '北美地区 公有云地域 硅谷',
            'cos.na-ashburn.myqcloud.com'       => '北美地区 公有云地域 弗吉尼亚',
            'cos.na-toronto.myqcloud.com'       => '北美地区 公有云地域 多伦多',
            'cos.eu-frankfurt.myqcloud.com'     => '欧洲地区 公有云地域 法兰克福',
            'cos.eu-moscow.myqcloud.com'        => '欧洲地区 公有云地域 莫斯科	',
        ];
    }



}