<?php

// +----------------------------------------------------------------------
// | Library for QuickAdmin
// +----------------------------------------------------------------------
// | 此文件改造于https://gitee.com/zoujingli/ThinkLibrary开源项目，在此特别提出感谢 ！！！！！！！！！！！！！！！
// +----------------------------------------------------------------------
// | 原始源代码来自 https://gitee.com/zoujingli/ThinkLibrary ，基于 MIT 协议开源
// +----------------------------------------------------------------------


declare (strict_types=1);

namespace quick\admin\library\storage;


use quick\admin\form\Form;

/**
 * 本地存储支持
 * Class LocalStorage
 * @package think\admin\storage
 */
class LocalStorage extends Storage
{

    /**
     * 初始化入口
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function initialize()
    {

        $type = $this->config['protocol'] ?? 'follow';
        if ($type === 'follow') $type = $this->app->request->scheme();

        $this->domian = trim(dirname($this->app->request->baseFile(false)), '\\/');

        if ($type !== 'path') {
            $domain = !empty($this->config['domain']) ?  $this->config['domain']:$this->app->request->host();
            if ($type === 'auto') {
                $this->domian = "//{$domain}";
            } elseif (in_array($type, ['http', 'https'])) {
                $this->domian = "{$type}://{$domain}";
            }
        }
    }

    public function configForm():Form
    {
        $form = Form::make();
        $form->radio('name_type','命名方式')->options([
            'hash' => '文件哈希值',
            'date' => '日期加随机',
        ])->default('hash')->required();


        $form->radio('protocol','访问协议')->options([
            'follow' => 'FOLLOW',
            'http' => 'HTTP',
            'https' => 'HTTPS',
            'path' => 'PATH',
            'auto' => 'AUTO',
        ])->required()->help('本地存储访问协议，其中 HTTPS 需要配置证书才能使用（ FOLLOW 跟随系统，PATH 文件路径，AUTO 相对协议 ）');

        $form->text('domain','访问域名')
            ->help('填写上传后的访问域名（不指定时根据当前访问地址自动计算），如：static.quickadmin.cn');
        return $form;
    }

    /**
     * 文件储存在本地
     * @param string $name 文件名称
     * @param string $file 文件内容
     * @param boolean $safe 安全模式
     * @param null|string $attname 下载名称
     * @return array
     */
    public function save(string $name, string $file, bool $safe = false, ?string $attname = null): array
    {
        try {
            $path = $this->savePath($name, $safe);
            file_exists(dirname($path)) || mkdir(dirname($path), 0755, true);
            if (file_put_contents($path, $file)) {
                return $this->info($name, $safe, $attname);
            }
        } catch (\Exception $exception) {

        }
        return [];
    }

    /**
     * 根据文件名读取文件内容
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return string
     */
    public function get(string $name, bool $safe = false): string
    {
        if (!$this->has($name, $safe)) return '';
        return file_get_contents($this->savePath($name, $safe));
    }

    /**
     * 删除存储的文件
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return boolean
     */
    public function del(string $name, bool $safe = false): bool
    {
        if ($this->has($name, $safe)) {
            return @unlink($this->savePath($name, $safe));
        } else {
            return false;
        }
    }

    /**
     * 检查文件是否已经存在
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return boolean
     */
    public function has(string $name, bool $safe = false): bool
    {
        return file_exists($this->savePath($name, $safe));
    }

    /**
     * 获取文件当前URL地址
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @param null|string $attname 下载名称
     * @return string
     */
    public function url(string $name, bool $safe = false, ?string $attname = null): string
    {
        return $safe ? $name : "{$this->domian}/upload/{$this->delSuffix($name)}{$this->getSuffix($attname,$name)}";
    }

    /**
     * 获取文件存储路径
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @return string
     */
    public function savePath(string $name, bool $safe = false): string
    {
        $root = $this->app->getRootPath();
        $path = $safe ? 'safefile' : 'public/upload';
        return strtr("{$root}{$path}/{$this->delSuffix($name)}", '\\', '/');
    }

    /**
     * 获取文件存储信息
     * @param string $name 文件名称
     * @param boolean $safe 安全模式
     * @param null|string $attname 下载名称
     * @return array
     */
    public function info(string $name, bool $safe = false, ?string $attname = null): array
    {
        return $this->has($name, $safe) ? [
            'url' => $this->url($name, $safe, $attname),
            'key' => "upload/{$name}",
            'file' => $this->savePath($name, $safe),
        ] : [];
    }

    /**
     * 获取文件上传地址
     * @return string
     */
    public function upload(): string
    {
        return url('admin/api.upload/file')->build();
    }



}
