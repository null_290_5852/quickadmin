<?php
declare (strict_types=1);

namespace quick\admin\components\element;


use quick\admin\Element;
use quick\admin\form\Form;

class QuickDialog extends Element
{


    public $component = "quick-dialog";

    /**
     * CustomField constructor.
     * @param $column
     * @param $title
     */
    public function __construct($title = '')
    {

        $title && $this->title($title);
        $this->props('type' ,'dialog');
        $this->attribute([
            "lock-scroll" => false,
            "top" => '10vh'
        ])->maxHeight("65vh")->width('750px');

    }


    /**
     * @param string $title
     * @return QuickDialog
     */
    public function title(string $title)
    {
        $this->withMeta(["title" => $title]);
        return $this->props(["title" => $title]);
    }


    /**
     * Prepare the field for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), []);
    }

    /**
     * @param string $width
     * @return $this
     */
    public function width(string $width)
    {
        is_numeric($width) && $width = $width . "px";
        $this->props("width", $width);
        return $this;
    }

    /**
     * @param $component
     * @param string $slot
     * @return Element
     */
    public function children($component, $slot = '')
    {
//        if ($component instanceof Form) {
//            $component->hideFooter();
//        }
        return parent::children($component, $slot);
    }

    /**
     * @param string $height
     * @return $this
     */
    public function height(string $height)
    {
        $this->props("height", $height);
        return $this;
    }

    /**
     * @param string $height
     * @return $this
     */
    public function maxHeight(string $height)
    {
        $this->props("max-height", $height);
        return $this;
    }

}
