<?php
declare (strict_types = 1);

namespace quick\admin\components;


use quick\admin\Element;
use quick\admin\metable\HasEmit;

/**
 * 内容切换组件
 *
 * Class SwitchContent
 * @package quick\components
 */
class SwitchContent extends Element
{


    use HasEmit;

    public $component = 'switch-content';

    protected $active;
    protected $inactive;

    public function __construct($active,$inactive,string $key,$condition,$value)
    {
//        $this->active = $active;
//        $this->inactive = $inactive;
        $this->active($active);
        $this->inactive($inactive);
        $this->listen($key,$condition,$value);

    }

    public function active($active)
    {
        $this->active = $active;
        return $this;
    }

    public function inactive($inactive)
    {
        $this->inactive = $inactive;
        return $this;
    }

    public function listen(string $key,$condition,$value)
    {

        $emits = [
            'name' => $key,
            'event' => 'change',
            'yes' => 'show',
            'no' => 'hidden',
            'condition' => $condition,
            'option' => $value
        ];
        $this->emit([$emits]);
        return $this;
    }

    /**
     * Prepare the field for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        $this->props([
            'emit' => $this->getEmit(),
            'active' => $this->active,
            'inactive' => $this->inactive,
        ]);
        return array_merge(parent::jsonSerialize(),[]);
    }
}
