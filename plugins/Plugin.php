<?php

namespace plugins;


use app\common\model\SystemPlugin;
use quick\admin\library\service\SystemService;
use quick\admin\QuickPluginService;

class Plugin extends QuickPluginService
{

    /**
     * init
     * @return $this
     */
    public function initPlugin()
    {
        $this->register();
        $this->boot();
        return $this;
    }


    /**
     * 注册资源事件。。。 已经安装的插件 系统每次request都会执行
     *
     * @return void
     */
    public function register()
    {


    }

    /**
     * 插件安装执行代码
     * @return mixed
     */
    public function install()
    {
        return true;
    }


    /**
     * 插件更新执行代码
     * @return bool
     */
    public function upgrade()
    {
        $this->upgradeVersion();
        return true;
    }


    /**
     * 执行版本升级
     * @return false|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function upgradeVersion()
    {

        $plugin = SystemPlugin::where(['name' => $this->getAppKey(),'is_deleted' => 0])->find();
        $currentVersion = empty($plugin->sql_version) ? '1.0.0':$plugin->sql_version ;
        $file =   app()->getRootPath() . "plugins" . DIRECTORY_SEPARATOR . $this->getAppKey() . DIRECTORY_SEPARATOR.'versions.php';
        if(!file_exists($file)){
            return false;
        }

        $lastVersion = $currentVersion;
        $versions = require_once $file;
        foreach ($versions as $ver => $func) {
            $lastVersion = $ver;
            if (version_compare($ver, $currentVersion) > 0) {
                if ($func instanceof \Closure) {
                    $func();
                }
            }
        }
        if(!$plugin){
            return false;
        }
        // 记录当前数据库插件版本号
        $plugin->sql_version = $lastVersion;
        $plugin->save();
    }

    /**
     * 插件卸载执行代码
     * @return mixed
     */
    public function uninstall()
    {
        return true;
    }


    /**
     * 禁用插件
     * @return bool
     */
    public function disable()
    {
        return true;
    }


    /**
     * 启用插件
     * @return bool
     */
    public function enable()
    {
        return true;
    }


    /**
     * 插件安装之前
     */
    public function beforeInstall()
    {

    }


    /**
     * 插件安装之后
     */
    public function afterInstall()
    {

    }


    /**
     * 插件更新之前
     */
    public function beforeUpdate()
    {

    }


    /**
     * 插件更新之后
     */
    public function afterUpdate()
    {

    }


    /**
     * 插件卸载之前
     */
    public function beforeUninstall()
    {

    }


    /**
     * 插件卸载之后
     */
    public function afterUninstall()
    {

    }




}
