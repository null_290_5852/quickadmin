<?php
declare (strict_types = 1);

namespace plugins\crud\command\service;


use quick\admin\form\Form;

class BuildForm
{



    /**
     * @param $name
     * @param $title
     * @param $fieldInfo
     * @param $modelName
     * @return array
     * @throws \Exception
     */
    public static function buildFieldItem($name,$title,$fieldInfo,$modelName,$tableName):array
    {

        $field = CrudConfig::buildField($name,$title,$fieldInfo,$modelName,$tableName);
        return $field;
    }


    /**
     * @param array $fields
     * @param $modelName
     * @return array
     * @throws \Exception
     */
    public static function buildFieldData(array $fields,$modelName,$tableName):array
    {
        $fieldList = [];
        foreach ($fields as $field){
            if(!empty($field['relation']) || !$field['is_form'] || $field['primary']){
               continue;
            }
            $fieldList[] = static::buildFieldItem($field['name'],$field['label'],$field,$modelName,$tableName);
        }

        return $fieldList;
    }

    /**
     * @param $fields
     * @param $modelName
     * @return Form|null
     * @throws \Exception
     */
    public static function buildField($fields,$modelName,$tableName)
    {
        $data = self::buildFieldData($fields,$modelName,$tableName);

        return Form::buildForm($data);
    }


}
