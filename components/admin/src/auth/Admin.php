<?php
declare(strict_types=1);

namespace components\admin\src\auth;

use app\common\model\SystemAdminInfo;
use app\common\model\SystemUser;
use components\admin\src\actions\auth\EditUserPasswordAction;
use quick\admin\library\tools\CodeTools;
use quick\admin\library\tools\TreeArray;
use quick\admin\annotation\AdminAuth;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\Resource;
use quick\admin\table\Table;
use think\Request;

/**
 * Class Admin
 * @AdminAuth(title="系统管理员",auth=true,login=true,menu=true)
 * @package components\admin\src\auth
 */
class Admin extends Resource
{
    /**
     * @var string
     */
    protected $title = '系统员工';

    /**
     * @var string
     */
    protected $description = "系统员工管理";

    /**
     * @var string
     */
    protected static $model = "app\\common\\model\\SystemAdminInfo";


    /**
     * 可搜索字段
     *
     * @var array
     */
    public static $search = ["nickname", 'name', 'phone'];


    /**
     * 过滤器
     *
     * @param Filter $filter
     * @return Filter
     */
    protected function filters(Filter $filter)
    {

        $filter->labelWidth(100);
        $filter->like("nickname", "用户昵称")->width(12);

        return false;
    }


    public function model($model)
    {
        $model->where([
            ["plugin_name", "=", app()->http->getName()],
            ["is_deleted", "=", 0],
        ]);
        return $model->order("id desc");
    }


    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {


        $table->attribute('border', false);
        $table->column("id", "ID")->width(80)->sortable();
        $table->column("user.username", "账户")->width(150);
        $table->column("name", "员工姓名")->width(150);
        $table->column("nickname", "员工昵称");
        $table->column("avatar", "头像")->image(40, 40);
        $table->column("phone", "手机");
        $table->column("email", "邮箱");
        $table->column("status", "启用状态")->switch(function ($value, $row) {

            $this->inactiveText("禁用")->activeText("启用")->width(55);

            $userId = app()->auth->getAdminId();
            // 自己跟超级管理员禁用
            if ($row->is_super_admin == 1 || $userId == $row['id']) {
                $this->disabled();
            }
        })->width(90);


        return $table;
    }


    /**
     * @param Form $form
     * @param Request $request
     * @return bool|Form
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function form(Form $form, Request $request)
    {
        $authList = $this->app->db->name('SystemAuth')
            ->field("id,pid,name as title")->where([
                'status' => 1,
                'plugin_name' => app()->http->getName(),
            ])->select();
        $authList = TreeArray::arr2tree($authList->toArray(), 'id', 'pid', 'children');

        $url = $this->createUrl('searchUser');
        $form->labelWidth(80);

        $form->radio('bind', '账户')->radioButton()->resolveUsing(function ($value, $row) {
            if (isset($row['id'])) {
                $this->disabled();
                return 1;
            }
            return 1;
        })->options([1 => '新账户', 2 => '绑定已有账户'])
            ->when('=', 1, function (Form $form) {
                $form->text('username', "账户")->resolveUsing(function ($value, $row) {
                    if (isset($row['id'])) {
                        $this->disabled();
                        return $row->user->username;
                    }
                    return '';
                })->rules("require");
            })
            ->when('=', 2, function (Form $form) use ($url) {
                $form->select('user_id', "账户")->setValueType('number')->remote($url)->resolveUsing(function ($value, $row) {
                    if (isset($row['id'])) {
                        $this->disabled();
                        return $value;
                    }
                    return '';
                })->rules("require");
            })->default(1);

        $form->text('name', "员工姓名")->rules("require");
        $form->text('nickname', "用户昵称")->rules("require");
        $form->text('phone', "手机")->rules('mobile');
        $form->text('email', "email")->rules("email");
        $form->image('avatar', "头像");
        $form->tree('auth_set', "角色")->options($authList)->fillUsing(function ($data, $model) {
            return $model->auth_set = implode(',', $data['auth_set']);
        })->resolveUsing(function ($value) {
            if (is_string($value)) {
                return explode(',', $value);
            }
        })->expandAll()->indent(28)->minWidth(90)->props("nodeKey", "id");

        return $form;
    }


    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [
            EditUserPasswordAction::make('设置密码')->canRun(function ($request, $model) {

                if ($model['plugin_name'] != app()->http->getName()) {
                    return false;
                }
                $userId = app()->auth->getAdminId();
//                超级管理员只能自己设置密码
                if ($model->is_super_admin == 1 && $userId != $model['id']) {
                    return false;
                }
                return true;
            })
        ];
    }


    /**
     * @AdminAuth(title="搜索会员",auth=true,login=true,menu=true)
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function searchUser()
    {
        $kewword = $this->request->post('query');
        $userList = SystemUser::where([
            'is_deleted' => 0,
        ])->where('username', 'like',"{$kewword}%")
            ->field('id as value,username as label')
            ->limit(30)
            ->select();
        return $this->success('success', $userList);
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [];
    }


    /**
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return $action->canRun(function ($request, $model) {
            if ($model['plugin_name'] != app()->http->getName()) {
                return false;
            }

            //超级管理员不能删除
            if ($model->is_super_admin == 1) {
                return false;
            }
            return true;
        });
    }

    /**
     * @param \quick\actions\RowAction|\quick\admin\actions\Action $action
     * @param Request $request
     * @return false|\phpDocumentor\Reflection\Types\Boolean|\quick\actions\RowAction|\quick\admin\actions\Action
     */
    protected function editAction($action, $request)
    {
        return $action->canRun(function ($request, $model) {
            if ($model['plugin_name'] != app()->http->getName()) {
                return false;
            }
            $userId = app()->auth->getAdminId();
            if ($model->is_super_admin == 1 && $userId != $model['id']) {
                return false;
            }

            return true;
        });
    }


    /**
     * @param \quick\admin\actions\Action $action
     * @param Request $request
     * @return \quick\admin\actions\Action
     */
    protected function addAction($action, $request)
    {
        return $action->beforeSaveUsing(function ($data, $request) {

            $data['plugin_name'] = app()->http->getName();
            if(empty($data['user_id'])){
                $user = SystemUser::where(['username' => $data['username'],'is_deleted' => 0])->find();
                if($user){
                    throw new \Exception('账户名称被占用');
                }

                $salt = CodeTools::random(5,2);
                $pass = CodeTools::random(20,2);
                $user = new SystemUser();
                $user->username = $data['username'];
                $user->salt = $salt;
                $user->password = SystemUser::hashPassword($data['password'] ?? $pass,$salt);
                $user->status = 1;
                $user->avatar =$data['avatar'] ?? '';
                $user->phone = $data['phone'] ?? '';
                $user->email = $data['email'] ?? '';

                $res = $user->save();
                if(!$res){
                    throw new \Exception('添加失败');
                }
            }else{
                $user = SystemUser::where(['id' => $data['user_id'],'is_deleted' => 0])->find();
                if(!$user){
                    throw new \Exception('账户不存在');
                }
                $admin = SystemAdminInfo::where(['user_id' => $user->id,'is_deleted' => 0,'plugin_name' => $data['plugin_name'] ])->find();
                if($admin){
                    throw new \Exception('账户已绑定');
                }
            }




            $data['user_id'] = $user->id;

            return $data;
        });
    }


}
