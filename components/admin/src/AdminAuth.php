<?php
declare(strict_types=1);

namespace components\admin\src;



use quick\admin\Quick;
use quick\admin\QuickTool;

class AdminAuth extends QuickTool
{

    public $name = "auth";



    public function resources():array
    {
        return Quick::scanDir(__DIR__);
    }


    /**
     * @return array
     * @throws \ReflectionException
     */
    public function script(): array
    {


        return [];
    }

    public function style(): array
    {
        return [];
    }
}
