<p align="center">
  <img src="https://demo.quickadmin.cn/upload/b7/420a26c63bb669ef6ea543e9a2a894.png" width="200" height="200" />
</p>

<h1 align="center">QuickAdmin</h1>

<p align="center">十分钟构建一个功能齐全的后台管理系统</p>

<p align="center">
  <a href="https://doc.quickadmin.cn" target="_blank">文档</a>
  <span>&nbsp;|&nbsp;</span>
  <a href="https://demo.quickadmin.cn" target="_blank">演示</a>
<p>



# QuickAdmin 

## 介绍

QuickAdmin是一款基于ThinkPHP6+Vue3+ElementPlus开箱即用的中后台管理系统框架，几分钟完成一个CRUD,
支持一键CRUD代码生成,日常开发无需重复的去创建各种vue，html页面， 只用php代码即可快速构建出一个功能完善的后台系统。


对比于其他admin类型产品，不需要为每个crud创建form,list页面，也不需要重新编译打包，只需简单的在php端编写维护resource类即可完成CRUD。
注重组件的复用，不需要重复为每个crud写form、list、table页面，我们把字段组件作为最小业务组件进行封装提供灵活高效的复用性，通过这些组件在后端配置组合成页面，
我们内置了丰富的组件足够您日常开发使用,你也可以自定义扩展组件满足你各种场景。




本项目前端基于 Fantastic-admin 修改定制,项目地址[https://gitee.com/hooray/fantastic-admin](https://gitee.com/hooray/fantastic-admin)


## 主要特性

* 快速开发CRUD
  * 支持一键生成CRUD,分钟级别的增删改查。
  * 只需要编写php代码即可完成CRUD
  * 丰富的`form`,`table`vue组件满足各种业务场景
  * 支持自定义`form-item`,`table-column`vue组件
* 强大的权限管理系统
  * 内置RBAC权限系统
  * 使用注解权限系统
  * 插件权限可独立设置，开发独立权限插件只需简单几步完成
* 强大的插件扩展功能
  * 可在线安装卸载升级插件
  * 插件权限可独立设置，开发独立插件只需简单几步完成
  
* 通用的会员模块和API模块
  * 统一账户管理
  * 内置会员模块，积分余额模块
  * 合理的api分层
  * 自动生成api文档


## 安装使用

1. 拉代码到本地
```
 git clone https://gitee.com/sciqtw/quickadmin.git
```
2. 把根目录.example.env重命名为.env文件
3. 修改.env配置debug=true开启调试模式
```
APP_DEBUG = true
```
4. 浏览器访问进入安装页面


[使用文档https://doc.quickadmin.cn](https://doc.quickadmin.cn)

## 在线演示

[https://demo.quickadmin.cn/](https://demo.quickadmin.cn)


用户名：admin

密　码：123456

## 问题反馈

在使用中有任何问题，请使用以下联系方式联系我们

1. 官方QQ交流群：[571438821](https://jq.qq.com/?_wv=1027&k=RIaWbSX2)


## 问题汇总
* 是否前后分离
  * Quick也可以支持前后分离开发，你可以把组件包独立出来给前端开发，前端只需要定制封装组件即可，不需要重复开发各种页面，前端开发好之后你再引入框架使用
  
* 开发新功能是否要编译
  * Quick内置丰富的组件足够满足各种场景开发，可以做到无需打包编译。
  * 如果内置组件无法满足开发需求，可以自己自定义组件，自定义组件提供免打包方式、打包方式进行开发。


## 特别鸣谢
 感谢巨人提供肩膀，排名不分先后
- [Thinkphp](http://www.thinkphp.cn/)
- [Vue](https://github.com/vuejs/core)
- [Element Plus](https://github.com/element-plus/element-plus)
- [Axios](https://github.com/axios/axios)
- [Fantastic-admin](https://gitee.com/hooray/fantastic-admin)

## 版权信息

QuickAdmin遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2021-2022 by QuickAdmin (https://www.quickadmin.cn)

All rights reserved。
