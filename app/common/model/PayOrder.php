<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class PayOrder
 *
 * @property integer  $id              
 * @property integer  $order_union_id  支付表ID
 * @property string   $order_no        用户订单号
 * @property float    $amount          支付金额
 * @property string   $plugin         插件名称
 * @property integer  $is_pay          支付状态:0=未支付,1=已支付
 * @property integer  $pay_code        支付方式
 * @property string   $title           支付标题
 * @property string   $notify_class    notify处理类
 * @property float    $refund          已退款金额
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 *
 * @package app\common\model
 */
class PayOrder extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'order_union_id' => 'require|integer',
            'order_no' => 'require|max:32',
            'amount' => 'require|float',
            'plugin' => 'require|max:32',
            'is_pay' => 'require|integer',
            'pay_code' => 'integer',
            'title' => 'require|max:128',
            'notify_class' => 'require|max:512',
            'refund' => 'float',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'order_union_id' => '支付表ID',
            'order_no' => '用户订单号',
            'amount' => '支付金额',
            'is_pay' => '支付状态',
            'pay_code' => '支付方式',
            'title' => '支付标题',
            'notify_class' => 'notify处理类',
            'refund' => '已退款金额',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
        ];
    }


    /**
     * 支付状态
     */
    public static function getIsPayList():array
    {
        return [
            0 => __('pay_order Is_pay 0'),
            1 => __('pay_order Is_pay 1'),
        ];
    }
    

  
}
