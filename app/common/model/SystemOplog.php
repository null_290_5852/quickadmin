<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class SystemOplog
 *
 * @property integer  $id              
 * @property integer  $type            日志类型:0=后台管理,1=用户接口
 * @property string   $node            当前操作节点
 * @property string   $geoip           操作者IP地址
 * @property string   $action          操作行为名称
 * @property string   $plugin          模块
 * @property integer  $user_id        账户id
 * @property string   $user_agent      User-Agent
 * @property string   $content         操作内容描述
 * @property string   $username        操作人用户名
 * @property string   $params          请求参数
 * @property string   $method          请求方法
 * @property string   $json_result     放回数据
 * @property string   $error_msg       错误信息
 * @property string   $created_at      创建日期
 *
 * @package app\common\model
 */
class SystemOplog extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'type' => 'integer',
            'node' => 'max:200',
            'geoip' => 'max:15',
            'action' => 'max:200',
            'admin_id' => 'integer',
            'user_agent' => 'max:255',
            'content' => 'max:1024',
            'username' => 'max:40',
            'method' => 'max:10',
            'error_msg' => 'max:4000',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'type' => '日志类型',
            'node' => '当前操作节点',
            'geoip' => '操作者IP地址',
            'action' => '操作行为名称',
            'admin_id' => '账户id',
            'user_agent' => 'User-Agent',
            'content' => '操作内容描述',
            'username' => '操作人用户名',
            'params' => '请求参数',
            'method' => '请求方法',
            'json_result' => '放回数据',
            'error_msg' => '错误信息',
            'created_at' => '创建日期',
        ];
    }


    /**
     * 日志类型
     */
    public static function getTypeList():array
    {
        return [
            0 => __('Type 0'),
            1 => __('Type 1'),
        ];
    }
    

  
}
