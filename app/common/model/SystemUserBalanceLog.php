<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class SystemUserBalanceLog
 *
 * @property integer  $id              
 * @property integer  $user_id         
 * @property integer  $type            类型:1=收入,2=支出
 * @property integer  $num             变动数量
 * @property integer  $current_num     当前余额-变动后
 * @property string   $desc            变动说明
 * @property string   $full_desc       自定义详细说明|记录
 * @property string   $sign            关联订单标识
 * @property string   $order_no        订单号
 * @property string   $created_at      创建日期
 *
 * @package app\common\model
 */
class SystemUserBalanceLog extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'user_id' => 'require|integer',
            'type' => 'require|integer',
            'num' => 'require|float',
            'current_num' => 'require|float',
            'desc' => 'require|max:255',
            'full_desc' => 'require',
            'sign' => 'require|max:155',
            'order_no' => 'require|max:255',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'type' => '类型',
            'num' => '变动数量',
            'current_num' => '当前余额-变动后',
            'desc' => '变动说明',
            'full_desc' => '自定义详细说明|记录',
            'sign' => '关联订单标识',
            'order_no' => '订单号',
            'created_at' => '创建日期',
        ];
    }


    /**
     * 类型
     */
    public static function getTypeList():array
    {
        return [
            1 => __('Type 1'),
            2 => __('Type 2'),
        ];
    }
    

    public function user()
    {
        return $this->belongsTo(SystemUser::class,'user_id');
    }
  
}
