<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class PayType
 *
 * @property integer  $id              
 * @property string   $plugin          插件名称
 * @property string   $pay_code        支付标识
 * @property string   $title           支付名称
 * @property string   $class           类地址
 * @property integer  $status          状态:0=禁用,1=启用
 * @property string   $config          支付配置
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 *
 * @package app\common\model
 */
class PayType extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'plugin' => 'require|max:32',
            'pay_code' => 'require|max:20',
            'title' => 'require|max:60',
            'class' => 'require|max:255',
            'status' => 'require|integer',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'plugin' => '插件名称',
            'pay_code' => '支付标识',
            'title' => '支付名称',
            'class' => '类地址',
            'status' => '状态',
            'config' => '支付配置',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
        ];
    }


    /**
     * 状态
     */
    public static function getStatusList():array
    {
        return [
            0 => __('pay_type Status 0'),
            1 => __('pay_type Status 1'),
        ];
    }
    

  
}
