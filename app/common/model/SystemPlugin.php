<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class SystemPlugin
 *
 * @property integer  $id              自增id
 * @property string   $name            插件key
 * @property string   $display_name    显示名称
 * @property string   $desc            描述
 * @property string   $avatar          图标
 * @property string   $version         版本号
 * @property string   $sql_version     数据版本
 * @property integer  $status          状态 1:启用, 0:禁用
 * @property integer  $is_deleted      删除: 1已删除 0未删除
 * @property integer  $deleted_at      删除日期
 * @property integer  $create_by       创建人admin_id
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 * @property integer  $update_by       修改人admin_id
 *
 * @package app\common\model
 */
class SystemPlugin extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'name' => 'max:255',
            'display_name' => 'max:255',
            'desc' => 'max:255',
            'avatar' => 'max:255',
            'version' => 'max:255',
            'sql_version' => 'max:50',
            'status' => 'integer',
            'is_deleted' => 'integer',
            'deleted_at' => 'integer',
            'create_by' => 'integer',
            'update_by' => 'integer',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => '自增id',
            'name' => '插件key',
            'display_name' => '显示名称',
            'desc' => '描述',
            'avatar' => '图标',
            'version' => '版本号',
            'sql_version' => '数据版本',
            'status' => '状态 1',
            'is_deleted' => '删除',
            'deleted_at' => '删除日期',
            'create_by' => '创建人admin_id',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
            'update_by' => '修改人admin_id',
        ];
    }


    

  
}
