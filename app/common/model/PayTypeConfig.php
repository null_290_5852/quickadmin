<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class PayTypeConfig
 *
 * @property integer  $id              
 * @property integer  $code            权限编号
 * @property string   $plugin          插件名称
 * @property string   $pay_code        支付标识
 * @property integer  $status          状态:0=禁用,1=启用
 * @property string   $config          支付配置
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 *
 * @package app\common\model
 */
class PayTypeConfig extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'code' => 'require|integer',
            'plugin' => 'require|max:40',
            'pay_code' => 'require|max:20',
            'status' => 'require|integer',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'code' => '权限编号',
            'plugin' => '插件名称',
            'pay_code' => '支付标识',
            'status' => '状态',
            'config' => '支付配置',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
        ];
    }


    /**
     * 状态
     */
    public static function getStatusList():array
    {
        return [
            0 => __('pay_type_config Status 0'),
            1 => __('pay_type_config Status 1'),
        ];
    }

    /**
     * @return \think\model\relation\HasOne
     */
    public function payType()
    {
        return $this->hasOne(PayType::class,'pay_code','pay_code');
    }
  
}
