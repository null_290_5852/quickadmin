<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class PayRefund
 *
 * @property integer  $id              
 * @property integer  $user_id         用户ID
 * @property string   $order_no        退款单号
 * @property string   $out_trade_no    支付单号
 * @property float    $amount          退款金额
 * @property integer  $is_pay          支付状态:0=未支付,1=已支付
 * @property string   $pay_code        支付方式
 * @property string   $title           支付内容
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 *
 * @package app\common\model
 */
class PayRefund extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'user_id' => 'require|integer',
            'order_no' => 'require|max:255',
            'out_trade_no' => 'require|max:255',
            'amount' => 'require|float',
            'is_pay' => 'require|integer',
            'pay_code' => 'require|max:100',
            'title' => 'require|max:128',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'user_id' => '用户ID',
            'order_no' => '退款单号',
            'out_trade_no' => '支付单号',
            'amount' => '退款金额',
            'is_pay' => '支付状态',
            'pay_code' => '支付方式',
            'title' => '支付内容',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
        ];
    }


    /**
     * 支付状态
     */
    public static function getIsPayList():array
    {
        return [
            0 => __('pay_refund Is_pay 0'),
            1 => __('pay_refund Is_pay 1'),
        ];
    }
    

  
}
