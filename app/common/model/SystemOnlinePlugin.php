<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\library\cloud\CloudService;
use think\paginator\driver\Bootstrap;


/**
 * Class SystemOnlinePlugin
 * @package app\common\model
 */
class SystemOnlinePlugin
{


    public function paginate($listRows = 20, $simple = false)
    {
        $page = request()->param('page/d',1);
        $type = request()->param('type/d',1);


        if($type == -2){
            $localList = $this->getLocalData($page);
            $dataTotal  = $localList['total'];
            $data  = array_values($localList['list']);
        }else{
            $localList = $this->getLocalData(0);
            $local  = $localList['list'];
            $res = CloudService::instance()->pluginList($page,$listRows);
            $data = (isset($res['code']) && $res['code'] == 0) ? $res['data']['data']:[];
            $dataTotal = $res['data']['total'] ?? 0;
            foreach ($data as &$item){
                $item['is_install'] = 0;
                if(isset($local[$item['name']])){
                    $item['is_install'] = 1;

                    $item['can_update'] = 0;
                    $item['version'] = $local[$item['name']]['version'];
                    $item['status'] = $local[$item['name']]['status'];

                    if(isset($item['addonVersion']) && count($item['addonVersion']) > 0){
                        foreach ($item['addonVersion'] as $ver){
                            if(isset($local[$item['name']]['version']) && version_compare($local[$item['name']]['version'],$ver['version'],"<") !== false){
                                $item['can_update'] = 1;
                            }
                        }

                    }

                    unset($local[$item['name']]);
                }else{
                    $item['can_update'] = 0;

                }
            }

            if(!empty($local)){
                $data = array_merge($data,array_values($local));
            }
        }



        $paginate = Bootstrap::make($data,$listRows,$page,$dataTotal);
        return $paginate;
    }

    public function getLocalData($page,$limit = 20)
    {
        $where = ['is_deleted' => 0];
        $total = SystemPlugin::where($where)->count();
        if($page){
            $localList = SystemPlugin::where($where)->page($page,$limit)->select();
        }else{
            $localList = SystemPlugin::where($where)->select();
        }

        $local = [];
        foreach ($localList as $v){
            $pluginInfo = pluginInfo($v['name']);
            if(empty($pluginInfo)){
                continue;
            }
            $v['is_install'] = 1;
            $v['can_update'] = 0;
            $v['addonVersion'] = [];
            $v['price'] = '无';
            $v['download_num'] = '-';
            $v['version'] = $pluginInfo['version'];
            $v['is_config'] = $pluginInfo['is_config'] ?? 0;
            $local[$v['name']] = $v;
        }
        return [
            'total' => $total,
            'list' => $local,
        ];
    }

    public function where($field, $op = null, $condition = null)
    {

        return $this;
    }
}
