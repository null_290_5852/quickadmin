<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class PayOrderUnion
 *
 * @property integer  $id              
 * @property integer  $user_id         用户id
 * @property string   $pay_no        系统支付单号
 * @property float    $amount          支付金额
 * @property integer  $is_pay          支付状态:0=未支付,1=已支付
 * @property string   $pay_code        支付方式
 * @property string   $title           支付内容
 * @property string   $support_pay_types 支持的支付方式（JSON）
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 *
 * @package app\common\model
 */
class PayOrderUnion extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'user_id' => 'require|integer',
            'pay_no' => 'require|max:32',
            'amount' => 'require|float',
            'is_pay' => 'require|integer',
            'pay_code' => 'max:20',
            'title' => 'require|max:128',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'user_id' => '用户id',
            'pay_no' => '系统支付单号',
            'plugin' => '插件名称',
            'amount' => '支付金额',
            'is_pay' => '支付状态',
            'pay_code' => '支付方式',
            'title' => '支付内容',
            'support_pay_types' => '支持的支付方式（JSON）',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
        ];
    }


    /**
     * 支付状态
     */
    public static function getIsPayList():array
    {
        return [
            0 => __('pay_order_union Is_pay 0'),
            1 => __('pay_order_union Is_pay 1'),
        ];
    }
    

  
}
