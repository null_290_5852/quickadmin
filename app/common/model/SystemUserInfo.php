<?php
declare (strict_types=1);

namespace app\common\model;

use think\Model;


/**
 * Class SystemUserInfo
 *
 * @property bool $is_deleted
 * @property float $balance 余额
 * @property float $total_balance 总余额
 * @property int $id 自增id
 * @property int $integral 积分
 * @property int $parent_id 上级id
 * @property int $temp_parent_id 临时上级
 * @property int $total_integral 最高积分
 * @property int $user_id 账号id
 * @property string $contact_way 联系方式
 * @property string $gender 性别
 * @property string $platform 用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp
 * @property string $platform_openid 平台id 如微信 openid
 * @property string $remark 备注
 * @property string $motto 个性签名
 * @package app\common\model
 */
class SystemUserInfo extends BaseModel
{

    protected $name = 'system_user_info';

    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'user_id' => 'integer',
            'integral' => 'integer',
            'total_integral' => 'integer',
            'balance' => 'float',
            'total_balance' => 'float',
            'contact_way' => 'max:255',
            'remark' => 'max:255',
            'motto' => 'max:255',
            'parent_id' => 'integer',
            'temp_parent_id' => 'integer',
            'platform_openid' => 'max:255',
            'platform' => 'max:255',
            'is_deleted' => 'integer',
        ];
    }


    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => '自增id',
            'user_id' => '账号id',
            'gender' => '性别',
            'integral' => '积分',
            'total_integral' => '最高积分',
            'balance' => '余额',
            'total_balance' => '总余额',
            'contact_way' => '联系方式',
            'remark' => '备注',
            'motto' => '个性签名',
            'parent_id' => '上级id',
            'temp_parent_id' => '临时上级',
            'platform_openid' => '平台id 如微信 openid',
            'platform' => '用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp',
            'is_deleted' => 'is_deleted',
        ];
    }

    public function user()
    {
        return $this->belongsTo(SystemUser::class,'user_id');
    }

    /**
     * 性别
     */
    public static function getGenderList():array
    {
        return [
            'male' => __('system_user_info Gender male'),
            'female' => __('system_user_info Gender female'),
            'unknow' => __('system_user_info Gender unknow'),
        ];
    }

}
