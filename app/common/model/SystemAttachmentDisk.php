<?php
declare (strict_types = 1);

namespace app\common\model;

use quick\admin\http\model\Model;

/**
 * Class SystemAttachmentDisk
 *
 * @property integer  $id              
 * @property integer  $storage_id      用户仓储ID
 * @property string   $type            类型
 * @property string   $config          配置
 * @property integer  $is_deleted      删除:0=未删除,1=已删除
 * @property integer  $deleted_at      删除日期
 * @property string   $created_at      创建日期
 * @property string   $updated_at      更新日期
 *
 * @package app\common\model
 */
class SystemAttachmentDisk extends Model
{


    /**
     * 验证规则
     * @return array
     */
    protected function rules(): array
    {
        return [
            'id' => 'integer',
            'storage_id' => 'require|integer',
            'type' => 'require|max:128',
            'desc' => 'max:225',
            'config' => 'max:2000',
            'is_deleted' => 'integer',
            'deleted_at' => 'integer',
        ];
    }

  
    /**
     * @return array
     */
    protected function attrLabels(): array
    {
        return [
            'id' => 'id',
            'storage_id' => '用户仓储ID',
            'type' => '类型',
            'desc' => '说明',
            'config' => '配置',
            'is_deleted' => '删除',
            'deleted_at' => '删除日期',
            'created_at' => '创建日期',
            'updated_at' => '更新日期',
        ];
    }


    /**
     * 删除
     */
    public static function getIsDeletedList():array
    {
        return [
            0 => __('system_attachment_disk Is_deleted 0'),
            1 => __('system_attachment_disk Is_deleted 1'),
        ];
    }


    public static function getStorageId(string $plugin)
    {
        $pluginObj = plugin($plugin);
        $storageKey = $pluginObj->uploadStorageKey();
        $map1 = [
            ['app_key', '=', $storageKey],
            ['is_deleted', '=',0],
        ];
        $storage = SystemAttachmentStorage::where($map1)->find();
        if(!$storage){
            $storage = SystemAttachmentStorage::create([
                'app_key' => $storageKey,
                'is_deleted' => 0,
                'desc' => $plugin
            ]);
        }
        return $storage->id;
    }

}
