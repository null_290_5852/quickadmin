<?php

namespace app\common\service\account;

use app\common\service\CommonService;

class CreateAccountService extends CommonService
{

    /**
     * 验证规则
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'integer',
            'parent_id' => 'require|integer',
            'storage_id' => 'require|integer',
            'name' => 'require|max:64',
        ];
    }


    /**
     * @return array
     */
    public function attrLabels(): array
    {
        return [];
    }


    /**
     * @return array|void
     */
    public function create()
    {
        if (!$this->validate()) {
            return $this->error($this->getFirstError());
        }

    }
}