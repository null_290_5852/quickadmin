<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/29
 */

namespace app\common\service\payment;

use think\facade\Log;

class PaymentNotify
{

    public function notify($plugin,$payType,$authCode)
    {
        $str = $plugin.'/'.$payType.'/'.$authCode;
        Log::info("-----notify--request--{$str}--".json_encode(request()->param()));
        $payment = Payment::instance($plugin,$authCode);
        $payment->notify($payType);
    }
}