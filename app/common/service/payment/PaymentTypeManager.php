<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/13
 */

namespace app\common\service\payment;

use app\common\model\PayType;
use quick\admin\Service;

class PaymentTypeManager extends Service
{


    /**
     * @param string $payCode
     * @param array $options
     * @return PayTypeInterface
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getPayByCode(string $payCode,array $options = []):PayTypeInterface
    {
        $payCodeModel  = PayType::where(['pay_code' => $payCode])->find();
        if(!$payCodeModel || empty($payCodeModel->class)){
            throw new \Exception("暂不支持该支付方式:".$payCode);
        }

        if(!$payCodeModel->status){
            throw new \Exception("支付方式已经停用");
        }

        return invoke($payCodeModel->class,[$options]);
    }
}