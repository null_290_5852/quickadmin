<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/29
 */

namespace app\common\service\payment;

use app\common\model\PayOrder;

interface  PayNotifyInterface
{

    public function notify(PayOrder $payOrder);
}