<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/13
 */

namespace app\common\service\payment;

use app\common\service\CommonService;

class OrderDTO extends CommonService
{
    public $orderNo;
    public $amount;
    public $title;
    public $notifyClass;
    public $payType;
    public $plugin;
    public $supportPayTypes = [];


    /**
     * PaymentOrder constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct($config = [])
    {
        $this->attributes = $config;
        if (!$this->validate()) {
            throw new \Exception($this->getFirstError());
        }
    }


    public function rules(): array
    {
        return [
            "orderNo" => "require",
            "title" => "require",
            "plugin" => "require",
            "amount" => "require",
            "notifyClass" => "require",
            "payType" => "max:20",
        ];
    }

}