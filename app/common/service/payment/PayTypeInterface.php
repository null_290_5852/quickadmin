<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/13
 */

namespace app\common\service\payment;

use app\common\model\PayOrderUnion;
use plugins\mall\service\payment\PaymentOrder;

interface PayTypeInterface
{


    /**
     * 获取支付参数
     * @param PayOrderUnion $paymentOrderUnion
     * @param array $data
     * @return array
     */
    public function payData(PayOrderUnion $paymentOrderUnion,array $data):array;


    /**
     * 处理异步数据
     * @return mixed
     */
    public function notify();

    /**
     * 查询订单
     * @param string $paymentOrder
     * @return mixed
     */
    public function query(string $paymentOrder);

    /**
     * 统一退款
     * @param PaymentOrder $paymentOrder
     * @return mixed
     */
    public function refundOrder(PaymentOrder $paymentOrder);

    /**
     * 退款查询
     * @param string $paymentOrder
     * @return mixed
     */
    public function refundQuery(string $paymentOrder);

    /**
     * 退款异步通知
     * @param PaymentOrder $paymentOrder
     * @return mixed
     */
    public function refundNotify(PaymentOrder $paymentOrder);

    /**
     * 配置项
     * @return mixed
     */
    public function settingForm();
}