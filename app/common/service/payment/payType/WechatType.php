<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/14
 */

namespace app\common\service\payment\payType;

use app\common\model\PayOrderUnion;
use app\common\service\payment\PayTypeInterface;
use plugins\mall\service\payment\PaymentOrder;
use quick\admin\form\Form;

/**
 * 微信支付
 */
class WechatType implements PayTypeInterface
{

    protected array $options;


    public function __construct($options = [])
    {
        $this->options = $options;

//        var_dump($this->options);die;
//        if(!empty($this->options['cert_public'])){
//            $this->options['cert_public'] = "-----BEGIN CERTIFICATE-----\n" .
//                wordwrap($this->options['cert_public'], 64, "\n", true) .
//                "\n-----END CERTIFICATE-----";
//        }

    }

    /**
     * 支付配置表单
     * @return mixed|Form
     */
    public function settingForm()
    {
//        $default =[
//            'appid'        => '', // 微信绑定APPID，需配置
//            'mch_id'       => '', // 微信商户编号，需要配置
//            'mch_v3_key'   => '', // 微信商户密钥，需要配置
//            'cert_serial'  => '', // 商户证书序号，无需配置
//            'cert_public'  => '', // 商户公钥内容，需要配置
//            'cert_private' => '', // 商户密钥内容，需要配置
//        ];


        $form = Form::make();
        $form->text('appid', '微信绑定APPID')->width(300)->required();
        $form->text('mch_id', '微信商户编号')->width(300)->required();
        $form->text('mch_v3_key', '微信商户密钥')->width(300)->required();
        $form->text('cert_public', '商户公钥内容')->width(500)->textarea();
        $form->text('cert_private', '商户密钥内容')->width(500)->textarea();

        return $form;
    }

    public function payData(PayOrderUnion $paymentOrderUnion, array $data = []): array
    {
        $payment = \WePayV3\Order::instance($this->options);

        if (empty($data['pay_type'])) {

        } elseif ($data['pay_type'] == 'native') {
            $info = [
                'appid' => $this->options['appid'],
                'mchid' => $this->options['mch_id'],
                'description' => $paymentOrderUnion->title,
                'out_trade_no' => $paymentOrderUnion->pay_no,
                'notify_url' => $data['notifyUrl'],//http://quicktest.com/index.php/admin/pay/plugin/wechat/2
                'amount' => [
                    'total' => $paymentOrderUnion->amount * 100,
                    'currency' => 'CNY'
                ],
            ];
        } elseif ($data['pay_type'] == 'jsapi') {
            $info = [
                'appid' => $this->options['appid'],
                'mchid' => $this->options['mch_id'],
                'description' => $paymentOrderUnion->title,
                'out_trade_no' => $paymentOrderUnion->pay_no,
                'notify_url' => $data['notifyUrl'],
                'amount' => [
                    'total' => $paymentOrderUnion->amount * 100,
                ],
                "payer" => [
                    "openid" => $data['openid'],//
                ]
            ];
        }
        $result = $payment->create($data['pay_type'], $info);

        return $result;
    }

    public function notify()
    {

        $payment = \WePayV3\Order::instance($this->options);
        $data = $payment->notify();
        return $data;
    }

    public function query(string $orderNo)
    {
        $payment = \WePayV3\Order::instance($this->options);
        $data = $payment->query($orderNo);
    }

    public function refundOrder(PaymentOrder $paymentOrder)
    {
        $payment = \WePayV3\Refund::instance($this->options);
        $data = $payment->create([]);
    }

    public function refundQuery(string $orderNo)
    {
        $payment = \WePayV3\Refund::instance($this->options);
        $data = $payment->query($orderNo);
    }

    public function refundNotify($orderNo)
    {
        $payment = \WePayV3\Refund::instance($this->options);
        $data = $payment->notify($orderNo);
    }


}