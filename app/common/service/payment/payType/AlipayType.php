<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/14
 */

namespace app\common\service\payment\payType;

use app\common\model\PayOrderUnion;
use app\common\service\payment\PayTypeInterface;
use plugins\mall\service\payment\PaymentOrder;
use quick\admin\form\Form;

/**
 * 支付宝支付
 */
class AlipayType implements PayTypeInterface
{

    public function payData(PayOrderUnion $paymentOrderUnion,array $data): array
    {
        // TODO: Implement payData() method.
        return [];
    }

    public function notify()
    {
        // TODO: Implement notify() method.
    }

    public function query(string $paymentOrder)
    {
        // TODO: Implement query() method.
    }

    public function refundOrder(PaymentOrder $paymentOrder)
    {
        // TODO: Implement refundOrder() method.
    }

    public function refundQuery(string $paymentOrder)
    {
        // TODO: Implement refundQuery() method.
    }

    public function refundNotify(PaymentOrder $paymentOrder)
    {
        // TODO: Implement refundNotify() method.
    }

    /**
     * 支付配置表单
     * @return mixed|Form
     */
    public function settingForm()
    {
        $form = Form::make();
        $form->text('app_key','app_key');
        $form->text('mch_no','商务号');
        $form->text('ali','支付宝');

        return $form;
    }
}