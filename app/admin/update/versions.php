<?php
/**
 * 版本升级文件
 */
return [
    '1.0.1' => function () {
        $sql = <<<EOF
ALTER TABLE `qk_system_admin_info` ADD COLUMN `is_super_admin` tinyint(1) NOT NULL DEFAULT 0 COMMENT '超级管理员' AFTER `status`;
ALTER TABLE `qk_system_attachment` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_attachment_cate` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_attachment_storage` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_auth` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `status`;
ALTER TABLE `qk_system_node` ADD COLUMN `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求类型json' AFTER `title`;
ALTER TABLE `qk_system_node` ADD COLUMN `is_log` tinyint(1) NULL DEFAULT 0 COMMENT '记录日志' AFTER `is_login`;
ALTER TABLE `qk_system_node` MODIFY COLUMN `plugin_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '系统插件plugin' AFTER `id`;
ALTER TABLE `qk_system_node` MODIFY COLUMN `mode` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节点类型:controller、resource' AFTER `pnode`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `type` tinyint(1) NULL DEFAULT 0 COMMENT '日志类型:0=后台管理,1=用户接口' AFTER `id`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `plugin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块' AFTER `type`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '账户id' AFTER `action`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `user_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'User-Agent' AFTER `user_id`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `params` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数' AFTER `username`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求方法' AFTER `params`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `json_result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '放回数据' AFTER `method`;
ALTER TABLE `qk_system_oplog` ADD COLUMN `error_msg` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '错误信息' AFTER `json_result`;
ALTER TABLE `qk_system_oplog` MODIFY COLUMN `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作人用户名' AFTER `content`;
ALTER TABLE `qk_system_plugin` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `is_deleted`;
ALTER TABLE `qk_system_user` MODIFY COLUMN `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期' AFTER `login_at`;
CREATE TABLE `qk_system_user_balance_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `num` decimal(10, 2) NOT NULL COMMENT '变动数量',
  `current_num` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '当前余额-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '余额记录' ROW_FORMAT = Dynamic;
ALTER TABLE `qk_system_user_info` ADD COLUMN `motto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '个性签名' AFTER `remark`;
CREATE TABLE `qk_system_user_integral_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '类型:1=收入,2=支出',
  `integral` int(11) NOT NULL COMMENT '变动积分',
  `current_integral` int(11) NOT NULL DEFAULT 0 COMMENT '当前积分-变动后',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '变动说明',
  `full_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '自定义详细说明|记录',
  `sign` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '关联订单标识',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '积分记录' ROW_FORMAT = Dynamic;

EOF;
      executeSql($sql);
    },
    '1.0.2' => function () {
        $sql = <<<EOF
        ALTER TABLE `qk_system_admin_info` MODIFY COLUMN `auth_set` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限' AFTER `plugin_name`;
        ALTER TABLE `qk_system_auth` DROP COLUMN `node_set`;
        ALTER TABLE `qk_system_oplog` MODIFY COLUMN `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数' AFTER `username`;
EOF;
        executeSql($sql);
    },
    '1.0.3' => function () {
        $sql = <<<EOF
ALTER TABLE `qk_system_oplog` MODIFY COLUMN `params` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求参数' AFTER `username`;
ALTER TABLE `qk_system_plugin` ADD COLUMN `sql_version` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '数据版本' AFTER `version`;
ALTER TABLE `qk_system_user_info` MODIFY COLUMN `gender` enum('male','female','unknow') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'unknow' COMMENT '性别:male=男,female=女,unknow=未知' AFTER `user_id`;
EOF;
        executeSql($sql);
    },
    '1.0.4' => function () {
        $sql = <<<EOF
CREATE TABLE `qk_pay_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '插件名称',
  `order_union_id` int(11) NOT NULL COMMENT '支付表ID',
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户订单号',
  `amount` decimal(9, 2) NOT NULL COMMENT '支付金额',
  `is_pay` int(1) NOT NULL DEFAULT 0 COMMENT '支付状态:0=未支付,1=已支付',
  `pay_code` int(1) NOT NULL DEFAULT 0 COMMENT '支付方式',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付标题',
  `notify_class` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'notify处理类',
  `refund` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '已退款金额',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_order_union_id`(`order_union_id`) USING BTREE,
  INDEX `idx_plugin`(`plugin`) USING BTREE,
  INDEX `idx_order_no`(`order_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付订单表' ROW_FORMAT = Dynamic;

CREATE TABLE `qk_pay_order_union`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `pay_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统支付单号',
  `amount` decimal(9, 2) NOT NULL COMMENT '支付金额',
  `is_pay` int(1) NOT NULL DEFAULT 0 COMMENT '支付状态:0=未支付,1=已支付',
  `pay_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '支付方式',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付内容',
  `support_pay_types` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支持的支付方式（JSON）',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_order_no`(`pay_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付订单关联表' ROW_FORMAT = Dynamic;

CREATE TABLE `qk_pay_refund`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款单号',
  `out_trade_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '支付单号',
  `amount` decimal(9, 2) NOT NULL DEFAULT 0.00 COMMENT '退款金额',
  `is_pay` int(1) NOT NULL DEFAULT 0 COMMENT '支付状态:0=未支付,1=已支付',
  `pay_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '支付方式',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付内容',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_order_no`(`order_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '退款订单' ROW_FORMAT = Dynamic;

CREATE TABLE `qk_pay_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付标识',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付名称',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付说明',
  `class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类地址',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态:0=禁用,1=启用',
  `config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支付配置',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_pay_code`(`pay_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付类型' ROW_FORMAT = Dynamic;

CREATE TABLE `qk_pay_type_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(11) NOT NULL DEFAULT 0 COMMENT '权限编号',
  `plugin` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件名称',
  `pay_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '支付标识',
  `status` int(1) NOT NULL DEFAULT 0 COMMENT '状态:0=禁用,1=启用',
  `config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '支付配置',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_plugin`(`plugin`) USING BTREE,
  INDEX `idx_pay_code`(`pay_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '支付参数配置' ROW_FORMAT = Dynamic;

CREATE TABLE `qk_system_attachment_disk`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL COMMENT '用户仓储ID',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除:0=未删除,1=已删除',
  `deleted_at` int(11) NOT NULL DEFAULT 0 COMMENT '删除日期',
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建日期',
  `updated_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新日期',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sti`(`storage_id`, `type`, `is_deleted`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '物理储存' ROW_FORMAT = Dynamic;
ALTER TABLE `qk_system_attachment_storage` DROP INDEX `idx_user_id`;
ALTER TABLE `qk_system_attachment_storage` ADD COLUMN `app_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用key' AFTER `id`;
ALTER TABLE `qk_system_attachment_storage` ADD COLUMN `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '仓库说明' AFTER `app_key`;
ALTER TABLE `qk_system_attachment_storage` DROP COLUMN `plugin_name`;
ALTER TABLE `qk_system_attachment_storage` DROP COLUMN `user_id`;
ALTER TABLE `qk_system_attachment_storage` ADD INDEX `idx_app_key`(`app_key`) USING BTREE;
ALTER TABLE `qk_system_plugin` ADD COLUMN `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置' AFTER `status`;
EOF;
        executeSql($sql);
    }
];