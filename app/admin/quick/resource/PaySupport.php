<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/14
 */

namespace app\admin\quick\resource;

use app\admin\quick\actions\SettingPayAction;
use app\common\model\PayType;
use quick\admin\actions\Action;
use quick\admin\actions\BatchDeleteAction;
use quick\admin\annotation\AdminAuth;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\Resource;
use quick\admin\table\Query;
use quick\admin\table\Table;
use think\Request;

/**
 *
 * @AdminAuth(title="支付方式",auth=true,login=true,menu=true)
 * @package app\admin\resource\auth
 */
class PaySupport extends Resource
{
    /**
     * @var string
     */
    protected $title = '支付方式';

    /**
     * @var string
     */
    protected $description = "支付列表";

    /**
     * @var string
     */
    protected static $model = "app\\common\\model\\PayTypeConfig";


    /**
     * 可搜索字段
     *
     * @var array
     */
    public static $search = [];


    /**
     * @param Filter $filter
     * @return Filter
     */
    protected function filters(Filter $filter)
    {

        $filter->labelWidth("100");
        $filter->like("title", "title")->width(12);

        return false;
    }

    protected function model(Query $model)
    {
        $where = [];
        $model->where($this->getWhere($where))->json(['value']);
        return $model->order("id desc");
    }

    private function getWhere(&$data):array
    {
        $app_name = app()->http->getName();
        $data['code'] = app()->auth->getAdminId();
        $data['plugin'] = $app_name;
        return $data;
    }


    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {

        $table->column('id', 'ID');
        $table->column('pay_code', '支付编号');
        $table->column('payType.title', '名称');
        $table->column('payType.desc', '说明');
        $table->column("status", "启用状态")->switch(function () {
            $this->inactiveText("禁用")->activeText("启用")->width(55);
        })->width(90);
        return $table;
    }


    /**
     * @param Form $form
     * @param Request $request
     * @return bool|mixed|Form
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function form(Form $form, Request $request)
    {


        $form->labelWidth(100);

        $payTypeList = PayType::field('pay_code as value,title as label')->where(['status' => 1])->select()->toArray();
        $form->select('pay_code', '支付方式')->options($payTypeList);
        $form->radio('status', '状态')->options([1 => '启用', 0 => '禁用'])->default(1);


        return $form;
    }


    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [
            SettingPayAction::make('设置')
        ];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [
            BatchDeleteAction::make("删除")
        ];
    }


    /**
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return $action->canRun(function ($request, $model) {
            if ($model['plugin'] != app()->http->getName()) {
                return false;
            }
            return true;
        });
    }


    /**
     * @param Action $action
     * @param Request $request
     * @return \quick\actions\RowAction
     */
    protected function editAction(Action $action, $request)
    {
        return false;
    }


    /**
     * @param Action $action
     * @param Request $request
     * @return Action
     */
    protected function addAction(Action $action, $request)
    {
        $app_name = app()->http->getName();
        $code = app()->auth->getAdminId();
        return $action->beforeSaveUsing(function ($data, $request) use ($code,$app_name) {

            $data['code'] = $code;
            $data['plugin'] = $app_name;
            $data['config'] = json_encode([]);
            return $data;
        });
    }

}