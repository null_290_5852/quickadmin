<?php
declare (strict_types = 1);
namespace app\admin\quick\resource;


use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\Resource;
use quick\admin\table\Query;
use quick\admin\table\Table;
use think\Request;


/**
 *
 * Class SystemUserIntegralLog
 * @AdminAuth(title="积分记录",auth=true,menu=true,login=true )
 * @package app\admin\quick\resource
 */
class SystemUserIntegralLog extends Resource
{
    /**
     * 标题
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * @var string
     */
    protected $description = "desc";

    /**
     * 关联模型 app\admin\model\SystemUserBalanceLog
     *
     * @var string
     */
    protected static $model = 'app\common\model\SystemUserIntegralLog';

    /**
     * 可搜索字段
     *
     * @var array
     */
    public static $search = [
        'user.username'
    ];

    protected string $searchPlaceholder = '输入用户名称搜索';

    protected function model(Query $model)
    {
        $model->withJoin('user');
        return $model;
    }


    /**
     * 过滤器
     * @param Request $request
     * @return array
     */
    protected function filters(Filter $filter)
    {
        return false;
    }


    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {
        
        $table->column('id', 'ID')->width(80)->sortable();
        $table->column('user.username', '用户名称')->sortable();
        $table->column('integral', '变动数量')
            ->display(function ($value,$model){
                if($model['type'] == 1){
                    $value =   '+'.$value;
                    $type = 'warning';
                }else{
                    $value =   '-'.$value;
                    $type = 'success';
                }
            return Component::custom('el-tag')->props('type',$type)->content($value);
        });
        $table->column('current_integral', '当前余额-变动后');
        $table->column('desc', '变动说明');
//        $table->column('full_desc', '自定义详细说明|记录')->html();
//        $table->column('sign', '关联订单标识');
        $table->column('order_no', '订单号');
        $table->column('created_at', '创建日期')->width(170);
        $table->disableActions();
        return $table;
    }



    /**
     * 定义form
     * @param Form $form
     * @param Request $request
     * @return Form
     * @throws \think\Exception
     */
    protected function form(Form $form, Request $request)
    {


        return $form;
    }



    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [];
    }


    /**
     * 设置删除
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return false;
    }

    /**
     * 设置编辑
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function editAction($action, $request)
    {
        return false;
    }


    /**
     *  设置添加
     * @param \quick\admin\actions\Action $action
     * @param Request $request
     * @return \quick\admin\actions\Action
     */
    protected function addAction($action, $request)
    {
        return false;
    }


}
