<?php
declare (strict_types = 1);
namespace app\admin\quick\resource;

use app\common\model\SystemUser as SystemUserModel;
use quick\admin\annotation\AdminAuth;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\Resource;
use quick\admin\table\Table;
use think\Request;


/**
 *
 * Class 账户管理
 * @AdminAuth(title="账户管理",auth=true,menu=true,login=true,log=false )
 * @package app\admin\quick\resource
 */
class SystemUser extends Resource
{
    /**
     * 标题
     *
     * @var string
     */
    protected $title = '账户管理';

    /**
     * @var string
     */
    protected $description = "账户管理";

    /**
     * 关联模型 app\common\model\SystemUser
     *
     * @var string
     */
    protected static $model = 'app\common\model\SystemUser';

    /**
     * 可搜索字段
     *
     * @var array
     */
    public static $search = ['username','nickname','phone'];


    /**
     * 过滤器
     * @param Request $request
     * @return array
     */
    protected function filters(Filter $filter)
    {
        $filter->date('created_at','加入时间')->datetimerange()->width(12);
        $filter->equal('phone','手机号码')->width(8);
        return $filter;
    }


    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {
        
        $table->column('id', 'ID')->width(80);
        $table->column('username', '账户名称');
        $table->column('nickname', '昵称');
        $table->column('avatar', '头像')->width(90)->image(40);
        $table->column('email', '电子邮箱');
        $table->column('phone', '手机号');
        $table->column('last_login_ip_at', '登录ip');
        $table->column('login_num', '登录次数');
        $table->column('login_fail_num', '失败次数');
        $table->column('login_at', '登录时间')->width(120);
        $table->column('created_at', '创建日期')->width(120);

        return $table;
    }



    /**
     * 定义form
     * @param Form $form
     * @param Request $request
     * @return Form
     * @throws \think\Exception
     */
    protected function form(Form $form, Request $request)
    {
        
//        $form->text('username', '账户名称')->rules('require|max:50');
        $form->text('nickname', '昵称')->rules('require|max:50');
        $form->text('avatar', '头像')->rules('require|max:150');
        $form->text('email', '电子邮箱')->rules('require|max:100');
        $form->text('phone', '手机号')->rules('require|max:15');
//        $form->text('password', '密码')->rules('require|max:32');
//        $form->text('salt', '密码盐')->rules('require|max:30');
//        $form->text('status', '状态 1')->rules('require|integer');
//        $form->text('last_login_ip_at', '登录ip')->rules('require|max:25');
////        $form->text('create_ip_at', '创建ip')->rules('require|max:25');
//        $form->inputNumber('login_num', '登录次数')->rules('require');
//        $form->text('login_fail_num', '失败次数')->rules('require|integer');
//        $form->text('login_at', 'Login_at');

        return $form;
    }



    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [];
    }


    /**
     * 设置删除
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return false;
    }

    /**
     * 设置编辑
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function editAction($action, $request)
    {
        return $action;
    }


    /**
     *  设置添加
     * @param \quick\admin\actions\Action $action
     * @param Request $request
     * @return \quick\admin\actions\Action
     */
    protected function addAction($action, $request)
    {
        return $action;
    }


}
