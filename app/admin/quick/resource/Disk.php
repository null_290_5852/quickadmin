<?php
declare (strict_types = 1);
namespace app\admin\quick\resource;

use app\admin\quick\actions\StorageConfigAction;
use app\common\model\SystemAttachmentDisk;
use quick\admin\annotation\AdminAuth;
use quick\admin\filter\Filter;
use quick\admin\form\Form;
use quick\admin\library\service\UploadService;
use quick\admin\Resource;
use quick\admin\table\Query;
use quick\admin\table\Table;
use think\Request;


/**
 *
 * Class Disk
 * @AdminAuth(title="上传配置",auth=true,menu=true,login=true )
 * @package app\admin\quick\resource
 */
class Disk extends Resource
{
    /**
     * 标题
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * @var string
     */
    protected $description = "desc";

    /**
     * 关联模型 app\common\model\SystemAttachmentDisk
     *
     * @var string
     */
    protected static $model = 'app\common\model\SystemAttachmentDisk';

    /**
     * 可搜索字段
     *
     * @var array
     */
    public static $search = [];


    public function model(Query $model)
    {
        $model->where('storage_id',$this->storageId());
        return $model;
    }

    private function storageId()
    {
        return SystemAttachmentDisk::getStorageId('admin');
    }



    /**
     * 过滤器
     * @param Request $request
     * @return array
     */
    protected function filters(Filter $filter)
    {
        return false;
    }


    /**
     * @param Table $table
     * @return Table
     * @throws \Exception
     */
    protected function table(Table $table)
    {

        $storageId = $this->storageId();
        $table->column('id', __('system_attachment_disk Id'))->width(90);
        $table->column('type', __('system_attachment_disk Type'));
        $table->column('desc', __('system_attachment_disk Desc'));
        $table->column('status', __('system_attachment_disk Status'))->switch(function () {
            $this->inactiveText("禁用")->activeText("启用")->width(55);
        },function ($value,$model) use ($storageId){
            if($value){
                SystemAttachmentDisk::where(['storage_id' => $storageId])->update(['status' => 0]);
            }
            return $value;
        });
        $table->column('created_at', __('system_attachment_disk Created_at'));

        return $table;
    }



    /**
     * 定义form
     * @param Form $form
     * @param Request $request
     * @return Form
     * @throws \think\Exception
     */
    protected function form(Form $form, Request $request)
    {

        $options = [];
        foreach (UploadService::$storageList as $key => $item){
            $options[$key] = $item['name'];
        }

        $form->radio('type', __('system_attachment_disk Type'))->radioButton()
            ->options($options)->resolveUsing(function ($value, $model){
                if(!empty($model)){
                    $this->disabled();
                }
                return $value;
            })->rules('require|max:128');
        $form->text('desc', __('system_attachment_disk Desc'))->rules('max:225');
//        $form->text('config', __('system_attachment_disk Config'))->rules('require|max:128');

        return $form;
    }



    /**
     * 注册行操作
     * @return array|mixed
     */
    protected function actions()
    {
        return [
            StorageConfigAction::make('设置')
        ];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [];
    }


    /**
     * 设置删除
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function deleteAction($action, $request)
    {
        return $action;
    }

    /**
     * 设置编辑
     * @param $action
     * @param $request
     * @return mixed
     */
    protected function editAction($action, $request)
    {
        return $action;
    }


    /**
     *  设置添加
     * @param \quick\admin\actions\Action $action
     * @param Request $request
     * @return \quick\admin\actions\Action
     */
    protected function addAction($action, $request)
    {
        $action->beforeSaveUsing(function ($data) {
            $data['storage_id'] = SystemAttachmentDisk::getStorageId('admin');
            $data['config'] = json_encode([]);
            return $data;
        });
        return $action;
    }


}
