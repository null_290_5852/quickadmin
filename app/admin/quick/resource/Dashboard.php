<?php

namespace app\admin\quick\resource;


use app\common\model\SystemPlugin;
use app\common\model\SystemUser;
use quick\admin\annotation\AdminAuth;
use quick\admin\components\Component;
use quick\admin\components\layout\Col;
use quick\admin\components\layout\Row;
use quick\admin\components\QkTimeline;
use quick\admin\Element;

/**
 * @AdminAuth(auth=true,menu=true,login=true,title="系统首页")
 * @package app\admin\quick\resource
 */
class Dashboard extends Resource
{
    /**
     * 标题
     *
     * @var string
     */
    protected $title = '欢迎使用 QuickAdmin';

    /**
     * @var string
     */
    protected $description = "QuickAdmin是一款基于ThinkPHP6+Vue3+ElementPlus开箱即用的中后台管理系统框架。";


    /**
     * @return Element
     */
    protected function display(): Element
    {






        $table = Component::qkDescriptions()->data([

            ['label' => 'ThinkPHP', 'content' => Component::custom("el-link")->props([
                'href' => 'https://www.kancloud.cn/manual/thinkphp6_0/1037479',
                'target' => '_blank',
                'type' => 'primary',
            ])->content('6.1.0')],
            ['label' => 'vue', 'content' => Component::custom("el-link")->props([
                'href' => 'https://cn.vuejs.org/guide/introduction.html',
                'target' => '_blank',
                'type' => 'primary',
            ])->content('3.0')],
            ['label' => 'element-plus', 'content' => Component::custom("el-link")->props([
                'href' => 'https://element-plus.gitee.io/zh-CN/',
                'target' => '_blank',
                'type' => 'primary',
            ])->content('2.2.9')],
            ['label' => 'Fantastic-Admin', 'content' => Component::custom("el-link")->props([
                'href' => 'https://gitee.com/hooray/fantastic-admin',
                'target' => '_blank',
                'type' => 'primary',
            ])->content('基础版')],
            ['label' => 'php版本', 'content' => '>=7.4'],
            ['label' => 'QuickAdmin', 'content' => '1.0.0.dev'],
        ])->border()->column(1);


        $content = Component::content()->cleanBackground();
        $content->title($this->title())
            ->description($this->description())
            ->body(function (Row $row) use ($table) {


                $userCount = SystemUser::withJoin('userInfo','inner')->where([
                    'system_user.is_deleted' => 0,
                    'userInfo.is_deleted' => 0,
                ])->count();


                $userDayCount = SystemUser::withJoin('userInfo','inner')->where([
                    'system_user.is_deleted' => 0,
                    'userInfo.is_deleted' => 0,
                ])->whereDay('created_at')->count();

                $userYesterdayCount = SystemUser::withJoin('userInfo','inner')->where([
                    'system_user.is_deleted' => 0,
                    'userInfo.is_deleted' => 0,
                ])->whereDay('created_at','yesterday')->count();

                $userLastMonthCount = SystemUser::withJoin('userInfo','inner')->where([
                    'system_user.is_deleted' => 0,
                    'userInfo.is_deleted' => 0,
                ])->whereMonth('created_at','last month')->count();
                $userMonthCount = SystemUser::withJoin('userInfo','inner')->where([
                    'system_user.is_deleted' => 0,
                    'userInfo.is_deleted' => 0,
                ])->whereMonth('created_at')->count();

                $pluginCount = SystemPlugin::where([
                    'is_deleted' => 0,
                ])->count();

                $height = '156px';

                $ratioFunc = function ($curVal,$oldVal){
                    $ratio =  empty($oldVal) ? $curVal:(empty($curVal) ? 0 - $oldVal:($curVal/$oldVal) - 1);
                    return $ratio*100;
                };



                // 当月数据
                $monthRatio = $ratioFunc($userMonthCount,$userLastMonthCount);
                $monthCom = Component::iconCard('月新增', $userMonthCount)->tagText('月')->percentageText('同比')->percentageNum($monthRatio)->themeBlue()->props('height',$height);
                if($monthRatio> 0){
                    $monthCom->up();
                }else{
                    $monthCom->down();
                }


                // 今日数据
                $userDayRatio = $ratioFunc($userDayCount,$userYesterdayCount);
                $dayCom = Component::iconCard('今日新增', $userDayCount,'el-icon-User')->themeBlue()->tagText('日')->down()->compareTitle('同比')
                    ->compareText($userDayRatio.'%')->props('height',$height);
                if($userDayRatio > 0){
                    $dayCom->up();
                }else{
                    $dayCom->down();
                }

                $row->props('gutter', 15);

                $row->col(6, $dayCom)
                    ->col(6,$monthCom)
                    ->col(6,
                    Component::iconCard('会员总数', $userCount,'el-icon-User')
                        ->iconLeft()->tagText('总')->themeRed()->props('height',$height)
                    )
                    ->col(6, Component::iconCard('插件总数', $pluginCount)->tagText('总')->themeBlue()->props('height',$height));



                $timeLine = QkTimeline::make();
                $timeLine->add(Component::custom("div")->children([
                    Component::html("QuickAdmin 遵循`Apache2.0`开源协议发布，提供无需授权的免费使用。<br>本项目包含的第三方源码和二进制文件之版权信息另行标注。")
                ]), '2022-07-22')->placement('bottom');
                $timeLine->add('系统开源', '2022-07-22')->placement('bottom');
                $row->col(12, Component::card(Component::custom("div")->children([
                    Component::html("QuickAdmin 遵循`Apache2.0`开源协议发布，提供无需授权的免费使用。本项目包含的第三方源码和二进制文件之版权信息另行标注。"),
                    Component::html("如果觉得项目不错，或者已经在使用了，希望你可以去帮我们点个 ⭐ Star，这将是对我们极大的鼓励与支持。"),
                    Component::custom("div")->children([
                        Component::action(Component::button('开源地址')->props(['type' => 'success']))->openInNewTab("https://gitee.com/sciqtw/quickadmin"),
                        Component::action(Component::button('使用文档')->props(['type' => 'primary']))->openInNewTab("https://doc.quickadmin.cn"),
                    ]),
                ]))->props('shadow', 'hover')->header(Component::custom('div')->children([

                    Component::custom('span')->content('QuickAdmin'),

                ])->style([
                    'position' => 'relative',
                ]))->style(["height" => "350px", 'margin-bottom' => '15px',]));


                $row->col(12, Component::card($table)->props('shadow', 'hover')->header(Component::custom('div')->children([
                    Component::custom('span')->content('系统信息'),
                ])->style([
                    'position' => 'relative',
                ]))->style(["height" => "350px", 'margin-bottom' => '15px']));

            });

        return $content;
    }


    /**
     * @return array|mixed
     */
    protected function actions()
    {
        return [];
    }


    /**
     * 注册批量操作
     * @return array
     */
    protected function batchActions()
    {
        return [];
    }


}
