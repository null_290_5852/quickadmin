<?php

/**
 * @copyright ©2022 QuickAdmin
 * @author QinTingWei
 * @link http://www.quickadmin.cn/
 * Date Time: 2023/3/14
 */

namespace app\admin\quick\actions;


use app\common\service\common\BuildGroupViewService;
use app\common\service\payment\PaymentTypeManager;
use app\common\service\payment\PayTypeInterface;
use quick\admin\actions\RowAction;
use quick\admin\annotation\AdminAuth;
use quick\admin\library\service\UploadService;
use think\Request;

/**
 * 配置上传
 * @AdminAuth(auth=true,menu=true,login=true,title="配置上传")
 * @package app\admin\resource\example\actions
 */
class StorageConfigAction extends RowAction
{



    protected function initAction()
    {
        $this->getDisplay()->type('text')->size('small');
        $this->dialog(['width' => '900px','title' => $this->name()]);
    }



    public function getForm(string $type)
    {
        $storage = UploadService::instance()->setConfig($type,[])->storage();
        return $storage->configForm();
    }

    public function resolve($request, $model)
    {
        $form = $this->getForm($model['type']);
        $form->url($this->storeUrl([

            self::$keyName => $request->param(self::$keyName)
        ]));
        $form->extendData([
            self::$pk => $model[self::$pk],
        ]);

        $form->resolve(json_decode($model->config));

        return $this->response()->success("success", $form);
    }

    public function handle($model, Request $request)
    {
        $form = $this->getForm($model['type']);
        $data = (array)$form->getSubmitData($request, 3);
        $model->config = json_encode($data);
        if ($model->save()) {
            $response = $this->response()->success()->message('设置成功')->event('refresh',[],0,true);
        } else {
            $response = $this->response()->error("设置失败");
        }
        return $response;
    }


}
