<?php

namespace app\admin\jobs;

use quick\admin\library\queue\job\BaseJob;
use quick\admin\library\service\ModuleService;
use quick\admin\library\service\QueueService;
use think\facade\Log;

class UpgradeJob extends BaseJob
{


    /**
     * @param $job
     * @param array $data
     * @return bool
     * @throws \quick\admin\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function handle($job, $data)
    {


        $job->progress(2,'>>>更新开始<<<');
        try {
            ModuleService::instance()->setQueueCode($job->getJobId())->installFile();
        }catch (\Exception $e){
            $job->progress(4,'>>>更新失败<<<');
        }

        return true;
    }
}