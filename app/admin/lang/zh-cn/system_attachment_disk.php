<?php

return [
    'system_attachment_disk Id' => '',
    'system_attachment_disk Storage_id' => '用户仓储ID',
    'system_attachment_disk Type' => '类型',
    'system_attachment_disk Desc' => '说明',
    'system_attachment_disk Config' => '配置',
    'system_attachment_disk Is_deleted' => '删除',
    'system_attachment_disk Status' => '状态',
    'system_attachment_disk Is_deleted 0' => '未删除',
    'system_attachment_disk Is_deleted 1' => '已删除',
    'system_attachment_disk Deleted_at' => '删除日期',
    'system_attachment_disk Created_at' => '创建日期',
    'system_attachment_disk Updated_at' => '更新日期'
];
