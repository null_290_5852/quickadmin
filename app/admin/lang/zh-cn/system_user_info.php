<?php

return [
    'system_user_info Id' => '自增id',
    'system_user_info User_id' => '账号id',
    'system_user_info Gender' => '性别',
    'system_user_info Gender male' => '男',
    'system_user_info Gender female' => '女',
    'system_user_info Gender unknow' => '未知',
    'system_user_info Integral' => '积分',
    'system_user_info Total_integral' => '最高积分',
    'system_user_info Balance' => '余额',
    'system_user_info Total_balance' => '总余额',
    'system_user_info Contact_way' => '联系方式',
    'system_user_info Remark' => '备注',
    'system_user_info Motto' => '个性签名',
    'system_user_info Parent_id' => '上级id',
    'system_user_info Temp_parent_id' => '临时上级',
    'system_user_info Platform_openid' => '平台id 如微信 openid',
    'system_user_info Platform' => '用户所属平台标识 facebook,google,wechat,qq,weibo,twitter,weapp',
    'system_user_info Is_deleted' => ''
];
